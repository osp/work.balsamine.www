Title: L'art de la joie
Dates: 2019/06/11 
Time: 21h00
Slug: art-joie
Key_image: 
Key_image_detail_body: img-18-19/Cover2.png 
Piece_author: Joy
Event_type: Concert slam
Subhead: Dans le cadre du PIF4
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=111
Status: draft
Color: #009933

La slameuse belge nous livre son hymne à la joie libertaire et engagé !

Dans une société où tout nous pousse à nous replier sur nous-même, dans laquelle nous côtoyons la violence, l'injustice et l'inégalité, comment éviter le cynisme et la déprime ? La slameuse belge nous livre son antidote en live, avec son nouvel album L’art de la joie. Inspiré de l’œuvre féministe de la romancière Goliarda Sapienza, cet album, au même titre que le roman, est une ode à la femme libre et humaniste et où se mêlent les questions de l'exil et des racines vécues dans un quotidien urbain, multiculturel et complexe.


Avec 
David Mouyal à la guitare, Alice Vande Voordee à la basse, Aurélie Charneux musique au clavier et à la clarinette et Judith Leonardon aux percussions.
{: .production }
