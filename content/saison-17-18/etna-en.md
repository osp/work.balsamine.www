Title: Etna
Date: 2018/03/07
Slug: etna
Translation: True
Lang: en
End_date: 2018/03/08
Time: 20h30
Piece_author: Thi-Mai Nguyen
Event_type: Dance show
Key_image: shows/9-Etna/thermos.png
Key_image_detail_body: shows/9-Etna/bottle-1.png
Partner_logo: /images/logos/brussels-dance.svg
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=25&cntnt01returnid=44
Color: #ff546d
Subhead: In the context of Brussels, dance! Focus on contemporary dance.
Intro: A wandering and solitary woman, like a ghost haunted by the debris of her life.

A wandering and solitary woman, like a ghost haunted by the debris of her life.

**Etna** is an ageless, homeless woman, harassed by the sounds of her past life and whose body embodies an extreme lassitude. She squats the stage of the theatre and inhabits it through her obsessions and her memories.

> “Poverty is there, over our heads, like the sword of Damocles. And we continue along our path without thinking that it concerns us. There is a thin line between reason and turmoil.
>
> A system on its last legs. A vortex which engulfs everything and leaves no hope. Men and women, on the edge of the abyss, without utopias. They are the mirror of a society on the decline, a society which is no longer breathing. Of a world which is suffocating in its individualism.
>
> In fact, nobody is safe from such a fall, we are all tightrope walkers and we stand firm as much as we can. We fight against the madness which is lying in wait for us.
>
> That is what I wish to evoke, this absurdity, this “almost” invisible world which floods our streets.”
>
> <footer>Thi-Mai Nguyen</footer>

Conception and interpretation
:   Thi-Mai Nguyen

Produced by Thi-Mai Nguyen with the support of Théâtre la Balsamine and with the help of Maison de la création, Ultima Vez and Théâtre Marni.
{: .production }

<div class="galerie" markdown=true>
![etna1](/images/shows/9-Etna/etna-tex.png){: .image-process-large}

![etna2](/images/shows/9-Etna/etna-bag.png){: .image-process-large}

![etna3](/images/shows/9-Etna/etna-bake-tex-2.png){: .image-process-large}
</div>
