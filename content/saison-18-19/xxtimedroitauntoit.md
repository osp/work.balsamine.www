Title: Droit à un toit? 
Date: 2019/02/12
Time: 21h30
Slug: xx-time-droitauntoit
Piece_author: DoucheFLUX
Event_type:  Débat avec notamment Laurent d'Ursel, président de l'association DoucheFLUX
Partner_logo: /images/logos/140378507994.jpg
Status: draft
Color: #fe5000

Débat après la representation d’ Etna avec Laurent d’Ursel, président et porte-parole de l’association Douche-Flux.

DoucheFLUX est un facilitateur de réinsertion pour les personnes en situation précaire, avec ou sans logement, avec ou sans papiers, d’ici ou d’ailleurs, en leur permettant d’accéder à des services, de prendre part à des activités et/ou de se joindre à l’équipe des volontaires.

DoucheFLUX a pour objectif de redonner énergie, dignité et estime de soi. Autant d’éléments indispensables pour accomplir de petits ou de grands pas dans l’existence. Comme sortir de la rue. Ou ne pas y (re)tomber.


En savoir plus sur l'association [DouchefFLUX](http://www.doucheflux.be/)
{: .pedagogique }

