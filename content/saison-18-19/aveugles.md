Title: Aveugles ou comment se donner du courage pour agir ensemble ?
Date: 2019/06/12 
Time: 20h00
Slug: aveugles
Key_image_detail_body: img-18-19/Aveugles3-Aloïs_Lecerf.jpg
Piece_author: Vincent Collet - Théâtre de Poche / Le joli collectif
Event_type: Théâtre - Diptyque théâtral
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=115
Subhead: Dans le cadre du PIF4
Status: draft
Color: #009933

**AVEUGLES** s’inspire autant du petit exposé de Yona Friedman Comment vivre avec les autres sans être chef et sans être esclave ?, que de la pièce de Maeterlinck de 1890 où un groupe se perd en forêt et attend le retour de son guide. Une fable écrite au plateau où les acteurs/personnages devront affronter leurs doutes et leurs propres fonctionnements pour avancer en groupe et organiser une démocratie embryonnaire.

**AVEUGLES** constitue le premier volet d’un travail en trois parties autour du pouvoir. Vincent Collet a décidé d’entamer une étude du pouvoir, plus sensible, moins rationnelle mais éclairante, à travers trois fables : Aveugles, centré autour du groupe, de l’immobilisme et de la délibération, Antigone, questionnant l’exécutif dans l’urgence de l’action, et Projet Justice, autour du droit, de la jurisprudence et des normes à venir.
Dans le cadre du PIF, nous vous proposons de découvrir AVEUGLES et ANTIGONE à la suite. 
{: .production }


Concept et mise en scène
:    Vincent Collet avec la collaboration de Pierre Déaux

Écriture et jeu
:    Marie-Lis Cabrières, Vincent Collet, Fanny Fezans, Vincent Voisin

Régisseur lumière et son 
:    Ewen Toulliou

Création lumière 
:    Florian Leduc

Production/Diffusion
:    Elisabeth Bouëtard et Marion Le Guerroué

Production
:    Théâtre de Poche / Le joli collectif – Scène de territoire pour le théâtre, Bretagne romantique & Val d’Ille - Aubigné

Coproducteurs
:    Au bout du plongeoir – Rennes, Les Fabriques – Nantes, Salle Guy Ropartz – Rennes

Soutiens
:    La Paillette – Théâtre MJC et L’Aire Libre – CPPC et Rennes Métropole

Très librement inspiré du texte Les Aveugles de Maurice Maeterlinck
{: .production }
