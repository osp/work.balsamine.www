Title: 1. Pink boys and old ladies
Subhead: La représentation du 28 septembre est complète. 
Dates: 2019/09/28 
       2019/09/30 → 2019/10/05 
Time: 20h30
Slug: pinkboys
Key_image: img-19-20/programme/chapitre-1/1280px-Hamburg_-_Klubhaus_St._Pauli_(2).jpg
Key_image_detail_body: img-19-20/programme/chapitre-1/1280px-Hamburg_-_Klubhaus_St._Pauli_(2).jpg
Piece_author: Marie Henry / Clément Thirion / Kosmocompany
Event_type: Création théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=119
Color: #ff7c48


Normand, qui n’est pas normand, est un petit garçon assez banal, mais Normand aime porter des robes. La sœur du père de Normand trouve ce comportement limite limite - elle double toujours les mots quand elle est gênée. La mère de Normand aimerait parfois trépaner son fils pour voir ce qui cloche à l’intérieur, ce qui est radical mais pas le plus pratique. Sa grand-mère maternelle - et pourquoi pas une cure d’hormones ? - semble être la plus concrète mais Normand ne veut pas changer de sexe, Normand veut juste se sentir bien, dans cet entre-deux, dans cet espace indéfini dans lequel il évolue, et qui ne porte pas de nom. Le père de Normand, enfin, aimerait s’exprimer, mais ce qu’il dit est inintelligible. Un jour, il décide d’accompagner son fils en robe à l’école…


Tiré d’un fait divers réel, *Pink boys and old ladies* déploie une fresque familiale impressionniste et caustique, où chacun, empêtré dans cette situation délicate, exprime mal son malaise. Si dans cette famille on parle beaucoup, on parle surtout de rien, parce qu’on ne veut pas parler de tout.
{: .push-down}


Mise en scène 
:   Clément Thirion 

Écriture et dramaturgie 
:   Marie Henry 

Interprétation 
:   Gwen Berrou, Lucas Meister, Clément Thirion, Simon Thomas, Mélodie Valemberg, Mélanie Zucconi

Assistante mise en scène 
:   Déborah Marchal 

Musique 
:   Thomas Turine 

Scénographie, lumières et costumes
:   Saskia Louwaard et Katrijn Baeten 

Vidéo et photographie
:   Julien Stroïnovsky

Régie générale et direction technique
:   Christophe Van Hove <br/>

Accompagnement et diffusion
:   BLOOM Project /Stéphanie Barboteau 

Production déléguée 
:   MARS, Mons arts de la Scène

Coproductions 
:   Kosmocompany, Théâtre de Liège, Théâtre la Balsamine, Maison de la Culture de Tournai/maison de création, La Coop asbl et Shelter Prod

Soutiens 
:   Fédération Wallonie-Bruxelles- Service du théâtre, taxshelter.be,  ING, Tax-shelter du gouvernement fédéral belge

Infos pour les professeurs et les associations : Grâce au soutien de la Cocof (programme initiation théâtrale), ce spectacle est proposé aux élèves du secondaire et aux groupes associatifs de la Région Bruxelles-Capitale au tarif de 2€ la place.
{: .pedagogique }

<div class="galerie photos-spectacle" markdown=true>
![pinkboys](/images/img-19-20/pinkboys3.jpg){: .image-process-large}
:   © Anoek Luyten 

![pinkboys](/images/img-19-20/pinkboys4.jpg){: .image-process-large}
:   © Anoek Luyten 

![pinkboys](/images/img-19-20/pinkboys5.jpg){: .image-process-large}
:   © Anoek Luyten 

![pinkboys](/images/img-19-20/pinkboys6.jpg){: .image-process-large}
:   © Anoek Luyten 

![pinkboys](/images/img-19-20/pinkboys7.jpg){: .image-process-large}
:   © Anoek Luyten 

![pinkboys](/images/img-19-20/pinkboys8.jpg){: .image-process-large}
:   © Anoek Luyten

![pinkboys](/images/img-19-20/_AnnahSchaeffer-2744.jpg){: .image-process-large}
:   © Annah Schaeffer

<iframe class="video" src=https://player.vimeo.com/video/360221080></iframe>

</div>
