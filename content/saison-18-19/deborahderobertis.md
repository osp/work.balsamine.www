Title: Déborah De Robertis
Date: 2019/04/27
Time: 20h30
Slug: Déborah De Robertis
Event_type: Rencontre et projection
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=109
Color: #fe5000 

Retour aux sources pour Déborah De Robertis, à Bruxelles, où elle a fait ses études et premières recherches. Une occasion rare d'un échange avec elle à la Balsa comme " safer space".
« C'est en prenant la caméra que j'ai pris le pouvoir. Le "point de vue du modèle nu féminin" ce regard non pas dirigé sur la nudité mais porté sur la nudité, une position que (la philosophe ) Geneviève Fraisse définit plus largement comme " le corps qui regarde" du point de vue philosophique et historique (...) C'est "le sexe qui regarde".

Deborah De Robertis, "Cours petite fille !", Editions des femmes - Antoinette Flouque, à paraître en 2019. 
{: .production }



