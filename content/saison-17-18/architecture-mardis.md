Title: Architecture
Date: 2017/11/14
Time: 20h30
Category: mardis contemporains
Key_image: shows/1-Helpers/helper-wind-grid.png
Key_image_detail_body: shows/1-Helpers/helper-wind-grid.png
Color: #46cea0
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=30&cntnt01returnid=44

Les mardis contemporains proposent aux oreilles curieuses de découvrir des facettes de la musique d’aujourd’hui, dans un programme pluriel qui dialogue avec l’histoire, la philosophie, la sociologie, mais aussi le cirque, le cinéma, la science ou l’architecture… Le temps de 3 concerts-rencontres, les mardis contemporains prennent leurs quartiers à la Balsamine.

## Architecture

### Le 14 novembre à 20h30

Il est surprenant de constater comme il existe un lexique commun entre l’architecture et la musique. Concevoir des espaces, élaborer des réflexions à la fois esthétiques et philosophiques sont autant de confluences possibles entre ces deux arts. François Schuiten, dont les dessins se nourrissent d’architectures existantes et imaginées et Francis Metzger, architecte à qui l’on doit le théâtre de la Balsamine ou la restauration de la gare Bruxelles Centrale notamment viendront nous parler d’architecture et du lien tissé avec l’esthétique, la société, la philosophie et les autres arts.
L’ensemble Sturm und Klang et le quatuor Tana interprèteront Tetras de Iannis Xenakis et City Life de Steve Reich, œuvres qui cultivent avec évidence les passerelles entre architecture et musique.
{: .production }

Invités
:    Francis Metzger et François Schuiten

Œuvres
:    Tetras de Iannis Xenakis (1983)
:    Quatuor à cordes n°2 de Luis Naon extrait du cycle Urbana (1999–2001)
:    City Life de Steve Reich (1995)
:    Création de Stéphane Orlando (commande Ars Musica 2017)

Direction
:    Thomas Van Haeperen

Interprétation
:    Quatuor TANA / Ensemble Sturm & Klang
