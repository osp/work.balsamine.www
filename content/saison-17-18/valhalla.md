Title: Valhalla ou le crépuscule des dieux
Date: 2018/06/14
Time: 21h00
Piece_author: Petri Dish / Anna Nilsson / Sara Lemaire
Event_type: Cirque - Work in progress
Key_image_detail_body: greenportrait.jpg
Status: draft
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=73
Subhead: Les réservations pour les soirées combinées se font uniquement par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68

Un bateau échoué, pris dans l’hiver d’un océan de glace.
A son bord, les derniers survivants d’un monde... sombrant doucement
dans la solitude de la folie. Jusqu’au jour où l’un des membres de l’équipage s’éloigne du bateau... Que la mutinerie commence. 
Avec du cirque, chant, danse et cornemuse, **Valhalla** parle tout bas du pouvoir, celui qu’on veut, celui qu’on prend, qui s’insinue et que l’on combat, des gens pris dans sa tempête.


Avec
: Joris Baltz, Viola Baroncelli, Laura Laboureur, Carlo Massari, Anna Nilsson, Jef Stevens

Concept et mise en scène
:  Anna Nilsson, Sara Lemaire

Création lumière
: Philippe Baste

Technique
: Tonin Bruneton, Cristian Gutierrez, Camille Rolovic

Production : Petri Dish / I.S.E. asbl
En coproduction avec Le Groupe des 20 Théâtres en Île-de-France / le Théâtre la Balsamine / Theater Op De Markt – Dommelhof / le Centre Culturel du Brabant Wallon
Avec l’aide de la Fédération Wallonie-Bruxelles, service du cirque, Kulturradet/Swedish Arts Council,
Konstnärsnämnden - the Swedish Arts Grants Committee
Partenaires : Circuscentrum / Théâtre Marni / Cirkus Cirkör / Wolubilis / Latitude 50° / Espace Catastrophe - Centre International de Création des Arts du Cirque / Subtopia / Dansenshus 
{: .production }