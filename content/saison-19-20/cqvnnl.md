Title: 2. Ce qui vit en nous
Date: 2019/11/5
End_date: 2019/11/9
Time: 20h30
Slug: ce-qui-vit
Translation: true
Lang: nl
Key_image: img-19-20/programme/chapitre-2/stephie.png
Key_image_detail_body: img-19-20/programme/chapitre-2/stephie.png
Piece_author: Stéphane Arcas, Baudouin de Jaer et Martijn Dendievel
Event_type: Kameropera in 3 akten - met boventiteling in NL+FR
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=120
Partner_logo: /images/logos/ars-musica.svg
Color: #7fa22a


Midden in een vreemde ruimte tracht een jongeman met geheugenverlies flarden herinneringen aan elkaar te rijgen om te achterhalen hoe hij daar terechtgekomen is. Een verpleegster komt naar hem toe, maar hij wordt er niet wijzer van. Na diverse absurde veronderstellingen gaat het doek open en zien we de realiteit: hij ligt in een autopsiekamer.

*Ce qui vit en nous* is een gezongen komedie waarin elk personage over de dood praat en laat zien dat die in de geest van elk levend wezen aanwezig is. Beurtelings beschrijven ze op hun eigen manier dat gevoel van unheimlichkeit, zoals Freud het noemde; de plek waar een schaduw over de alledaagsheid valt, waar het leven en het levenloze het op een akkoordje gooien - in alles wat ons bekend voorkomt ligt de dood verscholen. Ons hectische bestaan mag Magere Hein dan wel uit onze gedachten bannen, maar hij verschijnt altijd onaangekondigd. En dan dringt het tot ons door: hij was er al vanaf het begin Hij is wat in ons leeft.
{: .push-down}


Libretto, regie en scenografie
:   Stéphane Arcas

Muziek
:   Baudouin de Jaer

Muzikale leiding 
:   Martijn Dendievel

Zangers en acteurs
:   Sarah Théry (mezzo), Xavier de Lignerolles (tenor), Nicolas Luçon (acteur), Thomas Van Caekenberghe (bariton)

Musici
:   Ensemble Besides, Romy-Alice Bols (fluit), Jasper Braet (live electronics), Toon Callier (elektrische gitaar), Fabian Coomans (keyboard), Sam Faes (cello), Nele Geubels (saxofoon), Stefanie Van Backlé (viool), Jeroen Stevens (slagwerk), Jutta Troch (harp)

Regieassistentie 
:   Cécile Chèvre

Ruimtelijk concept en opstelling
:   Stéphane Arcas, Claude Panier et Marie Szernovicz

Lichtontwerp 
:   Julie Petit-Etienne

Geluidstechnicus
:  Jean-Maël Guyot 

scenografieassistentie 
:   Alice Guillou

Opnamen 
:   Mathieu Haessler et Sonia Ringoot

Afgevaardigd producent
:   Théâtre la Balsamine

Coproducties
:   BLACK FLAG ASBL, NOODIK productions, Théâtre la Balsamine, Ars Musica, La Coop asbl en Shelter Prod

Met de steun van
:   Forum des compositeurs, Fédération Wallonie-Bruxelles- Service du théâtre, taxshelter.be, ING, Tax-shelter van de Belgische federale overheid

<div class="galerie photos-spectacle" markdown=true>
![cqven](/images/img-19-20/cequivit.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe class="video"src="https://player.vimeo.com/video/366967279"></iframe>
</div>