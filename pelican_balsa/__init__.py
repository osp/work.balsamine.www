import shlex
import subprocess

from pelican import signals, contents
from pelican.readers import METADATA_PROCESSORS
from pelican.utils import get_date

from bs4 import BeautifulSoup

import random

import re
import datetime

time_chunk_patt = r'(\d+)(?:\:|h)(\d+)?'

# Recognize time part in the description
# \2 (group 2) is the separator, as to have the same separator in the match
time_patt = r'(\d+(:|h)(?:\d+)?)(?:\s*(?:→|au|->|à)\s*)?(\d+\2(?:\d+)?)?'

# \2 (group 2) is the separator, as to have the same separator in the match
date_range_patt = r'(\d+([/\-\.\s])\d+\2\d+)(?:\s*(?:→|au|->)\s*)(\d+\2\d+\2\d+)'

def parse_time_chunk (chunk):
    m = re.match(time_chunk_patt, chunk)
    hour = int(m.group(1))
    minutes = int(m.group(2) if m.group(2) else 0)
    return datetime.time(hour, minutes)

def parse_time_part (line):
    m = re.search(time_patt, line)

    if m:
        if m.group(3):
            return {
                'start': parse_time_chunk(m.group(1)),
                'end': parse_time_chunk(m.group(3))
            }
        else:
            return {
                'start': parse_time_chunk(m.group(1))
            }
    else:
        return None

def parse_time (time):
    parsed = parse_time_part(time)

    return parsed if parsed else time

def parse_date_part (line):
    m = re.match(date_range_patt, line)
    if m:
        return {
            'date': get_date(m.group(1)),
            'end_date': get_date(m.group(3))
        }
    else:
        return { 'date': get_date(line) }

def parse_date_line (line):
    date = parse_date_part(line)
    time = parse_time_part(line)

    if time:
        date['time'] = time

    return date

"""
    For future if there is a different time per event?

    Date formatting

    'dates': [
        { 
            'date': date,
            'time': (time,) | (time,time)
        },
        { 
            'start_date': date,
            'end_date': date,
            'time': (time,) | (time,time)
        },
    ]
"""
def parse_dates (generator, metadata):
    if 'dates' in metadata:
        dates = metadata['dates']
        if isinstance(metadata['dates'], list):
            metadata['dates'] = [parse_date_line(show) for show in metadata['dates']]
        else:
            metadata['dates'] = [parse_date_line(metadata['dates'])]
        metadata['end_date'] = metadata['dates'][-1]['end_date'] if ('end_date' in metadata['dates'][-1]) else metadata['dates'][-1]['date']

        if not 'date' in metadata:
            metadata['date'] = metadata['dates'][0]['date']
    elif 'end_date' in metadata:
        metadata['dates'] = [{'date': metadata['date'], 'end_date': metadata['end_date']}]
    else:
        metadata['dates'] = [{'date': metadata['date']}]

def add_metadata_processors(arg):
    METADATA_PROCESSORS['end_date'] = METADATA_PROCESSORS.get('end_date', METADATA_PROCESSORS.get('date'))
    METADATA_PROCESSORS['end_date'] = lambda x, y: get_date(x.replace('_', ' '))
    METADATA_PROCESSORS['time'] = lambda x, y: parse_time(x)

def process_css(pelican):
    path = str(pelican.settings['OUTPUT_PATH'] + '/theme/css/' + pelican.settings['CSS_FILE'])
    cmd = "./node_modules/.bin/postcss --map --use postcss-cssnext --output {} {}".format(path, path)

    call_params = shlex.split(cmd)
    subprocess.call(call_params)


def register():
    signals.initialized.connect(add_metadata_processors)
    signals.article_generator_context.connect(parse_dates)
    signals.finalized.connect(process_css)