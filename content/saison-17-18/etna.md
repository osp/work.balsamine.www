Title: Etna
Date: 2018/03/07
End_date: 2018/03/09
Time: 20h30
Piece_author: Thi-Mai Nguyen
Subhead: Dans le cadre de Brussels, dance ! Focus on contemporary dance.
Event_type: Création danse
Key_image: shows/9-Etna/thermos.png
Key_image_detail_body: shows/9-Etna/bottle-1.png
Partner_logo: /images/logos/brussels-dance.svg
Color: #ff546d
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=25&cntnt01returnid=44
Intro: Une femme errante et solitaire, tel un spectre hanté par ses débris de vie.

Une femme errante et solitaire, tel un spectre hanté par ses débris de vie.

**Etna** est une femme sans âge, sans domicile, harcelée par les sonorités de sa vie passée et dont le corps incarne une lassitude extrême. Elle squatte la scène du théâtre et l’habite au travers de ses obsessions et de ses souvenirs.

> «La pauvreté est là, au-dessus de nos têtes, comme une épée de Damoclès. Et nous poursuivons notre route sans penser que cela nous concerne. La frontière est mince entre la raison et l’égarement.
>
> Un système à bout de souffle. Un vortex qui engloutit tout et ne laisse aucun espoir. Des hommes et des femmes, au bord du gouffre, sans utopies. Ils sont le miroir d’une société en déclin, d’une société qui ne respire plus. D’un monde qui s’étouffe dans son individualisme.
>
> Et pourquoi tant de gens sont happés dans ce trou, dans ce retranchement. Cela tient à peu. Une perte d’emploi, une perte amoureuse. Une perte qui mène à la dérive et les fait sortir de notre réalité.
>
> En soi, personne n’est à l’abri d’une telle chute, nous sommes tous des funambules et nous résistons comme nous le pouvons. Nous luttons contre la folie qui nous guette.
>
> C’est de cela que je veux parler, de cette absurdité, de ce monde « presque » invisible qui inonde nos rues.»
>
> <footer>Thi-Mai Nguyen</footer>


Conception et interprétation
:   Thi-Mai Nguyen

Création sonore et régie son 
:   Antoine Delagoutte

Création lumière et régie lumière
: Rémy Urbain

Diffusion
: Eléonore Valère Lachky

Production déléguée
: Théâtre la Balsamine

Avec l’aide de la Fédération Wallonie-Bruxelles – Service de la Danse, 
Avec le soutien du Théâtre Marni, de la Maison de la Création et d’Ultima Vez.
{: .production }

<div class="galerie" markdown=true>
![etna](/images/shows/9-Etna/etna-tex.png){: .image-process-large}

![etna](/images/shows/9-Etna/etna-bag.png){: .image-process-large}

![etna](/images/shows/9-Etna/etna-bake-tex-2.png){: .image-process-large}

![etna1](/images/Etna1.jpg){: .image-process-large}

![etna2](/images/Etna2.jpg){: .image-process-large}

![etna3](/images/Etna3.jpg){: .image-process-large}

![etna4](/images/Etna4.jpg){: .image-process-large}

![etna1](/images/etna1.jpg){: .image-process-large}

![etna2](/images/etna2.jpg){: .image-process-large}

![etna3](/images/etna3.jpg){: .image-process-large}

![etna4](/images/etna4.jpg){: .image-process-large}

![etna5](/images/etna5.jpg){: .image-process-large}

![etna6](/images/etna6.jpg){: .image-process-large} 

<iframe class="video" src="https://player.vimeo.com/video/258297730"></iframe>
</div>


</div>
