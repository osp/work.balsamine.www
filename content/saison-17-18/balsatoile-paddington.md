Title: Paddington
Date: 2018/05/30
Time: 15h00
TimeEnd: 16h35
Category: balsatoiles
Length: 95'
Age: 7ans et plus
Piece_author: Paul Kling 
Event_type: Balsatoiles
Color: #dbb3ce
Key_image_detail_body:    PADDINGTON-master1050.jpg
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=61

Paddington raconte l'histoire d'un jeune ours péruvien fraîchement débarqué à Londres, à la recherche d'un foyer et d'une vie meilleure. Il réalise vite que la ville de ses rêves n'est pas aussi accueillante qu'il croyait. Par chance, il rencontre la famille Brown et en devient peu à peu un membre à part entière.

Un atelier créatif, animé par un artiste plasticien, est proposé de 14h à 15h.
(inscription obligatoire – nombre de place limité)


Projet réalisé avec le soutien du Fonds Baillet Latour, dans le cadre de l’initiative L’extrascolaire au cœur de l’intégration gérée par la Fondation Roi Baudouin.
{: .production }

[Voir la bande-annonce du film](http://www.allocine.fr/video/player_gen_cmedia=19549130&cfilm=207487.html)
{: .production }
