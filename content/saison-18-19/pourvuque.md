Title: Pourvu que quelque chose nous arrive
Date: 2019/06/13 
Time: 19h00
Slug: pourvu
Key_image_detail_body:img-18-19/VISUEL_POURVU_QUEcopyrightEve_Giordani.jpg
Piece_author: Eve Giordani & Claire Farah
Event_type: Installation immersive 
Subhead: Dans le cadre du PIF4 - Entrée libre
Status: draft
Color: #009933

Attendre que quelque chose arrive. Peu importe quoi, il suffit de savoir que quelque chose peut arriver. Une plante verte, une petite musique qui tourne en rond et la voix d’une hôtesse qui nous parle de la pluie et du beau temps. Mariner dans son jus, s’attarder sur les petits riens et se laisser prendre au jeu de la farce tragi-comique. 

**Pourvu que quelque chose nous arrive** propose aux spectateurs une expérience sensorielle pour évoquer l’attente comme condition même de l’existence, pour ce qu’elle révèle sur notre façon d’appréhender le temps présent et notre nécessité profonde de se projeter dans un ailleurs réel ou fantasmé.

A la frontière entre théâtre et installation, Claire Farah et Eve Giordani imaginent des dispositifs comme des bulles, des extractions temporelles, et proposent des écarts du regard et une exploration du peu. Non sans une certaine conscience de nager à contre-courant, c’est avec humour et dérision que leur langage se déploie et s’emploie à braver valeureusement les noirceurs de ces temps tonitruants.
{: .production }

Conception 
:    Eve Giordani & Claire Farah

Voix 
:    Karine Birgé

Technique son 
:    Nicolas Arnould

Création lumière 
:    Virginie Strub

Construction 
:    Quai 41

Remerciements : 
Virginie Strub, Félicie Artaud, Brice Cannavo, Elodie Bauchet, Cyril Briant, Ariane D'hoop, Nicolas Buysse, Nicolas Marty, Sophie Leso, Antoine Blanquart, Fré Werbrouck, Sara Sampelayo, Sybille Cornet, Mélanie Tamm, Ronan Deriez, Pierre Foulon.
{: .production }
