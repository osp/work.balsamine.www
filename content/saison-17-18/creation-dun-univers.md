Title: Création d'un univers
Date: 2017/12/29
Times: 10h00
       14h00
Category: Noël au Théâtre
Color: #a59cd8
Partner_logo: /images/logos/ctej.svg
              /images/logos/grenouille.jpg
Piece_author: Compagnie des Mutants
Age: à partir de 2 ans et demi
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=41

**Les représentations sont complètes**

La lune se couche.<br>
Le soleil se lève au son du jazz et du swing.<br>
Un homme et une femme apparaissent.<br>
Ensemble, ils vont danser et jouer avec des objets du quotidien :<br>
Une fleur, un œuf, un poisson…<br>
Par magie, ils vont transformer ces objets en œuvres d’art.<br>
Quand la lune amorcera son déclin,<br>
Ils auront créé leur univers,<br>
Leur petit musée d’art moderne…<br>
*La beauté est dans les yeux de celui qui regarde.* Oscar Wilde

Création collective
:    Dirk Opstaele, Fanny Hanciaux, Marc Weiss, Yoeri Lewijze

Chorégraphie
:    Dirk Opstaele

Chorégraphie du Lindy Hop 
:    Sylvie Planche

Scénographie
:    Ensemble Leporello et Chloé Coomans

Solutions techniques
:    Yoeri Lewijze et Patrick Dhooge

<div class="galerie" markdown=true>
![geo](/images/creation1.jpg)
photo: Gilles Destexhe

![geo2](/images/creation2.jpg)
photo: Gilles Destexhe
</div>
