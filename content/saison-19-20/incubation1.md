Title: Je suis venu voir les gens danser autour de moi...
Dates: 2019/09/17 
Time: 19h30
Slug: jesuisvenuvoirlesgens
Key_image: img-19-20/je_suis_venu__1_.png
Key_image_detail_body: img-19-20/je_suis_venu__1_.png
Piece_author: Louis Labadens et Anna Ten
Event_type: Sortie de résidence publique
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=146
Subhead: De le cadre des incubations
Status: draft
Color: #ff7c48



Actuellement en résidence à la Balsa, Anna Ten et Louis Labadens  présenteront le mardi 17 septembre, "Je suis venu voir les gens danser autour de moi", un spectacle mêlant danse contemporaine et théâtre documentaire, basé sur l’histoire singulière d’un spectateur. 

"Je suis venu voir les gens danser autour de moi" retrace l’histoire d’un danseur amateur sexagénaire qui est conduit par passion à rejoindre le plateau de chorégraphes de renom. A travers son parcours singulier, sont questionnées la transmission de la danse contemporaine parmi les non-professionnels et la frontière entre un spectateur et un pratiquant.

[Découvrez le projet en vidéo](https://www.youtube.com/watch?v=e8h7xlnacg4&t=2s)
{: .push-down}


Co-auteur et interprète 
:   Louis Labadens 

Mise en scène et dramaturgie 
:   Anna Ten 

