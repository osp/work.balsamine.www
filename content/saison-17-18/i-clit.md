Title: i-clit
Date: 2018/02/27
Slug: i-clit
Translation: true
Lang: fr
End_date: 2018/03/03
Time: 20h30
Key_image: shows/5-i-clit/shoes.png
Key_image_detail_body: shows/5-i-clit/scene-setup.png
Piece_author: Mercedes Dassy
Event_type: Création danse
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=24&cntnt01returnid=44
Subhead: Dans le cadre de Brussels, dance! Focus on contemporary dance.
Partner_logo: /images/logos/brussels-dance.svg
Intro: Un spectacle manifeste du corps, de la chair et du sexe féminin.
Color: #fce761

**Les représentations des 27 et 28 février ainsi que celles des 1er et 2 mars sont complètes.**

Un spectacle manifeste du corps, de la chair et où l'objet sexuel devient sujet.

Une nouvelle vague féministe est née — ultra-connectée, ultra-sexuée et plus populaire. Mais face au pouvoir ambivalent de la pop culture, où et comment se placer dans ce combat en tant que jeune femme? Quelles armes utiliser? 

**i-clit** traque ces moments de fragilité, où l’on oscille entre nouvelles formes d'oppressions et affranchissement.

Concept, chorégraphie, interprétation
:   Mercedes Dassy

Dramaturgie, regard extérieur
:   Sabine Cmelniski

Scénographie et création lumière
:   Caroline Mathieu

Création sonore
:   Clément Braive

Costumes, scénographie
:   Justine Denos
:   Mercedes Dassy

Diffusion
:   Art Management Agency (AMA)

Production déléguée
:   Théâtre la Balsamine


Une coproduction du Théâtre la Balsamine et de Charleroi-danse avec les soutiens de la Fédération Wallonie-Bruxelles- Service de la danse, du Théâtre Océan Nord, de l’Escaut, du B.A.M.P., de Project(ion) Room et Friends with benefits.Le son de ce spectacle a été créé grâce au soutien de la SACD.
{: .production }

<div class="galerie" markdown=true>
![iclit](/images/shows/5-i-clit/shoes-pillows-hd.png){: .image-process-large}

![iclit](/images/shows/5-i-clit/i-clit-composition-gamma-ok.png){: .image-process-large}

![iclit1](/images/iclit1.jpg){: .image-process-large}

![iclit2](/images/iclit2.jpg){: .image-process-large}

![iclit3](/images/iclit3.jpg){: .image-process-large}

![iclit4](/images/iclit4.jpg){: .image-process-large}

![iclit5](/images/iclit5.jpg){: .image-process-large}

![iclit6](/images/iclit6.jpg){: .image-process-large} 

<iframe class="video" src=" https://player.vimeo.com/video/255039212 "></iframe>
</div>
