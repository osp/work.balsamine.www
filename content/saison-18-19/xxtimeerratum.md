Title: Erratum
Date: 2019/02/19
End_date: 2019/03/22
Slug: xx-time-erratum
Piece_author:  Petite Poissone
Event_type:  Installation 
Status: draft
Color: #fe5000

Petite Poissone est une artiste grenobloise dont le travail est essentiellement basé sur le texte. Son oeuvre, elle aime la partager de manière directe à travers des aphorismes collés dans la rue ou sur des objets du quotidien. 
Pour le XX Time, elle poétise le bar de la Balsamine en y déployant quelques textes choisis. 



