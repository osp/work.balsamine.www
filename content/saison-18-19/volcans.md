Title: VOLCANS, We don’t know when it will happen
Date: 2019/06/13 
Time: 20h30
Slug: volcans
Key_image_detail_body:img-18-19/Julie_de_Halleux.jpeg
Piece_author: Aurélien Leforestier et Thymios Fountas
Event_type: Performance / Installation 
Subhead: Dans le cadre du PIF4
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=112
Status: draft
Color: #009933

Ah, les volcans ! Débordant de vie et d’animé, là où il n’y aurait que matière minérale et lois physiques, s’étendant des entrailles de la terre jusqu’au plus haut des cieux, ces montagnes ardentes n’auront eu de cesse d’embraser notre imaginaire à travers les arts et le temps. Dans **VOLCANS, We don’t know when it will happen**, ils ne seront pas moins de dix à se laisser aller à leur caractère éruptif et imprévisible, tandis que deux gentilhommes tenteront tant bien que mal de nous les présenter. Une conférence-performance en carton-pâte qui rend hommage à cette puissance terrestre et à l’incertitude dans laquelle elle nous plonge.


Concept, jeu et mise en scène 
:    Aurélien Leforestier et Thymios Fountas

Soutien 
:    Théâtre la Balsamine
{: .production }