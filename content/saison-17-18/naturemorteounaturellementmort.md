Title: Nature Morte ou Naturellement Mort
Date: 2018/06/13
Dates: 2018/06/13 20h00
       2018/06/13 22h00
Piece_author: Karin Vyncke
Event_type: Performance
Key_image_detail_body: nature morte 3.jpg
Status: draft
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=67
Subhead: Les réservations pour les soirées combinées se font uniquement par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68

Une performeuse nous invite au voyage par la puissance de l’évocation et peint dans notre imaginaire une toile. 
De mot en mot, le spectateur ajoute ses propres touches de couleur et devient le peintre de son propre spectacle.
Le champ s’élargit peu à peu, le peintre déborde du canevas, Cerberus le chien erre.
 > « La nature morte m'intéresse parce qu'elle est suggestive. Les scènes sont méticuleusement composées. Elles reflètent des tables richement dressées avec des fleurs bien rangées et des bols de fruits, des scènes de jeu avec des animaux morts et des fusils et des verres avec du vin rouge profond. Dans la performance, je remplace les objets peints par des mots avec lesquels je suggère des scènes. Mais la table ne suffit plus, les mots pénètrent toute la pièce. La nature morte commence à tout couvrir, comme un raz-de-marée. Il couvre finalement la surface entière et se transforme en paysage, un petit champ de bataille, dont vous ne pouvez pas nier la réalité. » 
 > <footer>Karin Vyncke</footer>

Une création de Karin Vyncke avec les soutiens de RAMDAM, le Centre LORCA à Bruxelles et la Balsamine.
{: .production }

