Title: Coraline   
Date: 2020/04/01
Time: 15h00
Slug: balsatoile5
Category: annulé
Length: 100 minutes
Age: 10 ans et plus
Piece_author: Henry Selick <br/> Projection annulée
Key_image:
Key_image_detail_body: img-19-20/coraline.jpg
Event_type: Projection gratuite
Color: #6b89ed


L'histoire d'une fillette qui pousse une porte secrète dans sa nouvelle maison et découvre alors une version alternative de sa propre vie. Au premier abord, cette vie parallèle est étrangement similaire à la sienne - en bien meilleure. Mais quand cette aventure fantastiquement déjantée commence à devenir dangereuse et que sa fausse mère essaie de la garder avec elle à jamais, Coraline n'a d'armes que son ferme entêtement et son courage, et la complicité de voisins et d'un chat noir parlant, pour venir en aide à ses vrais parents et aux autres enfants fantômes et rentrer enfin à la maison.

Entrée libre mais réservation souhaitée !

Chaque film sera précédé, à 14h, d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=XvCdAEF2rOU) 

Avec le soutien du Fonds Baillet Latour, dans le cadre de l'initiative "L’Extrascolaire au cœur de l’intégration" gérée par la Fondation Roi Baudouin.
{: .production }


Prochaines séances : 
- le 6 mai
- le 3 juin
{: .pedagogique }