Title: Le Grand Méchant Renard et autres contes
Date: 2018/02/07
Time: 15h00
Category: balsatoiles
Length: 78 minutes
Age: 5 ans et plus
Piece_author: Benjamin Renneret Patrick Imbert
Event_type: Balsatoiles
Color: #e5c172
Key_image_detail_body: le_grand_mechant_renard_1.jpg
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=51
Ceux qui pensent que la campagne est un lieu calme et paisible se trompent, on y trouve des animaux particulièrement agités, un Renard qui se prend pour une poule, un Lapin qui fait la cigogne et un Canard qui veut remplacer le Père Noël. Si vous voulez prendre des vacances, vous allez être surpris...

Un atelier créatif, animé par un artiste plasticien, est proposé de 14h à 15h.
(inscription obligatoire – nombre de place limité)


Projet réalisé avec le soutien du Fonds Baillet Latour, dans le cadre de l’initiative L’extrascolaire au cœur de l’intégration gérée par la Fondation Roi Baudouin. Avec l’aide du Réseau Action Culturelle Cinéma (R.A.C.C.) 
{: .production }

[Voir la bande-annonce du film](http://www.allocine.fr/video/player_gen_cmedia=19569940&cfilm=249654.html)
{: .production }
