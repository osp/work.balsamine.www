Title: Le corps féminin peut-il vraiment s'émanciper ?
Date: 2019/02/20
Time: 19h
Slug: xx-time-corpsdefemme
Piece_author:  Nathalie Grandjean
Event_type:  Réflexion philosophique et échange autour de cette question.  
Status: draft
Color: #fe5000

Nathalie Grandjean, docteure en philosophie, enseigne l’éthique du numérique et la philosophie féministe à l’Université de Namur. Elle est aussi administratrice de Sophia, le réseau belge d’études de genre. Elle a édité l’ouvrage Corps et Technologies. Penser l’hybridité (avec Claire Lobet, Peter Lang, 2012) et Valeurs de l’attention (avec Alain Loute, Presses du Septentrion, 2019 – sous presse).

