Title: Gus, petit oiseau, grand voyage  
Date: 2020/06/03
Time: 15h00
Slug: balsatoile7
Category: annulé
Length: 90 minutes
Age: 5 ans et plus
Piece_author: Christian De Vita <br/> Projection annulée
Key_image:
Key_image_detail_body: img-19-20/affiche-gus-petit-oiseau-grand-voyage-720x350.jpg
Event_type: Projection gratuite
Color: #6b89ed


À l'heure du départ pour la grande migration, Darius, le doyen de la volée, est blessé. Il va devoir confier tous ses secrets et le nouvel itinéraire du voyage au premier oiseau venu. Et cet oiseau... c'est notre héros, exalté à l'idée de découvrir enfin le monde... mais pas du tout migrateur !

Entrée libre mais réservation souhaitée !

Chaque film sera précédé, à 14h, d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=BK4FXEmCnvk) 

Avec le soutien du Fonds Baillet Latour, dans le cadre de l'initiative "L’Extrascolaire au cœur de l’intégration" gérée par la Fondation Roi Baudouin.
{: .production }