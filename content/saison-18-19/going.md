Title: Going
Dates: 2019/04/5 
Slug: going
Time: 22h00
Key_image: 
Key_image_detail_body: img-18-19/going3.jpg
Piece_author: Concert
Event_type: Free-jazz
Color: #fe5000

Le groupe GOING rassemble 4 musiciens belges, Giovanni Di Domenico, Joao Lobo, Pak Yan Lau et Mathieu Celleja. Leur musique fait écho à du free-jazz, mais est aussi étroitement liée à de la musique improvisée, expérimentale et minimale. Elle peut être décrite comme un grand voyage dans le système stellaire urbain d'aujourd'hui. 



<div class="galerie photos-spectacle" markdown=true>
![going](/images/img-18-19/going3.jpg){: .image-process-large}
:   © Dragan Markovic

![going](/images/img-18-19/going1.jpg){: .image-process-large}
:   -

![going](/images/img-18-19/going2.jpg){: .image-process-large}
:   - 

![going](/images/img-18-19/going4.jpg){: .image-process-large}
:   - 

</div>

En savoir plus sur [GOING](https://goingband.com)
{: .pedagogique }
