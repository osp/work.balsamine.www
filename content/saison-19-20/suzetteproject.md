Title: Suzette project
Date: 2019/12/29
Dates: 2019/12/29 11:30
          2019/12/29 16:00
Slug: suzetteproject
Category: Noël au Théâtre
Age: 7 ans et plus
Key_image_detail_body: img-19-20/suzetteproject.jpg
Piece_author: Daddy Cie
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=150
Color: #ff7c48
Partner_logo: /images/logos/ctej.svg
              /images/banner_mail_juste_nat.jpg
Un spectacle qui interroge la vision de la famille et de ses différents modèles. Des vidéos documentaires se mêlent au jeu théâtral et gestuel afin de découvrir, à travers un gang de mômes rugissants, les méandres de la diversité, l’ouverture à la tolérance, la naissance de la résistance.

Suzanne, fan d’Al Pacino et des tigres de savanes, a une maman et une mamoune. Suzanne a aussi une best friend forever, Alice, dont les parents sont divorcés. Dans la cour de récré, elles partagent leurs rêves d’aventures et font des plans sur la comète. Mais tout bascule le jour où on vole et déchire en mille morceaux le poème que Suzanne avait écrit pour ses deux mamans. Elle se lance alors dans une grande cyber-enquête qui deviendra le Suzette Project.

Texte et mise en scène
: Laurane Pardoen

Distribution
: Marine Bernard De Bayser, Ophélie Honoré et Nina Lombardo en alternance avec Laurane Pardoen

Assistanat à la dramaturgie
: Judith Bouchier-Végis

Assistanat
: Isabella Locurcio et Vera Rozanova

Voix
: Andrea Fabi

Montage vidéo
: Marie Gautraud

Costumes
: Bleuenn Brosolo

Scénographie
: Zoé Tenret et Laurane Pardoen

Création sonore
: Shu BA

Création lumière
: Jérôme DEJEAN

Régie générale
: Fanny Boizard

<div class="galerie photos-spectacle" markdown=true>
![suzette](/images/img-19-20/suzette1.jpg){: .image-process-large}
:   © Alice Piemme

![etrange](/images/img-19-20/suzette2.jpg){: .image-process-large}
:   © Marine Dricot

![etrange](/images/img-19-20/suzette3.jpg){: .image-process-large}
:   © Marine Dricot


</div>
