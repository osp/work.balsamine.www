Title: Les pirates  ! Bon à rien, mauvais en tout
Date: 2017/12/20
Time: 15h00
Category: balsatoiles
Length: 1h28
Age: Pour tous, à partir de 6 ans
Piece_author: Peter Lord & Jeff Newitt
Event_type: Balsatoiles
Color: #f2d67b
Key_image_detail_body: Les-Pirates-Bon-a-rien-mauvais-en-tout-im.jpg
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=36
Le Capitaine des pirates entend bien remporter le Prix du Pirate de l'année, mais ce titre est également convoité par Black Bellamy et Cutlass Liz. Il part à l’aventure avec ses équipiers en couleurs : voyageant des paysages exotiques de Blood island aux rues embrumées de Londres, ils trouveront sur leur chemin reine diabolique et un jeune scientifique.

Un atelier créatif, animé par un artiste plasticien, est proposé de 14h à 15h.
(inscription obligatoire – nombre de place limité)


Projet réalisé avec le soutien du Fonds Baillet Latour, dans le cadre de l’initiative L’extrascolaire au cœur de l’intégration gérée par la Fondation Roi Baudouin.
{: .production }

[Voir la bande-annonce du film "Les pirates ! Bon à rien, mauvais en tout !"](http://www.allocine.fr/video/player_gen_cmedia=19280117&cfilm=43118.html)
{: .production }
