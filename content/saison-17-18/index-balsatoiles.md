Title: index
Date: 2015
Category: Balsatoiles
Slug: index_balsatoiles

Le temps de 6 mercredis après-midi. La Balsa se transforme en cinéma de quartier. Ces projections gratuites de films d’animation seront des moments privilégiés de partage. Chaque film sera précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre. Chaque séance se clôturera par un débat autour d’un goûter.

Entrée libre mais réservation souhaitée !

Projet réalisé avec le soutien du Fonds Baillet Latour, dans le cadre de l’initiative L’extrascolaire au cœur de l’intégration gérée par la Fondation Roi Baudouin.
