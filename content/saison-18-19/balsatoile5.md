Title: Arrietty, le petit monde des chapardeurs 
Date: 2019/04/24
Time: 15h00
Slug: balsatoile5
Category: balsatoiles
Length: 1h34
Age: pour tous à partir de 7 ans 
Piece_author: de Hiromasa Yonebayashi
Key_image:
Key_image_detail_body: img-18-19/Arrietty-2.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=96
Event_type: Projection gratuite
Color: #ffffff

Dans la banlieue de Tokyo, sous le plancher d'une vieille maison perdue au coeur d'un immense jardin, la minuscule Arrietty vit en secret avec sa famille. Ce sont des Chapardeurs. Arrietty connaît les règles : on n'emprunte que ce dont on a besoin, en tellement petite quantité que les habitants de la maison ne s'en aperçoivent pas. Plus important encore, on se méfie du chat, des rats, et interdiction absolue d'être vus par les humains sous peine d'être obligés de déménager et de perdre cet univers miniature fascinant fait d'objets détournés... Arrietty sait tout cela. Pourtant, lorsqu'un jeune garçon, Sho, arrive à  la maison pour se reposer avant une grave opération, elle sent que tout sera différent. Entre la jeune fille et celui qu'elle voit comme un géant, commence une aventure et une amitié que personne ne pourra oublier...

Chaque film sera précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=RYwYgH9uA_8) 
{: .production }

Prochaine séance : 
- le 15 mai
{: .pedagogique }
