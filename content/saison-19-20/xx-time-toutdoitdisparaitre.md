Title: Tout doit disparaître
Date: 2020/04/03
Slug: xx-time-tout-doit-disparaitre
Time: 19h30
Key_image_detail_body: img-19-20/ophelie_mac.png
Piece_author: Ophélie Mac <br/>Performance annulée  
Event_type: Performance - démo (+18 ans) <br/> Dans le cadre du XX Time <br/>entrée gratuite sur réservation
Status: draft
Color: #ffffff


Tout doit disparaître questionne non seulement la frontière entre l’art, le sexe et l’artisanat, mais aussi la sous-représentation des corps noirs dans le monde de l’art. La Balsamine invite l’artiste pour qu’elle effectue une démonstration type « tupperware » pour y présenter et vendre ses Encastrables :  des godemichets en céramique faits par ses soins. 
Elle vous explique pourquoi la céramique est fantastique et répond à vos questions techniques. Cash & Cartes acceptées.
Ophélie Mac, aka Mac Coco, est une artiste afro-féministe activiste qui se définit comme céramiste-performeuse. Depuis 2012 elle vit et travaille à Bruxelles où elle a ouvert il y a peu l’espace Fatsabbats, dans le quartier d’Anneessens. Un bar fait pour et par les femmes POC ( Person Of Color), qui se destine en priorité à la communauté queer. Son travail artistique questionne sa double culture, son identité métissée, ses influences religieuses, ses croyances, son intimité ainsi que sa relation au public.



[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)