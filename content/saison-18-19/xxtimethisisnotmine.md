Title: This is not mine
Date: 2019/03/18
Time: 19h
Slug: xx-time-thisisnotmine
Piece_author: Gaspard Rozenwajn
Event_type:  Worshop de twerk pour débutant.e.s
Status: draft
Color: #fe5000

« Le Twerk est un symbole très concret d’émancipation du corps. A coups de hanches et de fesses, la pratique du twerk procure un sentiment de libération et permet de renforcer l’estime de soi. Peut-être ne bougeons-nous pas assez cette partie si chère qu’est notre bassin ? C’est ce que je souhaite partager avec vous lors de ce workshop de 2 heures d’initiation à la danse Twerk. Aucune expérience nécessaire. Genouillères recommandées. » Gaspard Rozenwajn

