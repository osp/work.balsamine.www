Title:  Les trois brigands
Date: 2019/03/27
Time: 15h00
Slug: balsatoile4
Category: balsatoiles
Length: 1h29
Age: pour tous à partir de 5 ans 
Piece_author: de Hayo Freitag
Key_image:
Key_image_detail_body: img-18-19/les trois brigrands.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=95
Event_type: Projection gratuite
Color: #ffffff

La petite Tiffany traverse une sombre forêt à bord d'une diligence qui la conduit vers son nouveau foyer, un orphelinat dirigé par une tyrannique directrice. C'est alors que l'attelage se fait attaquer par les “maîtres de la forêt » : trois brigands à l'air patibulaire, portant de vastes manteaux et de grands chapeaux noirs. Mais l'audacieuse Tiffany est enchantée de cette rencontre : elle sent tenir là sa chance pour échapper à l'orphelinat. À l'aide de quelques stratagèmes et de quelques mensonges (!), elle parvient à se faire enlever par les trois brigands. La petite fille va totalement chambouler leur vie. Tiffany, parviendra-t-elle à convaincre nos compères de recueillir tous les malheureux orphelins du royaume ? Réussira-t-elle à transformer nos trois redoutables voleurs de grand chemin en gentils papas ?
Entrée libre mais réservation souhaitée !

Chaque film sera précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](http://www.allocine.fr/video/player_gen_cmedia=18776880&cfilm=129227.html) 
{: .production }

Prochaines séances : 
- le 24 avril
- le 15 mai
{: .pedagogique }
