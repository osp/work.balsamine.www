Title: Pattern
Date: 2018/06/15
Dates: 2018/06/15 20h30
       2018/06/16 20h00
Piece_author: Emilie Maréchal / Camille Meynard
Event_type: Théâtre documentaire
Key_image_detail_body: visuel vertical n&b.png
Status: draft
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=71
Subhead: Les réservations pour les soirées combinées se font uniquement par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68

**Pattern** est une expérience scénique et cinématographique qui nous conduit à une réflexion sur la filiation.
Nous traversons le réel pour questionner et regarder le père, notre père.
Cette histoire qui mêle le cinéma documentaire et le théâtre est entièrement constituée de récits réels.

Que reste-t-il après la mort d’un père ? Il ne reste que la mémoire. 
Un homme est fait de mémoire, s’il n’est pas fait de mémoire, il n’est fait de rien.

Concept, écriture, réalisation et mise en scène 
: Emilie Maréchal et Camille Meynard

Avec 
: Simon André

Scénographie
: Sylvain Descazot 


