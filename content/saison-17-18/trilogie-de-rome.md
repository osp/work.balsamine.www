Title: Trilogie de Rome
Date: 2018/04/23
End_date: 2018/04/27
Time: 20h30
Piece_author: Ludovic Drouet
Event_type: Création théâtre
Key_image: shows/3-Trilogie-de-Rome/renders-hd.png
Key_image_detail_body: shows/3-Trilogie-de-Rome/lupa-capitolina-right-profile.png
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=27&cntnt01returnid=44
Intro: Qu’est devenue l’antiquité sinon une surface de projection ? L’histoire d’un crime qui s’est trouvé une scène.
Color: #c6a545

On dit que César dans sa jeunesse ayant vu une statue d'Alexandre le Grand s'était mis à pleurer en répétant : " A mon âge il avait déjà conquis la moitié du monde. " Pauvre César qui se plaignait de n'avoir rien fait. Que dire de nous alors ? Nous sommes en 2018 et nous vivons toujours au temps de César. Les fils du XXIème siècle ont quitté leurs couffins. Ils ont désormais du poil au menton et, pour certains, la furieuse envie d'en découdre sur les sables mouvants d'un monde qui n'a pas encore payé le prix de sa légèreté. 

> «Peut-être avons-nous passé l’âge d’écrire sous différents modes les préludes d’une même catastrophe et peut-être sommes-nous encore trop jeunes pour en narrer les séquelles. Notre ère est donc l’ère du désastre. Le temps n’y existe que pour être dilaté et c’est cette nouvelle capacité de dilatation qui le définit et nous définit.»
> <footer>Ludovic Drouet</footer>


Ecriture
:   Ludovic Drouet

Interprétation
:   Ludovic Drouet 
:   Simon Espalieu
:   Lucas Meister
:   Nicolas Patouraux
:   Andreas Perschewski 
:   Lionel Ueberschlag


Mise en scène 
:   Ludovic Drouet
:   Julia Huet-Alberola

Création lumière 
:   Iris Julienne

Création sonore 
:   Noam Rzewski
:   Brice Agnès

Création vidéo
:  Sébastien Corbière

Création costumes et accessoires
:   Ludovic Drouet
:   Rita Belova

Construction
:   Sébastien Corbière
:   Anita Lemasne

Stagiaire régie plateau et machinerie
:   Lionel Ueberschlag

Régie
: Rémy Urbain

Une production du Théâtre la Balsamine avec le soutien de la Commission communautaire française de la Région bruxellloise - Fonds d'Acteurs. 
{: .production }

<div class="galerie" markdown=true>

![trilo1](/images/01trilo.jpg){: .image-process-large}

![trilo2](/images/trilo2.jpg){: .image-process-large}

![trilo3](/images/trilo3.jpg){: .image-process-large}

![trilo5](/images/trilo5.jpg){: .image-process-large}

![trilo6](/images/trilo6.jpg){: .image-process-large}

![trilo7](/images/trilo7.jpg){: .image-process-large}

![trilo8](/images/trilo8.jpg){: .image-process-large}

![trilo9](/images/trilo9.jpg){: .image-process-large}

![trilo10](/images/trilo10.jpg){: .image-process-large} 

![trilo11](/images/trilo11.jpg){: .image-process-large}

![trilo12](/images/trilo12.jpg){: .image-process-large} 

![trilo13](/images/trilo13.jpg){: .image-process-large} 

![trilo14](/images/trilo14.jpg){: .image-process-large} 

![trilo15](/images/trilo15.jpg){: .image-process-large} 

![trilo16](/images/trilo16.jpg){: .image-process-large} 

<iframe class="video" src="https://player.vimeo.com/video/265555334"></iframe>


</div>


