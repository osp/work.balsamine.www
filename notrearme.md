Title: Notre arme sera l'allégorie
Date: 2019/06/15 20h00
Slug: notre_arme
Key_image_detail_body:img-18-19/Visuel_Transitscape_pour_le_PIF.jpg
Piece_author: t.r.a.n.s.i.t.s.c.a.p.e / Pierre Larauza + Emmanuelle Vincent
Event_type: Performance - ~~sculpture~~ - maçonnerie - atelier - entraînement
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=28&cntnt01returnid=114
Subhead: Dans le cadre du PIF4
Color: #009933

"**On a tous un mur dans notre vie, qu’on a pas su franchir, qu’on ne sait pas franchir,qu’on ne pourra pas franchir.**"
 
Corps sportifs mesurés et corps usés cohabitent le temps d’un entraînement.
Et si on s’éduquait à sauter ? A dire “je peux” ? A tenter l’impossible ? Apprendre à sauter serait-il la clef pour déjouer les frontières ? 

Au programme&thinsp;:
{: .programme }
### 16h : Initiation à la Maçonnerie (1h) : tout public, tout niveau, gratuit
### 18h : Initiation au Saut en hauteur  (1h) : tout public, tout niveau, gratuit
### 20h : Performance - ~~sculpture~~ - atelier - entraînement
	                      maçonnerie

Avec 
:    Gaston Barbe, Antoine Bruart, Annie Burgheim, Jacky Deholn, Manuela Fiori, Gene Francart, Pierre Larauza, Emmanuelle Vincent

Soutien 
:    Théâtre la Balsamine

Remerciements à la maison de repos Les heures douces et au Royal Athletic Club Louvièrois.
{: .production }
