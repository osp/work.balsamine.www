Title: Looking for the putes mecs
Date: 2019/03/01
Slug: xx-time-looking
Translation: true
Lang: fr
End_date: 2019/03/02
Time: 20h30
Key_image: img-18-19/svg/xx-time-01.svg
Key_image_detail_body: img-18-19/svg/looking-01.svg
Piece_author: Diane Fourdrignier et Anne Thuot
Event_type: Reprise théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=85
Status: draft
Subhead: Les deux représentations sont complètes. Nous ouvrons une liste d'attente sur place, chaque soir, à partir de 19h.
Partner_logo:
Color: #fe5000

Deux femmes cherchent un péripatétitien, un "pute mec" et se confrontent à l'ignorance et aux clichés d'un désir formaté.Une interrogation de la place visible du désir féminin, une tentative d'invention de nouveaux imaginaires érotiques.

Avec
:   Diane Fourdrignier, Anne Thuot + Guests

Concept et réalisation
:   Diane Fourdrignier et Anne Thuot

Régie
:   Rémy Urbain

Photo
:   Pierre Debroux et Sara Sampelayo

Une production Fast ASBL. Avec le soutien du Théâtre la Balsamine , de l'Ancre - Théâtre Royal et du Théâtre des Doms.
{: .production }

Looking for the putes mecs a reçu le Prix du Spectacle Vivant 2017 attribué par le Comité belge de la SACD.
{: .production }

<div class="galerie" markdown=true>
![xx](/images/img-18-19/svg/looking-02-b.svg)
![xx](/images/img-18-19/svg/looking-03.svg)
![xx](/images/img-18-19/svg/looking-05.svg)
![xx](/images/img-18-19/svg/looking-06.svg)
![xx](/images/img-18-19/svg/looking-07.svg){: .demi}
![xx](/images/img-18-19/svg/looking-08.svg)
![xx](/images/img-18-19/svg/looking-09.svg)
![xx](/images/img-18-19/svg/looking-10.svg)
</div>

<div class="galerie photos-spectacle" markdown=true>
![looking](/images/img-18-19/lookingportrait.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/lookingauxdoms1.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/lookingauxdoms2.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/lookingauxdoms3.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/lookingauxdoms5.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/LookingSaraSampelayo1.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/LookingSaraSampelayo2.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/LookingSaraSampelayo3.jpg){: .image-process-large}
:   © Hichem Dahes

![looking](/images/img-18-19/LookingSaraSampelayo4.jpg){: .image-process-large}
:   © Hichem Dahes

</div>
