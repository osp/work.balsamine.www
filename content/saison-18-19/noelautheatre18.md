Title: Noël au Théâtre
Date: 2018/12/26
End_date: 2018/12/30
Key_image_detail_body: img-18-19/DEFNAT18_newID_02 (002).jpg
Color: #ffffff
Partner_logo: /images/logos/ctej.svg
              

La création « jeune public » est dans tous ses états à l’occasion du Festival Noël au Théâtre : des spectacles, des créations, des chantiers, des lectures et plusieurs animations débarquent du 26 au 30 décembre à Bruxelles.

Cette saison encore, la Balsamine accueille le festival et ouvre grandes ses portes à la magie et la chaleur du théâtre pour tous.

**[Respire - Théâtre de la Guimbarde](https://www.balsamine.be/noel-au-theatre/Respire.html)** :

> Les 28 et 29/12 à 11h30 - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=105)


**[Les Carnets de Peter - Théâtre du Tilleul](https://www.balsamine.be/noel-au-theatre/les-carnets-de-peter.html)** :

> Les 28 et 29/12 à 14h -[Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=82)

> Représentations hors Festival Noël au Théâtre : Le 27/12 à 19h30 et du 3 au 6/01 à 14h 
