Title: ANTIGONE, une autre histoire de l’Europe
Date: 2019/06/12 
Time: 21h30
Slug: antigone
Key_image_detail_body: img-18-19/ANTIGONE1__Gabrielle.Gillies.jpg
Piece_author: Vincent Collet - Théâtre de Poche / Le joli collectif
Event_type: Théâtre - Cirque - Diptyque théâtral - Work in progress
Subhead: Dans le cadre du PIF4
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=115
Status: draft
Color: #009933

Qu’est-ce qui trouble et anime les dirigeants d’un pouvoir en pleine crise, dans les discussions off ou dans les échanges publics - parfois violents ?
Pour ce deuxième volet, nos 4 personnages s’engagent dans un véritable marathon du politique et nous entraînent dans les coulisses du pouvoir au XXIème siècle, au cœur-même du Parlement européen. 
 

**ANTIGONE** constitue le second  volet d’un travail en trois parties autour du pouvoir. Vincent Collet a décidé d’entamer une étude du pouvoir, plus sensible, moins rationnelle mais éclairante, à travers trois fables : Aveugles, centré autour du groupe, de l’immobilisme et de la délibération, Antigone, questionnant l’exécutif dans l’urgence de l’action, et Projet Justice, autour du droit, de la jurisprudence et des normes à venir.
Dans le cadre du PIF, nous vous proposons de découvrir AVEUGLES et ANTIGONE à la suite. 
{: .production }

Concept et mise en scène 
: Vincent Collet avec la collaboration de Pierre Déaux

Écriture et jeu 
: Marie-Lis Cabrières, Vincent Collet, Fanny Fezans, Vincent Voisin

Création lumière 
:    Florian Leduc

Costumière 
:    Angèle Micaux

Scénographie 
:    Marine Brosse

Technicien lumière et son 
:    Ewen Toulliou

Conseils dramaturgiques 
:    Julie Valéro

Production/Diffusion 
:    Elisabeth Bouëtard et Marion Le Guerroué

Production 
:    Théâtre de Poche / Le joli collectif – Scène de territoire pour le théâtre, Bretagne romantique & Val d’Ille - Aubigné

Coproducteurs (en cours de recherche) 
:    Le Canal - théâtre du Pays de Redon / scène conventionnée pour le théâtre, Le TU-Nantes / scène conventionnée jeune création et émergence et le Théâtre de la Balsamine - Bruxelles 

Avec le soutien à la résidence de La Maison du Théâtre – Brest, du Vivat / Scène conventionnée art et création – Armentières et La Bellone / maison du théâtre – Bruxelles (résidence de recherche 2018)
Ce projet a reçu l’aide à la production du Ministère de la Culture - DRAC de Bretagne.
{: .production }
