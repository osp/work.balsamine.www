Title: Les fortunes de la viande
Date: 2018/01/30
End_date: 2018/02/10
Time: 20h30 <br>(relâche le dimanche)
Key_image: shows/4-Les-Fortunes-de-la-Viande/sheep-weightpaint2.png
Key_image_detail_body: shows/4-Les-Fortunes-de-la-Viande/anim-moutmout.gif
Piece_author: Martine Wijckaert
Event_type: Création théâtre
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=23&cntnt01returnid=44
Intro: Un aller simple pour le trou du cul du monde. Bienvenue dans l’anus planétaire.
Color: #ecb9b3

Les représentations sont complètes. Nous ouvrons une liste d'attente chaque soir à l'ouverture des portes dès 19h. 

Un aller simple pour le trou du cul du monde. Bienvenue dans l’anus planétaire.

**Les fortunes de la viande** narrent les tribulations existentielles d’un boucher-charcutier, de sa femme et du confesseur de celle-ci, que rejoint à l’impromptu Diane Chasseresse, déboulée pour ainsi dire dans ce marasme métaphysico-trivial. Au fil de ce texte épique, où s’interpénètrent épisodes quotidiens des plus triviaux et mouvements de l’âme parfaitement irrationnels, c’est la métaphysique obscure du quotidien qui est en action. Car les personnages aspirent invariablement au chaos, mus par une volonté d’incorrection absolue visant à briser tous les canons sociétaux.


Avec
:   Marie Bos
:   Héloïse Jadoul
:   Claude Schmitz
:   Alexandre Trocki

Écriture et mise en scène
:   Martine Wijckaert

Assistante à la mise en scène
:   Astrid Howard

Scénographie
:   Valérie Jung

Lumières
:   Stéphanie Daniel

Costumes et accessoires
:   Laurence Villerot

Image vidéo
:   Jacques André

Création son
:   Thomas Turine

Direction technique
:   Fred Op de Beeck

Régie spectacle
:   Mathieu Bastyns

Une production du Théâtre la Balsamine en coproduction avec La Coop asbl, avec le soutien de Shelterprod, Taxshelter.be, ING et du Tax-Shelter du gouvernement fédéral belge.
{: .production }

[Suivre l'actualité de Martine Wijckaert](http://www.martine-wijckaert.be/).
{: .production }


**Autour du spectacle**

**AFTER SHOW**
Mercredi 31 janvier : à l’issue de la représentation, nous proposons un échange entre le public et l’équipe artistique. Petite particularité, cette rencontre est menée par Ludovic Drouet, autre artiste de la saison choisi par Martine Wijckaert.
{: .production }

**Chambre d'écoute** 
organisée par RED /Laboratoire pédagogique

Samedi 3 février à 18h
{: .production }

Une chambre d'écoute spéciale « Boucherie » #70 Désossage Tendre :
{: .production }

Présentation du fanzine Désossage Tendre #2 suivie de la Chambre d'écoute avec inserts de poésie sonore: Les carnassiers et Fragments de véganismes (monologues de Sybille Cornet)
{: .production }

[Plus d’info sur le collectif](http://www.redlaboratoirepedagogique.be)
{: .production }


<div class="galerie" markdown=true>
![les fortures de la viande](/images/shows/4-Les-Fortunes-de-la-Viande/agneau-wireframe.png){: .image-process-large}

![les fortures de la viande](/images/shows/4-Les-Fortunes-de-la-Viande/SheepDiffuseSkin-Contours.png){: .image-process-large}

![les fortures de la viande](/images/shows/4-Les-Fortunes-de-la-Viande/sheep-helpers.png){: .image-process-large}

![fortunes1](/images/fortunes_viande1.jpg){: .image-process-large}

![fortunes2](/images/fortunes_viande_2_1.jpg){: .image-process-large}

![fortunes3](/images/fortunes_viande5.jpg){: .image-process-large}

![fortunes4](/images/fortunes_viande8.jpg){: .image-process-large}

![fortunes5](/images/fortunes_viande9.jpg){: .image-process-large}

![fortunes7](/images/fortunes_viande13.jpg){: .image-process-large} 

![fortunes8](/images/fortunes_viande18.jpg){: .image-process-large} 

![fortunes9](/images/fortunes_viande21.jpg){: .image-process-large} 

<iframe class="video" src="https://player.vimeo.com/video/252485904"></iframe>

<iframe class="video" src=https://player.vimeo.com/video/252680642?title=0&byline=0&portrait=0></iframe>

</div>
