Title: home
save_as: index.html
status: hidden
Template: home


<section markdown=true class="first-column">
#saison 2019-&#8203;2020
</section>

<section markdown=true class="moyenne-colonne moyenne-rangee">
##À venir
###Stage de théâtre : "Le bonheur dans tous ses états"<br/>
animé par François Pinte<br/>
Du 24 au 28 août / âge : de 12 à 15 ans<br/>
[réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=179)<br/>
[plus d'infos](https://www.balsamine.be/reporte/Stage.html)
![stage](/images/img-19-20/visuel_stage_pâques.jpg)
</section>

<section markdown=true class="petite-colonne moyenne-rangee">

##Mise en lumière de la Balsamine par CultureTogether : entretien avec notre directrice, Monica Gomes.

CultureTogether est un concept imaginé et animé par les agences Movietown et BE CULTURE

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FCulturetogetherBE%2Fvideos%2F254740222273589%2F&show_text=0&width=267" width="267" height="476" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>

</section>

<section markdown=true class="moyenne-colonne moyenne-rangee">

##Appel à votre mémoire 
En juin 2021, la Balsamine va fêter ses 40 ans d’implantation dans les casernes Dailly. Nous sommes en pleine préparation de ce Balsanniversaire. Pour amorcer cet événement, nous réalisons une petite enquête et sollicitons votre mémoire avec cette question :
Quel est le spectacle qui vous a le plus marqué à la Balsa ? votre coup de coeur ? Vous pouvez nous en donner plusieurs, et même partager avec nous votre top 3, 5 ou 10 ! Tout est ok !
Envoyez-nous vos réponses à l'adresse : [info@balsamine.be](mailto:info@balsamine.be)
D’avance merci ! Nous sommes fort curieux.ses 

![historique 2](/images/historique2.jpg){: .image-process-large}
:   © Danièle Pierre

![location](/images/img-18-19/Grande_Salle.jpg){: .image-process-large}
:   © Hichem Dahes
</section>

