Title: How to welcome the Aliens
Date: 2020/03/21
End_date: 2020/03/22
Time: 19h00
Slug: howtowelcomethealiens
Event_type: Cirque  
Category: annulé
Color: #6b89ed
Partner_logo: /images/logos/logo-festival-up-neutre-blanc.png
Piece_author: Cie EA EO <br/> Festival up <br/> Spectacle annulé
Key_image: /img-19-20/HOW-TO-WELCOME-THE-ALIENS_Cie-EAEO_Festival-UP-2020_Robin-Montrau-10-683x1024.jpg
Key_image_detail_body: /img-19-20/HOW-TO-WELCOME-THE-ALIENS_Cie-EAEO_Festival-UP-2020_Robin-Montrau-10-683x1024.jpg


Une cérémonie jonglée pour nous préparer à l'arrivée des Aliens sur Terre.
Imaginons que des extraterrestres débarquent sur notre vieille boule (environ) bleue. Comment leur expliquer la passion de la jonglerie ? C’est sur la base de cette délirante question que Jay Gilligan et Éric Longequel ont conçu un spectacle qui a tout d’un ovni. « Que pourrions-nous offrir à boire et à voir à nos invités pour
qu’ils se sentent comme chez eux ? », se demande très sérieusement ce duo de haut vol. Numéros de jonglage, cotillons, ballons, bonbons : un cocktail explosif qui en profite pour expliquer la vie sur Terre, son histoire, sa
matière, sa gravité, sa (bonne) atmosphère.

DE & AVEC
: Jay Gilligan & Eric Longequel

CRÉATION LUMIÈRES ET SCÉNOGRAPHIE 
: Thibault Condy 

avec l'aide de 
: Josefin Hinders

MUSIQUE 
: David Maillard

COSTUMES 
: Marion Duvinage

DRAMATURGIE 
: Andy Kaufman

CONSEILLER TECHNIQUE 
: Greg Dubos

REGARD EXTÉRIEUR
: Johan Swartvagher

SPEECH 
: Jimmy Carter lu par Neta Oren & Ivar Heckscher

BALLES À ONDES GRAVITATIONNELLES 
: Ameron Rosvall

BALLES DÉSINTÉGRABLES ET BRODERIE 
: Ivar Heckscher

PRODUCTION 
: Association Asin

DIFFUSION 
: Laure Caillat

COPRODUCTIONS 
: Maison des Jonglages [FR], La Verrerie - Alès [FR], Cité du Cirque - Pôle Cirque du Mans [FR], Le PALC - Châlons-en-Champagne [FR]

ACCUEILS EN RÉSIDENCE & SOUTIENS 
: Espace Catastrophe - Centre International de Création des Arts du Cirque [BE], Le PALC - Pôle National Cirque en préfiguration - Grand Est - Châlons-en-Champagne [FR], Maison des Jonglages - Centre Culturel Jean
Houdremont La Courneuve [FR], PNC Occitanie La Verrerie d'Alès [FR], La Cascade - PNC - Ardèche Auvergne-Rhône-Alpes de Bourg Saint Andéol [FR], Cité du Cirque pour le Pôle régional Cirque Le Mans [FR]

SUBVENTIONNÉS PAR 
: Direction régionale des affaires culturelles d'Île-de-France [FR], Ministère de la Culture et de la Communication
