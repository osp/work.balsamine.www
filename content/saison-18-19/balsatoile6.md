Title: Ernest et Célestine 
Date: 2019/05/15
Time: 15h00
Slug: balsatoile6
Category: balsatoiles
Length: 1h28
Age: pour tous à partir de 5 ans 
Piece_author: de Stéphane Aubier, Vincent Patare et Benjamin Renne
Key_image:
Key_image_detail_body:img-18-19/Ernest_Et_Celestine_4.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=97
Event_type: Projection gratuite
Color: #ffffff

Dans le monde conventionnel des ours, il est mal vu de se lier d'amitié avec une souris. Et pourtant, Ernest, gros ours marginal, clown et musicien, va accueillir chez lui la petite Célestine, une orpheline qui a fui le monde souterrain des rongeurs. Ces deux solitaires vont se soutenir et se réconforter, et bousculer ainsi l'ordre établi... 

Chaque film sera précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=UJ1rmOYLr2U) 
{: .production }


