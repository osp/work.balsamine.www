 Title: Crédits images
Date: 2019/06/01
Translation: true
Lang: fr
Slug: credits
status: hidden


## Chapitre 1

![](/images/img-19-20/programme/chapitre-1/1280px-Hamburg_-_Klubhaus_St._Pauli.jpg)
source : https://commons.wikimedia.org/wiki/File:Hamburg_-_Klubhaus_St._Pauli_(2).jpg, auteur·e·s :Fred Romero, CC BY 2.0
{: .credits}

![](/images/img-19-20/programme/chapitre-1/1280px-Hamburg_-_Klubhaus_St._Pauli.jpg)
source : https://commons.wikimedia.org/wiki/File:Hamburg_-_Klubhaus_St._Pauli_(2).jpg, auteur·e·s :Fred Romero, CC BY 2.0
{: .credits}

## Chapitre 2

## Chapitre 3

![](images/img-19-20/programme/chapitre-3/229853121_c9996696c2_o-reuse-noncomercial.jpg)
source : https://www.flickr.com/photos/x-ray_delta_one/4775747903/in/photostream/, auteur·e·s : James Vaughan, CC BY-NC-SA
{: .credits}

![](images/img-19-20/programme/chapitre-3/229853255_9eb9846d69_o.jpg)
source : https://www.flickr.com/photos/x-ray_delta_one/4776383372/in/photostream/, auteur·e·s : James Vaughan, CC BY-NC-SA
{: .credits}

![](images/img-19-20/programme/chapitre-3/229853255_9eb9846d69_o.jpg)
source : https://commons.wikimedia.org/wiki/File:%22Loudon_Hospital,_Livingstonia%22,_Malawi,_ca.1910_(imp-cswc-GB-237-CSWC47-LS4-1-036).jpg, auteur·e·s : Inconnu·e·s, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/Widows_Walk_Gaithersburg.jpg)
source : https://commons.wikimedia.org/wiki/File:Widows_Walk_Gaithersburg.jpg, auteur·e·s : Jim Kuhn, CC BY 2.0
{: .credits}

![](images/img-19-20/programme/chapitre-3/Zaragoza_-_Graffiti_-_Technologia_omnipotens_regnat.JPG)
source : https://commons.wikimedia.org/wiki/File:Zaragoza_-_Graffiti_-_Technologia_omnipotens_regnat.JPG, auteur·e·s : Ecelan, CC BY-SA
{: .credits}


![](images/img-19-20/programme/chapitre-3/32007777341_384fc2b361_o.jpg)
source : https://en.wikipedia.org/wiki/Arthur_Mole#/media/File:Woodrow_Wilson_(1918).jpg, auteur·e·s : Arthur Samuel Mole, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/iss004-308-010.jpg)
source : https://archive.org/details/iss004-308-010, auteur·e·s : NASA, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/31316292823_6d849bd2c4_o.jpg)
source : https://publicdomainreview.org/collections/the-living-photographs-of-mole-and-thomas/, auteur·e·s : Arthur Samuel Mole, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/31287478154_1447ffb6a8_o.jpg)
source : https://publicdomainreview.org/collections/the-living-photographs-of-mole-and-thomas/, auteur·e·s : Arthur Samuel Mole, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/iss004-308-010.jpg)
source : https://archive.org/details/iss004-308-010, auteur·e·s : NASA, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/TNM_ZEISS_prism_binoculars_20171028_0198_0000.jpg)
source : https://archive.org/details/TNM_ZEISS_prism_binoculars_20171028_0198, auteur·e·s : Inconnu·e·s, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/42804162581_789a9e1ffc_o.jpg)
source : https://www.flickr.com/photos/tranbc/42804162581/, auteur·e·s : B.C. Ministry of Transportation and Infrastructure, CC BY-NC-ND
{: .credits}

![](images/img-19-20/programme/chapitre-3/3330430516_cbe181e581_o.jpg)
source : https://www.flickr.com/photos/quelquepartsurlaterre/3330430516/, auteur·e·s : quelquepartsurlaterre Camille Bernier, CC BY-NC
{: .credits}

![](images/img-19-20/programme/chapitre-3/Camille_Bernier_L'hiver_en_bretagne.jpg)
source : https://fr.wikipedia.org/wiki/Fichier:Camille_Bernier_L%27hiver_en_bretagne.jpg, auteur·e·s : Moreau.henri Camille Bernier, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/Camille_Bernier_Chevaux_près_des_châtaigniers_de_Kerlagadic.jpg)
source : https://commons.wikimedia.org/wiki/File:Camille_Bernier_Chevaux_pr%C3%A8s_des_ch%C3%A2taigniers_de_Kerlagadic.jpg, auteur·e·s : Moreau.henri Camille Bernier, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/Camille_Bernier_Chevaux_près_des_châtaigniers_de_Kerlagadic.jpg)
source : https://commons.wikimedia.org/wiki/File:Camille_Bernier_Chevaux_pr%C3%A8s_des_ch%C3%A2taigniers_de_Kerlagadic.jpg, auteur·e·s : Moreau.henri Camille Bernier, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/zebras_black_and_white_animals_wild_striped_horses-1151337.jpg!d.jpeg)
source : https://pxhere.com/fr/photo/1151337, auteur·e·s : Moreau.henri Camille Bernier, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/13530338413804.png)
source : http://www.publicdomainfiles.com/show_file.php?id=13530338413804, auteur·e·s : 10binary, domaine public
{: .credits}


![](images/img-19-20/programme/chapitre-3/giraffe-skin-pattern-14425725159QM.jpg)
source : https://www.publicdomainpictures.net/fr/view-image.php?image=129460&picture=motif-girafe-de-peau, auteur·e·s : Petr Kratochvil, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-3/pexels-photo-1172187.jpeg)
source : https://www.pexels.com/fr-fr/photo/1172187/, auteur·e·s : Marisa Zuniga, Pexels
{: .credits}


![](images/img-19-20/programme/chapitre-3/poule-de-face.jpg)
source : https://animaux.l214.com/poules-pondeuses/hors-elevage/poule-de-face, auteur·e·s : L214 - Éthique & animaux, CC BY 3.0
{: .credits}


## Chapitre 4

## Chapitre 5

## Chapitre 6

![](images/img-19-20/programme/chapitre-6/37227a722d193c0d757daa74532a314a.png)
source : https://commons.wikimedia.org/wiki/File:Thos._W._Keene_in_Hamlet.png, auteur·e·s : United States Library of Congress's Prints and Photographs division, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-6/'The_Carpathian_Castle'_by_Léon_Benett_27.jpg)
source : https://fr.wikisource.org/wiki/Fichier:%27The_Carpathian_Castle%27_by_L%C3%A9on_Benett_27.jpg, auteur·e·s : Léon Benett, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-6/Ballet_Goes_To_the_Factory_D14048.jpg)
source : https://commons.wikimedia.org/wiki/File:Ballet_Goes_To_the_Factory-_Dance_and_Entertainment_Organised_by_the_Council_For_the_Encouragement_of_Music_and_the_Arts,_England,_1943_D14048.jpg, auteur·e·s : Ministry of Information Photo Division Photographer of England, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-6/Interieur_toneelzaal,_coulisse_-_Amsterdam_-_20384265_-_RCE.jpg)
source : https://commons.wikimedia.org/wiki/File:Interieur_toneelzaal,_coulisse_-_Amsterdam_-_20384265_-_RCE.jpg, auteur·e·s : A. J. van der Wal pour le Rijksdienst voor het Cultureel Erfgoed, CC BY-SA 4.0
{: .credits}

![](images/img-19-20/programme/chapitre-6/Coulisse_arrière_scene.jpg)
source : https://fr.wikipedia.org/wiki/Fichier:Coulisse_arri%C3%A8re_scene.jpg, auteur·e·s : Anthony Rauchen, CC BY-SA 2.0
{: .credits}

![](images/img-19-20/programme/chapitre-6/59010071_German_Whores_in_Backstage,_Berlin_2001.JPG)
source : https://commons.wikimedia.org/wiki/File:59010071_German_Whores_in_Backstage,_Berlin_2001.JPG, auteur·e·s :  Julica da Costa , CC BY-SA 2.5 + 2.0 + 1.0 et GNU Free Documentation License
{: .credits}

![](images/img-19-20/programme/chapitre-6/4ab45e326b014599ce07dcc9b4dcf20f.jpg)
source : https://fr.wikipedia.org/wiki/Fichier:Jos%C3%A9_Jim%C3%A9nez_Aranda_-_Figaro%27s_Shop_-_Walters_374.jpg, auteur·e·s : Walters Art Museum, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-6/The_theatre_through_its_stage_door.jpg)
source : https://commons.wikimedia.org/wiki/File:The_theatre_through_its_stage_door_(1919)_(14764870465).jpg, auteur·e·s : Internet Archive Book Images, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-6/laundry_dry_dry_laundry_clothing_hang_hang_hang-556089.jpg!d.jpeg)
source : https://pixabay.com/fr/photos/lave-linge-sec-s%C3%A8che-linge-1559231/, auteur·e·s : Internet Archive Book Images, Pixabay License
{: .credits}

![](images/img-19-20/programme/chapitre-6/Intérieur_du_chalet_du_Mollard-1.jpg)
source : https://commons.wikimedia.org/wiki/File:Int%C3%A9rieur_du_chalet_du_Mollard-1_(2017).jpg, auteur·e·s : B. Brassoud, Pixabay License
{: .credits}

![](images/img-19-20/programme/chapitre-6/Intérieur_du_chalet_du_Mollard-1.jpg)
source : https://commons.wikimedia.org/wiki/File:Int%C3%A9rieur_du_chalet_du_Mollard-1_(2017).jpg, auteur·e·s : B. Brassoud, CC BY-SA 4.0
{: .credits}

![](images/img-19-20/programme/chapitre-6/800px-Ready_to_eat_microwave_food_TV_dinner_Currywurst_with_French_fries.JPG)
source : https://fr.wikipedia.org/wiki/Fichier:Ready_to_eat_microwave_food_(TV_dinner)_Currywurst_with_French_fries.JPG, auteur·e·s : User:Mattes, CC BY-SA 2.0
{: .credits}

![](images/img-19-20/programme/chapitre-6/Henri_Pille_-_Au_bal_de_l'Opéra_2.jpg)
source : https://commons.wikimedia.org/wiki/File:Henri_Pille_-_Au_bal_de_l%27Op%C3%A9ra_2.jpg, auteur·e·s : Henri Pille, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-6/Living_statue_barcelona.jpg)
source : https://commons.wikimedia.org/wiki/File:Living_statue_barcelona.jpg, auteur·e·s : Scorpio835, CC BY-SA 4.0
{: .credits}

![](images/img-19-20/programme/chapitre-6/Living_statue_Vienna.jpg)
source : https://commons.wikimedia.org/wiki/File:Living_statue_Vienna.jpg, auteur·e·s : Gveret Tered, CC BY-SA 3.0 et GNU Free Documentation License
{: .credits}

![](images/img-19-20/programme/chapitre-6/Platon_Cave_Sanraedam_1604.jpg)
source : https://fr.wikipedia.org/wiki/All%C3%A9gorie_de_la_caverne#/media/Fichier:Platon_Cave_Sanraedam_1604.jpg, auteur·e·s : Jan Saenredam, domaine public
{: .credits}

![](images/img-19-20/programme/chapitre-6/Mosaic_depicting_theatrical_masks_of_Tragedy_and_Comedy_Thermae_Decianae.jpg)
source : https://fr.m.wikipedia.org/wiki/Fichier:Mosaic_depicting_theatrical_masks_of_Tragedy_and_Comedy_(Thermae_Decianae).jpg, auteur·e·s : Carole Raddato, CC BY-SA 2.0
{: .credits}
