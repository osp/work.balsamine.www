Title: Valhalla ou le crépuscule des dieux
Date: 2018/11/21
Slug: valhalla
End_date: 2018/11/23
Time: 20h30
Key_image: img-18-19/svg/valhalla-01.svg
Key_image_detail_body: img-18-19/svg/valhalla-01.svg
Piece_author: Petri Dish / Anna Nilsson / Sara Lemaire
Event_type: Création cirque
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=80
Color: #913291

Le crépuscule, un bateau pris dans la glace. Le naufrage d'un équipage qui sombre dans la solitude et la folie.Il s'y joue du cirque, du chant, de la danse et de la cornemuse. Les esprits s'échauffent. La mutinerie commence !

Avec
:   Joris Baltz, Viola Baroncelli, Thomas Dechaufour, Laura Laboureur, Carlo Massari, Anna Nilsson et Jef Stevens

Concept et mise en scène
:   Anna Nilsson et Sara Lemaire

Création lumière
:   Philippe Baste

Technique
:   Tonin Bruneton, Cristian Gutierrez et Camille Rolovic


Une production de Petri Dish et d’I.S.E. asbl en coproduction avec Le Groupe des 20 Théâtres en Île-de-France, le Théâtre la Balsamine, Theater Op De Markt – Dommelhof et le Centre Culturel du Brabant Wallon. Avec l’aide de la Fédération Wallonie-Bruxelles, service du cirque, Kulturradet/Swedish Arts Council et du Konstnärsnämnden - the Swedish Arts Grants Committee. Avec les soutiens du Circuscentrum, du Théâtre Marni, du Cirkus Cirkör, de Wolubilis, de Latitude 50, de l’Espace Catastrophe - Centre International de Création des Arts du Cirque, de Subtopia et de Dansens Hus.
{: .production }

En savoir plus sur [La compagnie Petri Dish](https://www.petridish.be/)
{: .pedagogique }


<div class="galerie" markdown=true>
![valhalla](/images/img-18-19/svg/valhalla-02.svg)
![valhalla](/images/img-18-19/svg/valhalla-03.svg)
![valhalla](/images/img-18-19/svg/valhalla-04.svg)
</div>

<div class="galerie photos-spectacle" markdown=true>
![valhalla](/images/img-18-19/Valhalla1.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla2.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla3.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla4.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla5.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla6.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla7.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla8.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla9.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla10.jpg){: .image-process-large}
:   © Hichem Dahes

![valhalla](/images/img-18-19/Valhalla11.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe width="560" height="315" src="https://www.youtube.com/embed/fsSPWGazNlo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</div>
