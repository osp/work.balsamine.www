Title: De la poésie, du sport, etc.
Date: 2019/02/19
Slug: xx-time-poesie
Translation: true
Lang: fr
End_date: 2019/02/21
Time: 20h30
Key_image: img-18-19/svg/xx-time-01.svg
Key_image_detail_body: img-18-19/svg/poesie-01.svg
Piece_author: Fanny Brouyaux et Sophie Guisset
Event_type: Création danse <br/> Dans le cadre de Brussels, dance ! Focus on contemporary dance.
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=84
Partner_logo: /images/logos/brussels-dance.svg
Status: draft
Color: #fe5000

### De la poésie, du sport, etc.

Du seuil de l'intime à un possible sésame vers l'universel, deux jeunes femmes courent et se livrent bataille, cherchant la voie de l'émancipation. Semé d'embûches, ce parcours physique devient poétique et dramatique.

Avec
:   Fanny Brouyaux et Sophie Guisset

Dramaturgie
:   Claire Diez

Oeil extétieur - coach chorégraphique
:   Moya Michaël et Louise Vanneste

Créateur son
:   Christophe Rault

Créateur lumière
:   Raphaël Rubbens

Production exécutive 
:   TMTT a.s.b.l 

En coproduction avec Charleroi danse, avec les soutiens de la Fédération Wallonie Bruxelles - service de la Danse, du POLE-SUD - CDCN –Strasbourg, du Réseau Grand Luxe 2018, de Materiais diversos, du Théâtre la Balsamine, de Tanzhaus Zurich, de 3CL, du KVS, Le Vivat Armentières, du théâtre La Coupole et du Théâtre Varia et avec l'accompagnement de Grand Studio.
{: .production }

<div class="galerie" markdown=true>
![xx](/images/img-18-19/svg/poesie-03.svg)
![xx](/images/img-18-19/svg/poesie-04.svg)
![xx](/images/img-18-19/svg/poesie-05.svg)
![xx](/images/img-18-19/svg/poesie-06.svg)
![xx](/images/img-18-19/svg/poesie-07.svg)
![xx](/images/img-18-19/svg/poesie-08.svg)
</div>

<div class="galerie photos-spectacle" markdown=true>
![poesie](/images/img-18-19/poesie4.jpg){: .image-process-large}
:   © Julien Carlier

![poesie](/images/img-18-19/poesie1.jpg){: .image-process-large}
:   © Julien Carlier

![poesie](/images/img-18-19/poesie2.jpg){: .image-process-large}
:   © Julien Carlier 

![poesie](/images/img-18-19/poesie3.jpg){: .image-process-large}
:   © Julien Carlier

<iframe class="video" src=https://player.vimeo.com/video/316988924></iframe>
</div>

