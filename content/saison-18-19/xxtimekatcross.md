Title: Katcross
Date: 2019/03/22
Time: 22h
Slug: xx-time-katcross
Event_type:  Concert- duo electro pop 
Status: draft
Color: #fe5000

Au coeur de Katcross, une pulsation vitale, une alchimie voix / machines autour de laquelle gravitent une myriade de sons fourmillants, lumineux. Kat et Mat élaborent une synthpop qui n’appartient qu’à eux et qu’ils savent faire vivre sur la scène avec intensité, une musique de danse qui n’est pas un simple transport de surface pour night clubs, mais pose des questions essentielles, humaines profondément.

