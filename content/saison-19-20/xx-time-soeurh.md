Title: 7.B In my big fireworks I play to you the final bouquet
Date: 2020/03/27
Slug: xx-time-soeurh
End_date: 2020/03/28
Time: 20h30
Key_image_detail_body: img-19-20/programme/chapitre-7/soeurh.png
Piece_author: Les soeurs h <br/> Spectacle annulé 
Event_type: Concert performé <br/> Dans le cadre du XX Time
Status: draft
Color: #ffffff

Dans un entre-deux flottant, trop rose et trop pailleté, deux adolescentes et un musicien naviguent à vue.
Dans cet espace hybride où se mêlent musique live, vidéo, écriture et performance,  Les sœurs h explorent le passage de l’enfance à l’âge adulte comme un plongeon dans l’inconnu.

À travers la performance, les artistes questionnent les notions de transition et de construction identitaire, caractéristiques de l’adolescence. Période d’incertitudes, de tentatives, de désirs, d’ambivalences et de contradictions. Les sœurs h s’intéressent à ce désir de trouver sa propre voie, à ce rejet instinctif des schémas préétablis. Elles traduisent cette quête d’identité bouillonnante, quasi explosive, qui n’a pas peur de sortir des sentiers battus, pour y trouver son propre sens.
{: .push-down}

Vidéos, texte 
:   Les sœurs h (Marie Henry et Isabelle Henry)

Musique live 
:   Maxime Bodson

Performance 
:   Clara Petit-Jean et Jeanne Wehrlin

Scénographie 
:   Marie Szersnovicz

Lumières
: Laurence Halloy

Production 
:   Mortimer et les sœurs h, Perfect Shirley asbl. 

Soutiens 
: SACD, Ville de Lausanne, Casino Barrière Montreux, Pro Helvetia, Fondation suisse pour la culture. Partenaires La Bellone, Maison de Quartier de Chailly

[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)


<div class="galerie photos-spectacle" markdown=true>

![inmybig](/images/img-19-20/LES_SŒURS_H_margotmontigny-8886web.jpg){: .image-process-large}
:   © Margot Montigny / CCS

![inmybig](/images/img-19-20/LES_SŒURS_H_margotmontigny-8892web.jpg){: .image-process-large}
:   © Margot Montigny / CCS

![inmybig](/images/img-19-20/LES_SŒURS_H_margotmontigny-8907web.jpg){: .image-process-large}
:   © Margot Montigny / CCS

![inmybig](/images/img-19-20/LES_SŒURS_H_margotmontigny-891web.jpg){: .image-process-large}
:   © Margot Montigny / CCS

![inmybig](/images/img-19-20/LES_SŒURS_H_margotmontigny-8933web.jpg){: .image-process-large}
:   © Margot Montigny / CCS

![inmybig](/images/img-19-20/LES_SŒURS_H_margotmontigny-8922web.jpg){: .image-process-large}
:   © Margot Montigny / CCS

<iframe class="video" src=https://player.vimeo.com/video/393945437></iframe>

</div>

