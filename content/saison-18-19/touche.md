Title: "touchés" "perchés" "glorieux(ses)"
Date: 2018/11/12
Slug: touches-perche-glorieux
End_date: 2018/11/30
Time: 19h00 
category: Installation
Key_image_detail_body: img-18-19/touche1.png
Piece_author: Valérie Jung 
Event_type: Installation - entrée libre 
Subhead: les soirs de représentation
Color: #ffffff

de la même manière que je guettais

autrefois les bois flottés

et autres trésors rejetés par la mer

et que j'assemblais mentalement les

pièces entre elles avant de les cueillir

dans une ville 

un jour 

je suis tombée sur l'intérieur d'un piano

tristement échoué sur un trottoir

et j'ai vu là aussi

des "touchés"

des "perchés"

des "glorieux(ses)"

qui ne demandaient qu'à sortir pour 

vivre une vie nouvelle. 

les choses sont nées comme ça

s'imposant à moi 

je les ai laissées venir

Valérie Jung
{: .small }


En les voyant, serrés en rangs ordonnés, là, sur le toit de l'armoire, avec le regard ébahi ou goguenard, parfois éberlué, je pense à une bien aimable petite armée, fantaisiste et débonnaire, une douce petite armée, celle des gardiens du quotidien, dois-je dire gardiens, dois-je dire sentinelles, gardiens ou sentinelles, ils ou elles sont affables, que leur posture soit régalienne ou plébéienne, toujours astucieuse, faite de noires et de blanches, de marteaux encore tout vibrants de Chopin, affables, ils ou elles sont affables, modestes, casqués ou chapeautés, avec parfois un bec ou un long nez. Petite armée des simples et des gentils, mémoire benoîte de nos vieux arpèges et des coins de salon où sommeiller à l'automne mordoré avant le long hiver qui sera le berceau de nos printemps encore à vivre.
Gardiens, sentinelles, compagnons humbles et tenaces
{: .production }

Martine Wijckaert, artiste associée du Théâtre la Balsamine. Valérie Jung est sa collaboratrice à la sécnographie depuis de nombreuses années. 
{: .production }

<div class="galerie" markdown=true>


</div>
