Title: La lettre volée
Date: 2017/11/09
End_date: 2017/11/11
Time: 20h30
Subhead: En coproduction avec Ars Musica
Piece_authors: Denis Bosse
               Pascal Nottet
               Thomas Van Haeperen
               Frédéric Dussenne
Event_type: Création Vidéopéra<br/>En coproduction avec Ars Musica
Key_image: shows/6-La-Lettre-Volee/inkwell-hd.png
Key_image_detail_body: shows/6-La-Lettre-Volee/tiroir.png
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=21&cntnt01returnid=44
Intro: Une enquête palpitante à la recherche d’une lettre perdue. Lyrique et atypique.
Color: #aa928c
Partner_logo: /images/logos/ars-musica.svg
** La représentation supplémentaire du samedi 11 novembre à 17h30 est complète !**

Une enquête palpitante à la recherche d’une lettre perdue. Lyrique et atypique.

**La lettre volée**, opéra librement inspiré de la nouvelle éponyme d’Edgar Poe, convoque à la fois public, chanteurs et musiciens au cœur d’une action dramatique: la poursuite d’une lettre volée qui rend fous ceux qui la désirent.
Une vertigineuse, dramatique, musicale et existentielle mise en abîme.

Directeur artistique et compositeur
:    Denis Bosse

Librettistes
:    Pascal Nottet et Denis Bosse

Directeur musical
:    Thomas Van Haeperen

Conseiller artistique
: Pierre Thomas

Metteur en scène
:    Frédéric Dussenne

Scénorgraphie et vidéo
: Helga Dejaegher

Chanteurs
:    Nicolas Ziélinski (Contre-ténor — Dupin)
:    Sarah Defrise (Soprano — Le fou)
:    Xavier de Lignerolles (Ténor — Le préfet de Police)
:    Kris Belligh (Baryton Basse — Ministre)
:    Anne Matic (Mezzo Soprano — Reine)
:    Thomas Van Caekenberghe (Baryton — Narrateur)

Musiciens
:    Ensemble Sturm und Klang sous la direction de Thomas Van Haeperen avec la participation d’étudiants et professeur d’ARTS²
:    Justine Debeer (clarinette)
:    Amaury Geens (saxophones)
:    SzeFong Yeong (cor)
:    Jean-Louis Maton (percussions)
:    Olivier Douyez (accordéon)
:    Marion Lambert (piano)
:    Maxime Stasyk (violon 1)
:    Loris Douyez (violon 2)
:    Dominica Eyckmans (alto)
:    Catherine Lebrun (violoncelle)
:    Natacha Save (contrebasse)

Arts Visuels
:    Étudiants et professeurs d'ARTS²

Assistants scénographie et vidéo
:    Dimitri Baheux 
:    Emmanuel Selva

Une production Quart de Ton — Janine Al-Asswad en coproduction avec Ars Musica. Avec les soutiens de  : Théâtre la Balsamine, la Fédération Wallonie-Bruxelles — conseil de la musique contemporaine,de Sturm und Klang, du Forum des compositeurs et du Festival Loop . En collaboration avec ARTS², École Supérieure des Arts de Mons (Domaine Arts visuels — Musique et Théâtre).
{: .production }




<div class="galerie" markdown=true>
![la lettre volée](/images/shows/6-La-Lettre-Volee/regency-desk-2-transparent.png){: .image-process-large}

![la lettre volée](/images/shows/6-La-Lettre-Volee/pen-hd.png){: .image-process-large}

![la lettre volée](/images/shows/6-La-Lettre-Volee/letter.png){: .image-process-large}
</div>
