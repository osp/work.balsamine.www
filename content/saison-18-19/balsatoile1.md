Title: Zombillénium
Date: 2018/10/17
Time: 15h00
Slug: balsatoile1
Category: balsatoiles
Length: 78 minutes
Age: 8 ans et plus
Piece_author: Arthur de Pins & Alexis Ducord 
Key_image:
Key_image_detail_body: img-18-19/Zombilenium.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=92
Event_type: Projection gratuite
Color: #ffffff


Dans le parc d’attractions d’épouvante Zombillénium, les monstres ont le blues. Non seulement, zombies, vampires, loups garous et autres démons sont de vrais monstres dont l’âme appartient au Diable à jamais, mais en plus ils sont fatigués de leur job, fatigués de devoir divertir des humains consuméristes, voyeuristes et égoïstes, bref, fatigués de la vie de bureau en général, surtout quand celle-ci est partie pour durer une éternité... Jusqu'à l'arrivée d'Hector, un humain, contrôleur des normes de sécurité, déterminé à fermer l’établissement. Francis, le Vampire qui dirige le Parc, n’a pas le choix : il doit le mordre pour préserver leur secret. Muté en drôle de monstre, séparé de sa fille Lucie, et coincé dans le parc, Hector broie du noir... Et si il devenait finalement la nouvelle attraction phare de Zombillénium ? 

Entrée libre mais réservation souhaitée !

Chaque film sera précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://youtu.be/JZzFBwi9d48) 
{: .production }

Prochaines séances : 
- le 12 décembre 
- le 6 février 
- le 23 mars 
- le 24 avril
- le 15 mai
{: .pedagogique }
