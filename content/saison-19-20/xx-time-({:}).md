Title:({:})
Date: 2020/03/12
Slug: xx-time-imprononçable
End_date: 2020/03/13
Time: 20h30
Key_image_detail_body: img-19-20/Visuel_Imprononçable.jpg
Piece_author: Lorette moreau
Event_type: Performance scénique, plastique et sensorielle <br/> Dans le cadre du XX Time <br/> Les deux représentations sont complètes. Nous ouvrons une liste d'attente sur place, chaque soir. 
Status: draft
Color: #ffffff

({:}) est une performance scénique, plastique et sensorielle. On y trace une géographie singulière, celle du sexe féminin, depuis la vulve jusqu’au col de l’utérus. Trois comédiennes invitent les spectateurs et spectatrices à découvrir ce biotope particulier, un paysage vaste et vallonné qui frémit et palpite sur leur passage. Conçue comme une promenade des sens, la performance traverse plusieurs saisons et climats. Le sujet, éminemment politique, est ici abordé par l’observation sensitive de cette contrée encore trop méconnue, en s’écartant du dégoût, de la crainte ou de l’utilitarisme trop souvent associés au sexe féminin.

Conception 
: Jennifer Cousin, Céline Estenne, Caroline Godart, Charlotte Lippinois, Laurence Magnée, Réhab Mehal, Lorette Moreau et Salomé Richard 

Mise en scène 
: Lorette Moreau 

Installation plastique
: Charlotte Lippinois 

Jeu 
: Céline Estenne, Réhab Mehal, Salomé Richard 

Dramaturgie
: Caroline Godart 

Création lumières 
: Laurence Magnée 

Assistanat à la mise en scène et création sonore 
: Jennifer Cousin 

Production déléguée 
: Théâtre de Liège, DC&J Création avec le soutien du Tax Shelter du Gouvernement fédéral de Belgique et de Inver Tax Shelter 

Diffusion 
: L'amicale 

Soutien et résidences 
: le CORRIDOR, Kunstenwerkplaats Pianofabriek, Service provincial des arts de la scène/Fabrique de Théâtre, Centre Culturel de Chénée, Théâtre de la Montagne magique, BAMP, La Bellone et la Roseraie  

Avec l’aide de 
: la Fédération Wallonie-Bruxelles / Service des projets pluridisciplinaires

Prix Coup de Cœur des Jeunes – Festival Emulation 2019 – Théâtre de Liège
{: .production }

[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)
