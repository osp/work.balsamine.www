Title: L'étrange Noël de Mr Jack
Date: 2018/12/12
Time: 15h00
Slug: balsatoile2
Category: balsatoiles
Length: 76 minutes
Age: 7 ans et plus
Piece_author: Henry Selick
Key_image_detail_body: img-18-19/l'étrange noel de monsieur Jack.jpg
Event_type: Projection gratuite
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=93
Color: #ffffff

Jack Skellington, roi des citrouilles et guide de Halloween-ville, s'ennuie : depuis des siècles, il en a assez de préparer la même fête de Halloween qui revient chaque année, et il rêve de changement. C'est alors qu'il a l'idée de s'emparer de la fête de Noël...
Entrée libre mais réservation souhaitée !

Chaque film sera précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

La séance et l'atelier sont complets. 

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=9dCFxqGjVtU) 
{: .production }

Prochaines séances : 
le 6 février 
- le 23 mars 
- le 24 avril
- le 15 mai
{: .pedagogique }