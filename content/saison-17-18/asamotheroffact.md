Title: As a mother of fact
Date: 2018/06/12
End_date: 2018/06/13
Time: 21h00
Piece_author: notch company / oriane varak
Event_type: Danse-Théâtre
Key_image_detail_body: Asamof-Dommelhof-HD-64.jpg
Status: draft
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=66
Subhead: Les réservations pour les soirées combinées se font uniquement par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68
*As a Mother of fact* est un spectacle qui porte sur ces liens conscients et inconscients qui déterminent les relations entre femmes, à travers leur statut de fille, ou de mère.
Il est question d’aliénation, tantôt librement consentie, tantôt subie ou rejetée. Jusqu’où acceptons-nous d’être manipulées ?

Concept et mise en scène 
: Oriane Varak

Création et interprétation 
: Jenna Jalonen, Audrey Lucie Riesen / Gala Moody, Oriane Varak /Mercedes Dassy

Composition et musique live 
: Guillaume Le Boisselier

Création lumières
: Laurence Halloy

Régie
: Matthieu Vergez

Costumes 
: Lieve Meeussen

Regard extérieur
: Gala Moody

Dramaturgie
: Hildegard De Vuyst 

Construction table 
: Luc Cools (De Werf)

Développement 
: In Co-laBo (Programme de résidence / Residency program from les Ballets c de la b)

Coproduction
: TAKT Dommelhof (BE), De Werf (Bruges, BE)

Production déléguée 
: Théâtre la Balsamine

Soutien 
: Pudding asbl, Théâtre la Balsamine

Ce projet a reçu la bourse de la SACD pour la composition musicale
{: .production }