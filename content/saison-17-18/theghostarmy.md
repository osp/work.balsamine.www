Title: The Ghost Army
Date: 2018/06/12
Dates: 2018/06/12 20h30
       2018/06/16 23h00
Piece_author: Isabelle Bats / Boris Dambly 
Event_type: Installation - Performance
Key_image_detail_body: icono.jpg
Status: draft
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=68
Subhead: Les réservations pour les soirées combinées se font uniquement par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68



*La Ghost Army* était un bataillon des forces alliées pendant la Seconde Guerre mondiale, constituée essentiellement d’artistes recrutés dans le but de mener des opérations de diversion contre les troupes de l’axe.
Boris Dambly et Isabelle Bats nous proposent de mettre cette stratégie en œuvre afin de faire de la Balsamine une zone souveraine.
On parlera de lutte poétique, de poésie en acte et, finalement, d’acte de résistance et d’insoumission.

> NOUS VOULONS MONTER LA FACTION.<br>
> NOUS VOULONS MONTER LES ACTIONS.<br>
> NOUS VOULONS COMMENCER L’ENTRAÎNEMENT.<br>
> NOUS VOULONS RÉPERTORIER LES ARMES.<br>
> NOUS VOULONS RÉPERTORIER LES ÂMES.<br>
> NOUS VOULONS MENER AU DÉSASTRE.<br>
> REJOIGNEZ LA GHOST ARMY<br>
> MAINTENANT !


Conception et performance
: Isabelle Bats et Boris Dambly

Direction technique
: Cathy Peraux

Une Production de Boris Dambly avec le soutien du Théâtre la Balsamine  
{: .production }



