Title: Gen Z
Date: 2018/05/23
Dates: 2018/05/23 20h00
       2018/05/24 13h30
       2018/05/24 20h00
       2018/05/25 20h00
Piece_author: Salvatore Calcagno
Event_type: théâtre
Partner_logo: /images/logos/cocog.png
              /images/logos/parlement.jpg
              /images/logos/fwb gouvernement.jpg
              /images/logos/fwb.jpg 
Subhead: entrée libre, réservation conseillée au 02 735 64 68 ou par mail à l'adresse reservation@balsamine.be
Color: #ecb9b3

GEN Z, diminutif de "génération Z", est un spectacle documenté qui questionne la réalité de la jeunesse née après 1995.
Dans une démarche exploratrice, à travers des collaborations avec des écoles et des associations et via des enquêtes sur les espaces de vie de cette jeunesse européenne, Salvatore Calcagno a collecté les paroles de nombreux jeunes et saisi leurs regards.
En proie à de nouvelles formes de violence, en quête d’une singulière beauté, la génération Z, créatrice et actrice du monde de demain, a des choses à nous dire sur le monde d’aujourd’hui. 
Salvatore Calcagno réunit sur le plateau des comédiens professionnels et des jeunes, ouvrant un espace où peuvent exister leurs rêves, leurs questionnements, les réflexions qui les animent.
GEN Z célèbre les élans, les nouveaux langages et les projections de cette génération, en rendant sensible l’éphémère et l’intense de l’adolescence au-delà des murs et des codes du théâtre. 
Le spectacle est le fruit de la rencontre entre le metteur en scène Salvatore Calcagno et la classe de 7PC de l'Institut des Filles de Marie à Saint-Gilles.

Mise en scène 
: Salvatore Calcagno 

Écriture
: Salvatore Calcagno, Émilie Flamant et Antoine Neufmars

Avec
: Diogo Alves, Sara Badi, Aziz Delire, Fatoumata Diallo, Nisrine Harrak, Madalina Iolu, Wassima Jerrari, Narcisse Joao, Rayhane Kaapoun et Daniel Rampelo (un groupe de jeunes acteurs non-professionnels) et Raphaëlle Corbisier, Egon Di Mateo, Émilie Flamant, Pauline Guigou Desmet, Manon Joannoteguy, Antoine Neufmars et Sophia Leboutte

Création vidéo 
: Zeno Graton

Caméraman
: Simon Fascilla

Scénographie et lumière
: Simon Siegmann

Assistante scénographie et lumières
: Angela Massoni

Collaboratrice artistique
: Sofie Kokaj

Conseiller dramaturgie
: Sébastien Monfé

Aide à l’élaboration de l’environnement sonore
: Jean-François Lejeune

Direction technique
: Philippe Baste

Assistant direction technique
: Malo Martiny

Aide aux costumes
: Adriana Maria Calzetti

Maquillage
: Edwina Calcagno

Accompagnement à la diffusion
: Sabine Dacalor

Production
: garçongarçon - Manon Faure 

> **Les jeunes acteurs non-professionnels sont issus de la classe de 7ème PC de l’Institut des Filles de Marie à Saint-Gilles.** 
> **Ils sont encadrés pour ce projet par leur professeure principale Stéphanie Laurent.**

Une production garçongarçon en coproduction avec le Théâtre Les Tanneurs, Mars (Mons Arts de la Scène), CENTRAL – La Louvière et La Coop asbl.Avec le soutien du Centre Vaba Lava de Tallinn, du Kinneksbond-Centre culturel Mamer, du Théâtre des Doms et du Cinéma Galeries.Avec l'aide de la Fédération Wallonie-Bruxelles - Service du Théâtre, du Centre des Arts Scéniques et de la Ville de Bruxelles. 
Avec le soutien de Shelterprod, taxshelter.be, ING et du tax-shelter du gouvernement fédéral belge.
{: .production }

Salvatore Calcagno | garçongarçon est en résidence artistique au Théâtre Les Tanneurs.
{: .production }

La reprise exceptionnelle du spectacle Gen Z de Salvatore Calcagno pour 4 représentations les 23, 24 et 25 mai 2018 au Théâtre La Balsamine a été rendue possible grâce au soutien du Parlement Francophone Bruxellois - Madame Julie De Groote, de la Commission Communautaire Francophone - Madame Fadila Laanan, du Gouvernement de la Fédération Wallonie-Bruxelles - Ministère de la Culture - Madame Alda Greoli et du Parlement de la Fédération Wallonie-Bruxelles - Monsieur Philippe Courard.
{: .production }


<div class="galerie" markdown=true>
![genz1](/images/GENZ1.png){: .image-process-large}

![genz2](/images/GENZ2.png){: .image-process-large}

![genz3](/images/GENZ3.png){: .image-process-large}

</div>
