Title: Lost in Ballets Russes
Date: 2018/12/6
Slug: lost-in-ballets-russes
Translation: true
Lang: fr
End_date: 2018/12/8
Time: 20h30
Key_image: img-18-19/svg/lost-01.svg
Key_image_detail_body: img-18-19/svg/lost-01.svg
Piece_author: Lara Barsacq
Event_type: Danse
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=81
Color: #913291

Quelques dessins d'un grand-oncle(Léon Bakst-peintre, décorateur et costumier des Ballets russes au début du XXème siècle), une danse en hommage à papa, quelques ustensiles et objets des années 1970. Les chemins de la mémoire génèrent une chorégraphie autobiographique pleine de sincérité.

Projet, texte, chorégraphie, dramaturgie et interprétation
:   Lara Barsacq

Aide à la dramaturgie
:  Clara Le Picard

Conseils artistiques  
:  Gaël Santisteva

Costumes  
:  Sofie Durnez

Création lumière
:  Kurt Lefevre

Musiques
:  Bauhaus, Claude Debussy et Maurice Ravel

Participation
:  Lydia Stock Brody et Nomi Stock Meskin


Une production asbl Gilbert et Stock, en coproduction avec Charleroi danse.Avec le soutien de la Fédération Wallonie-Bruxelles - service de la danse, de Wallonie- Bruxelles International  de La Bellone, de la Ménagerie de Verre et du Théâtre la Balsamine.
{: .production }

<div class="galerie" markdown=true>
![lost](/images/img-18-19/svg/lost-planter-fougere2.svg)
![lost](/images/img-18-19/svg/lost-02.svg)
![lost](/images/img-18-19/svg/lost-03.svg)
<div class="galerie photos-spectacle" markdown=true>
<iframe class="video" src=https://player.vimeo.com/video/300696210></iframe>
</div>

