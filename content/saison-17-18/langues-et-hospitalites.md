Title: Langues et hospitalités
Date: 2018/04/21
Time: 17h00
TimeEnd: 19h00
Partner_logo: /images/logos/culture-et-democratie.png
              /images/logos/radio-panik.png
              /images/logos/united-stages.svg
Event_type: Émission de radio en public
Key_image_detail_body:  UnePomme_170501-4.jpg 
Subhead: organisée par Culture & Démocratie et le théâtre la Balsamine, conçue et animée par Caroline Berliner, avec le soutien de Radio Panik.
Color: #d39556

Artistes, juristes, enseignants et membres d’associations – ayant ou non une expérience de l’exil –, interrogeront leurs pratiques, ce que recouvre aujourd’hui la notion d’hospitalité en Europe et comment la rencontre de l’autre peut devenir un atout, un enrichissement au lieu d’être un obstacle et un sujet de crainte.


Avec
: Avec : Matthieu Lys – avocat notamment spécialisé en droits des étrangers et droits de l’Homme –, Camille Louis – philosophe et dramaturge –, Jeddou Abdel Wahab – militant des droits de l'Homme –, Marie Poncin – enseignante DASPA (Dispositif d’accueil des élèves primo-arrivants) au Campus Saint Jean –, Juliette Pirlet – fondatrice de La Petite École, Patrick Monjoie -conseiller communal à Ittre et directeur du CRIBW
Conclusion poétique à deux voix entre Milady Renoir – performeuse, auteur et animatrice d'ateliers –, et un des membres de la Voix des sans papiers.


Les débats seront ponctués par des lectures de participants aux ateliers du Medex Museum et des interludes musicaux en live avec le groupe Nawaris
{: .production }

La soirée se poursuivra avec la diffusion d’une pièce radiophonique,[Le Voyage](https://balsamine.be/saison-17-18/le-voyage.html) de Brice Cannavo.
{: .production }

L’émission sera diffusée sur les ondes de [Radio Panik](http://www.radiopanik.org/)(105.4 FM) le 26 avril de 16h à 18h.
[Culture & Démocratie](http://www.cultureetdemocratie.be/) et le théâtre la Balsamine sont associés au label [United Stages](https://unitedstages.wordpress.com/).
Illustration : collectif Coconut Valley
{: .production }

Réservation souhaitée mais pas indispensable à l'adresse : reservation@balsamine.be
{: .production }
