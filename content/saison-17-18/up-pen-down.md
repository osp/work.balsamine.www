Title: Up Pen Down
Date: 2017/10/14
Time: 20h30
Piece_authors: OSP
               Adva Zakaï
Event_type: Danse codée et commentée
Subhead: Dans le cadre de la Saison numérique 2017
Key_image: shows/1-Helpers/helper-uppendown.png
Key_image_detail_body: shows/1-Helpers/helper-uppendown.png
Partner_logo: /images/logos/saison-numerique.svg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=33
Color: #fac533
Slug: up-pen-down
Translation: true
Lang: fr

Dans le cadre de la [saison numérique](http://saisonnumerique.be/), les typographes d’[Open Source Publishing](http://osp.kitchen/) et la chorégraphe Adva Zakaï reviennent avec un nouvel épisode de leur recherche entre corps et écriture — danse et typographie qui oscille entre des
moments de type conférence, de prototypage de mouvement, de monstration de processus logiciels et de mouvements de traceurs (plotters).

![](http://ospublish.constantvzw.org/images/var/albums/Up-pen-down--01/2015-10-25%2016_44_47.jpg?m=1492279209){: .half}

Cet événement est issu d'un travail de recherche situé à la rencontre entre typographie digitale et corps. Nous y partagerons nos découvertes et questions avec le public — signes et mouvements, notations de danse et programmation, codes digitaux et gestes, plotters (tables traçantes) et corps entreront en dialogue jusqu'à rendre joyeusement confuse la distinction entre pratiques chorographiques et digitales.
La performance sera une publication ou plutôt nous imprimerons une performance. Vous invitant possiblement sur scène pour bouger, taper, dessiner, lire et écrire jusqu'à ce qu'éventuellement nos actions tracent leurs chemin vers un livret avec lequel nous vous souhaiterons bonsoir.

![](https://cloud.osp.kitchen/s/fC0mEtLVrKrlEEp/download){: .third}
![](https://cloud.osp.kitchen/s/Y5x60v93o96fzfz/download){: .third}

Concept, recherche, performance
:    Gijs De Heij, Pierre Huyghebaert, Ludi Loiseau, Adva Zakaï

Regard extérieur
:    Sarah van Lamsweerde, Louise Baduel

Avec les soutiens de la Fédération Wallonie-Bruxelles - Arts numériques, la Vlaamse Gemeenschapscommissie et le Théâtre la Balsamine.
{: .production}
