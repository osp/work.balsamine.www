Title: Laboratoire POISON
Dates: 2019/01/29 → 2019/02/02 20h30
       2019/02/01 14h
Slug: laboratoire-poison
Key_image: img-18-19/svg/laboratoire-01.svg
Key_image_detail_body: img-18-19/svg/laboratoire-01.svg
Piece_author: Adeline Rosenstein
Event_type: Création - Théâtre documentaire
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=83
Color: #006cdb

Des documents exhumés par le sociologue Jean-Michel Chaumont, dans le cadre de sa recherche sur les codes de conduite du combattant défait, sont le cœur de cette création théâtrale documentaire. Entre trahir et se tuer, y a-t-il une autre possibilité ? Bienvenue au LABORATOIRE POISON, dans les murmures, codes et actes de résistance.

Avec
:  Marie Alié, Brune Bazin, Olindo Bolzan, Ninon Borsei, Léa Drouet, Rémi Faure, Isabelle Nouzha, Titouan Quittot, Martin Rouet et Thibaut Wenger

Conception, écriture et mise en scène
:   Adeline Rosenstein

Assitante à la mise en scène
:   Marie Devroux

Composition sonore
:   Andrea Neumann

Espace
:   Yvonne Harder

Création lumière et direction technique
:   Caspar Langhoff

Masques
:   Rita Belova

Costumes
: Anna Raisson

Production déléguée
:   Leïla Di Gregorio

Une production Little Big Horn en coproduction avec le Théâtre la Balsamine et la Coop asbl. Avec les soutiens de la Fédération Wallonie-Bruxelles- Services du Théâtre, de la Promotion des lettres et de la cellule DOB, de la Cocof, du Kunstencentrum Buda, de taxshelter.be, d'ING et du tax-shelter du gouvernement fédéral belge, de Zoo Théâtre, de L'Esact et de Les Banc Publics - lieu d'expérimentations culturelles.
{: .production }



### Table ronde autour du spectacle
Samedi 2 février de 17h à 19h
Entrée libre - réservation souhaitée
{: .pedagogique }

Comment juger les délations arrachées par les coups et la torture ? Apparemment choquante, la question s’autorise de l’existence historique de codes de conduite et d’instances de jugement qui obéissent à des logiques compréhensibles. Comment aujourd'hui mettre en forme, notamment théâtrale, ces réalités historiques méconnues et permettre au public d’expérimenter tout à la fois la difficulté de juger et la nécessité de le faire ? Comment ces questions bousculent et vivifient à la fois nos représentations de la Résistance et de ses figures ?
Quatre invités pour traiter ces trois questions :
{: .pedagogique }

- José Gotovitch, historien, directeur scientifique du Centre des Archives communistes en Belgique (Carcob).
- Jean-Michel Chaumont, sociologue, professeur à l’université de Louvain.
- Markus Meckl, historien, professeur à l’université d’Akureyri.
- Cécile Vast, historienne, Docteur en histoire, chercheur associé au Laboratoire de recherches historiques Rhône-Alpes (LARHRA-UMR 5190).
{: .pedagogique }

Avec le soutien de la Fédération Wallonie- Bruxelles, service de la Cellule Démocratie ou Barbarie.
{: .production }


<div class="galerie" markdown=true>
![laboratoire](/images/img-18-19/svg/laboratoire-02.svg)
</div>
<div class="galerie photos-spectacle" markdown=true>
</div>

<div class="galerie photos-spectacle" markdown=true>
![laboratoire](/images/img-18-19/labo1serge.jpg){: .image-process-large}
:   © Serge Gutwirth

![laboratoire](/images/img-18-19/labo2serge.jpg){: .image-process-large}
:   © Serge Gutwirth

![laboratoire](/images/img-18-19/labo3serge.jpg){: .image-process-large}
:   © Serge Gutwirth

![laboratoire](/images/img-18-19/labo4serge.jpg){: .image-process-large}
:   © Serge Gutwirth

![laboratoire](/images/img-18-19/labo6serge.jpg){: .image-process-large}
:   © Serge Gutwirth
