Title: Jusqu'à preuve
Date: 2018/06/14
Time: 20h00
Piece_author: Simon Loiseau / Marion Menan
Event_type: Performance
Key_image_detail_body: 1.png
Status: draft
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=69
Subhead: Les réservations pour les soirées combinées se font uniquement par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68


**Jusqu'à preuve** est une performance qui joue avec les concepts scientifiques et les formes plastiques, qui orchestre une rencontre entre théorie et matière. Loin du ton didactique et formel des discours savants, cette expérience appréhende les mots comme des images, et les théories comme des histoires, dans le but de constituer un nouveau récit non linéaire, pour fabriquer une nouvelle manière de raconter le monde. 

Production : Vaisseau
{: .production }