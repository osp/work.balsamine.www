Title: Stage de théâtre 
Date: 2020/04/06
Slug: Stage 
End_date: 2020/04/10 
Piece_author: animé par François Pinte 
Subhead: Reporté - nouvelles dates : du lundi 24 au vendredi 28 août
Category: reporté
Key_image: img-19-20/visuel_stage_pâques.jpg
Key_image_detail_body: img-19-20/F_Pinte__7_.jpg
Event_type: de 12 à 15 ans
Color:#ffffff
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=179

Stage de théâtre : "le bonheur dans tous ses états"

Initialement prévu 6 au 10 avril, le stage est reporté au mois d'août :
du lundi 24 au vendredi 28 août de 13h à 17h + Présentation de fin de stage le vendredi 28 août à 18h


Le stage est animé par François Pinte, comédien-animateur depuis plus de 20 ans. Il met ses compétences au service des groupes, pour les aider à élaborer la forme théâtrale qui leur correspond.

- Prérequis et déroulement du stage:
Le stage de théâtre est ouvert à tout le monde.
Quelles que soient ton expérience et tes motivations, que tu sois calme ou énergique, timide ou… Viens comme tu es. Si ça t’attire, ça t’intéresse, rejoins-nous !
On va apprendre à s’échauffer, à se mettre dans la peau d’un personnage, à jouer des situations de la vie. On va improviser, explorer, écrire, composer, … Et tout ça dans une ambiance festive, pour le plaisir de créer ensemble. 

- Objectif du stage : 
Quand est-ce qu’on peut dire qu’on est heureux ? Content, satisfait, joyeux ? Ça se passe comment à l’intérieur ? Ça tient à quoi, le bonheur ? Et comment ça s’exprime ? C’est contagieux ? …
A partir de vos réflexions et idées sur la thématique : « Le bonheur dans tous ses états », nous créerons une petite comédie qui sera présentée en fin de stage.

- Tarif : 
Schaerbeekois : 60 euros non-Schaerbeekois : 80 euros

- Inscription :
- par téléphone au : 02 735 64 68
- par mail à : reservation@balsamine.be
- En mentionnant :
- Nom + Prénom du stagiaire :
- Âge :
- N° de téléphone du responsable du stagiaire :
- mail du responsable du stagiaire :
- Les bureaux de la Balsamine sont fermés du 1er juillet au 16 août inclus. Pour toutes questions,  nous serons à nouveau disponibles par mail et par téléphone dès le 17 août. 





