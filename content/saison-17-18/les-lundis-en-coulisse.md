Title: Les lundis en coulisse
Date: 2017/12/11
Time: 14h00
TimeEnd: 18h00
Piece_author:
Event_type: À la rencontre des écritures dramatiques contemporaines
Reservation_link:https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=32
Partner_logo: /images/logos/lundis.svg
Key_image: shows/1-Helpers/helper-cuble-grid.png
Color: #95b497

**Les lundis en coulisse** sont un moment de rencontre informel permettant de découvrir à chaque séance des textes dramatiques contemporains qui n'ont jamais été présentés, et qui sont les coups de cœur d'un invité, dit “Le Passeur".
Les rôles des pièces sont distribués au cours de la séance à ceux qui ont envie de s'aventurer dans une “lecture découverte" à voix haute, tandis que d'autres écoutent.

**Passeur**: Jean LE PELTIER, metteur en scène, comédien, performer et auteur.
Né en 1985, Jean Le Peltier obtient en 2008 un master des arts du spectacle (université Rennes II). Il étudie un an en Allemagne (département des sciences du théâtre appliqué à Giessen) et se forme grâce à des workshops de théâtre, de danse, de performance (les Ballets C. de la B., Loïc Touzé, Minako Seki, Kris Verdonck, Mabou Mines Company). Depuis il développe ses spectacles (Vieil, Juste avant la nuit, Les Loups, et joue dans différentes pièces en Belgique, en Allemagne et en France (Rotterdam Presenta, Léa Drouet, Cécile Cozzolino, Compagnie Ocus, les Frères Zipper…).
{: .production }

**Programme des lectures proposées**
{: .production }

**Azote et Fertilisant de Ronan Mancec, Éditions Théâtrales, 2014**
{: .production }

L'explosion de l'usine d'engrais AZF, qui a causé 31 morts et des milliers de blessés le 21 septembre 2001 à Toulouse, est le cadre d'Azote et fertilisants.
Grâce à des recherches documentaires et à la collecte de témoignages, Ronan Mancec s'interroge sur le résultat, la réaction presque chimique de l'immixtion soudaine d'un événement violent dans le quotidien des gens. En entrelaçant paroles rapportées et détours fictionnels, il tisse une poétique de la catastrophe et propose un matériau de jeu à l'efficacité cathartique pour une tragédie d'aujourd'hui.
{: .production }

**Blockhaus d'Alexandre Koutchevski, Éditions l'Entretemps, 2015**
{: .production }

« Trois personnage s'interrogent, en mêlant éléments historiques et histoires d'enfance, sur les blockhaus, lieux du souvenir intime et collectif, côté allemand et français, sur la mémoire et ce qu'il restera des stigmates de la guerre la plus meurtrière de l'histoire de l'humanité dans les siècles futurs. »
{: .production }

**Nos débuts dans la vie de Modiano, Gallimard, octobre 2017**
{: .production }

«Elle sort du théâtre et elle s'agrippe à mon bras… Elle me dit que le metteur en scène, Savelsberg, est venu à l'entracte dans sa loge pour lui proposer le rôle de Nina dans La Mouette, la saison prochaine… Elle ne comprend pas… Savelsberg se déplaçant pour la voir, elle, une débutante, dans une reprise de Noix de coco et lui proposant de jouer Tchekhov? Nous montons la rue Blanche sous cette couche de neige… comme dans un rêve…»
{: .production }


Une coproduction de
:   Atelier Théâtre Jean Vilar, Centre d’études théâtrales de l’UCL, ARTS² – Conservatoire de Mons, La Bellone, Rideau de Bruxelles, Théâtre la Balsamine, Théâtre de Liège, Théâtre des Martyrs, Théâtre Varia, SACD  en partenariat avec le Centre des Écritures Dramatiques – Wallonie Bruxelles.
{: .production }

Les lundis en coulisse belges se sont inspirés d’un dispositif du même nom, inventé par la metteure en scène **Gislaine Drahy**, et qui existe depuis 2002 à Lyon. Depuis il a été repris par **François Rancillac** au Théâtre de l’Aquarium à Paris ainsi que par la Compagnie Les encombrants à Dijon. Nous remercions Gislaine Drahy de nous avoir permis l’organisation des Lundis en Belgique. Ici **Les lundis en coulisse** sont une initiative de **Silvia Berutti-Ronelt**.
{: .production }
