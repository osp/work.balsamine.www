Title: Les oracles
Date: 2017/09/28
End_date: 2017/09/30
Time: 20h30
Piece_author:
Event_type: Création transversale<br>danse/​vidéo/​musique/​texte
Subhead: En partenariat avec <a href="http://www.productionsrhizome.org">Rhizome</a> et <a href="http://transcultures.be/">Transcultures</a>
Partner_logo: /images/logos/transcultures.svg
              /images/logos/rhizome.svg
Key_image: shows/12-Les-Oracles/les-oracles-compo.png
Key_image_detail_body: shows/12-Les-Oracles/anim-boule.gif
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=20&cntnt01returnid=44
Color: #d2d373


**Les oracles** allient danse, poésie, création sonore et vidéo autour de thématiques à la fois féministes et intemporelles, traversées par les textes et la présence des écrivaines Catrine Godin et Martine Delvaux.
La question du genre dans nos perceptions et nos imaginaires contemporains est ici poétisée. Une proposition artistique belgo-québécoise.

Conception, direction artistique et mise en scène
:    Simon Dumas

Coordination et administration du projet
: Yves Doyon

Conception sonore et coproduction
:    Philippe Franck

Création vidéo
:    Thomas Israël
{: .production }

## Percées

Auteure
:    Catrine Godin

Chorégraphie
:    Karine Ledoyen

Interprètes (pour la vidéo)
:    Fabien Piché
:    Ariane Voineau

## Prototype Nº1

Auteure
:    Martine Delvaux

Chorégraphie
:    Manon Oligny

Interprète et co-chorégraphe
:    Marilyn Daoust


Une production de Rhizome, coproduit par Transcultures, avec les soutiens de Manon fait de la danse, du Théâtre la Balsamine, du Conseil des Arts du Canada, du Conseil des arts et des lettres du Québec, du ministère des Relations internationales et Francophonie du Québec et de WBI.
{: .production }

<div class="galerie" markdown=true>
![les oracles](/images/shows/12-Les-Oracles/crystal-ball-1.png){: .image-process-large}

![les oracles](/images/shows/12-Les-Oracles/knuckebones.png){: .image-process-large}

![les oracles](/images/shows/12-Les-Oracles/knucklebone-tex.png){: .image-process-large}

![les oracles](/images/shows/12-Les-Oracles/les-oracles-creampot-1.png){: .image-process-large}
</div>
