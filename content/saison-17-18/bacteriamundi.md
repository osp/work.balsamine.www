Title: Bacteria mundi
Date: 2018/03/15
Dates: 2018/03/15 14h00
       2018/03/15 20h30
       2018/03/16 14h00
       2018/03/16 20h30
       2018/03/17 20h30
Piece_author: Isabelle Dumont et Jacques André
Event_type: Laboratoire scénique<br/>
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=49
Color: #8e708a

##Les deux représentations du 15 mars et celle du 16 mars à 14h sont complètes.

Après un premier hommage au monde microbien dans Bac(h)teria, créé à la Balsamine il y a deux saisons, Isabelle Dumont s’associe avec Jacques André pour prolonger dans Bacteria Mundi une découverte aventureuse et poétique du vivant invisible. Sous la forme d’un laboratoire scénique combinant joyeusement narrations, démonstrations, expérimentations et dégustations culinaires, ils explorent à leur façon les rôles vitaux des bactéries, les relations symbiotiques qu’elles tissent avec nous, leur influence sur nos corps mais aussi sur nos comportements… Qui sait si ce ne sont pas elles qui mettent Isabelle et Jacques en scène ? En suivant les étapes d'un voyage qui parcourt la terre, l’eau, l’air et la peau jusque dans nos intestins, Bacteria Mundi manifeste l’interdépendance de tout le vivant et invite à transformer nos appréhensions à l’égard des « microbes » en émerveillements qui modifient notre regard sur les autres espèces avec lesquelles nous cohabitons.

Conception et performance
:    Isabelle Dumont et Jacques André


Création produite par La Balsamine, avec l’aide de la SACD (bourse aux projets de spectacles vivants) et le soutien du laboratoire de microbiologie de l’Université Libre de Bruxelles
{: .production }

<div class="galerie" markdown=true>
![bacteria](/images/visuelBacteriamundi.jpg)