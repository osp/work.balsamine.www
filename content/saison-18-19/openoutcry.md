Title: Open Outcry [rituel de désenvoûtement de la finance]
Date: 2019/06/14 
Time: 22h00 
Slug: open_outcry
Key_image_detail_body:img-18-19/DesorcelerLaFinance-_c_Rozenn_Quéré-2.jpg
Piece_author: Compagnie Loop-s / Désorceler la finance
Event_type: Performance - L'intégralité de la billetterie sera dédiée à financer les prochaines étapes de travail de Désorceler la finance.
Subhead: Dans le cadre du PIF4
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=116
Status: draft
Color: #009933

A l’occasion du PIF Festival, le laboratoire sauvage Désorceler la finance vous propose un rituel visant à désenvoûter notre façon d’envisager la terre, prise dans le pouvoir sorcier de la finance.

A cheval entre rituel magique, performance théâtrale et action politique, Open Outcry [rituel de désenvoûtement de la finance] se déploie sous la forme d'un rite collectif œuvrant à la transformation de notre système économique. Au cours de ce rituel, vous serez convié.e.s à couper symboliquement le lien, l’emprise, de la finance sur vos vies afin de retrouver, ici et maintenant, dans votre quotidien, de la puissance pour imaginer un nouveau monde.

La finance donne un prix aux céréales, aux semences, à la terre ; elle façonne les terres agricoles, elle détermine notre façon d’accéder à l’alimentation. La finance est partout, l’argent déborde. Les milliards et milliers de milliards des banques, des fortunes démesurées et des dettes abyssales nous sifflent aux oreilles à l’écoute des nouvelles. Ils nous impressionnent. Mais souvent nous ne les entendons plus, occupé.e.s que nous sommes à nous débattre avec quelques dizaines, 
quelques centaines, quelques milliers d’euros peut-être.Peut-être ces milliards nous empêchent-ils aussi, par leurs injonctions et leur insistance étouffante, d’agir autrement. Le temps est venu de dépasser cet état d’empêchement, d’étouffement. Le temps est venu de briser l’envoûtement, l’impuissance sidérée dans lequel nous sommes maintenu.e.s.
{: .production }

En savoir plus sur [Désorceler la finance](http://desorcelerlafinance.org)
{: .pedagogique }

Création collective 
:     Julien Celdran, Luce Goutelle, Emmanuelle Nizou, Camille Lamy, Maxime Lacôme, Aline Fares, Fabrice Sabatier, Ilaria Boscia, Dimitri Tuttle, Yohan Dumas, Aude Schmitter, Arthur Lacomme, Amandine Faugère, Vincent Matyn, Suzie, Suptille, Grégory Edelein, Alice Conquand, Émilie Siaut, Martin Pigeon, Jean-Baptiste Molina, Jonathan Frigeri

Co-production 
:     Loop-s asbl, Fédération Wallonie-Bruxelles, Nuit Blanche Bruxelles

Avec le soutien des fondations Un Monde par Tous, Pour le Progrès de l’Homme et le Théâtre La Balsamine
{: .production }

Remerciements 
:     Grégory Rivoux, Lora Verheecke, Gabriel Nahoum, Stéphane Verlet-Bottéro - La brigade d'actions paysannes
{: .production }
