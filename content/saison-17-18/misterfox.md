Title: Fantastic Mister Fox
Date: 2018/01/17
Time: 15h00
Category: balsatoiles
Length: 1h27
Age: 6 ans et plus
Piece_author: Wes Anderson
Event_type: Balsatoiles
Color: #c3b4c6
Key_image_detail_body: maxresdefault_13.jpg
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=37
Mr Fox est le plus rusé des voleurs de poules. Une fois marié, son épouse Felicity lui demande de mettre fin à ses activités incompatibles avec la vie d'un honorable père de famille. La mort dans l'âme, il se fait alors engager comme éditorialiste dans un journal local où il s'ennuie terriblement. Quand son fils Ash a 12 ans, Mr Fox part s'installer en famille à la campagne à proximité d'élevages de volailles détenus par trois ignobles fermiers : le gros Boggis, Bunce le petit et Bean le maigre. La tentation est trop forte : Mr Fox reprend ses anciennes activités et s'attire les foudres des trois fermiers. Il doit appeler à la rescousse tous les animaux de la région pour protéger sa famille et son territoire.

Un atelier créatif, animé par un artiste plasticien, est proposé de 14h à 15h.
(inscription obligatoire – nombre de place limité)


Projet réalisé avec le soutien du Fonds Baillet Latour, dans le cadre de l’initiative L’extrascolaire au cœur de l’intégration gérée par la Fondation Roi Baudouin.
{: .production }

[Voir la bande-annonce du film "Fantastic Mister Fox"](http://www.allocine.fr/video/player_gen_cmedia=18942560&cfilm=114976.html)
{: .production }