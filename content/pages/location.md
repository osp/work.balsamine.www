Title: Louer la Balsamine
Date: 2017/04
Slug: location
Lang: fr
Translation: true
Status: hidden

[TOC]



Située à quelques pas des institutions européennes, la Balsamine vous propose plusieurs de ses espaces intégrant une infrastructure technique et professionnelle ainsi qu’un bar convivial.

Séminaires, workshops, mariages, anniversaires, formations… Proposez-nous vos événements et nous vous conseillerons.

* * *

[Télécharger le dossier complet](doc/Balsamine-location_espaces-dossier.pdf)



## Nos espaces


### Le Foyer

Cœur du théâtre où tout se passe et tout le monde passe, le Foyer possède un bar et une cuisine. Il est ouvert de plain-pied sur un parc public.

* Superficie : 200m²
* Capacité : 130 pers. assises / 200 pers. debout
 

<div class="jcarousel" markdown>

![location](/images/img-18-19/Architecture270818Balsamine17réduit.jpg ){: .image-process-large}
:   © Hichem Dahes

![location](/images/img-18-19/Architecture270818Balsamine21réduit.jpg){: .image-process-large}
:   © Hichem Dahes

![location](/images/img-18-19/Foyer.jpg){: .image-process-large}
:   © Hichem Dahes
  
</div>

### L’Amphithéâtre
D’un ancien auditorium militaire, les architectes ont conçu un costume sur mesure pour les utilisateurs du théâtre. L’Amphithéâtre comporte un cadre de scène tellement large qu’il projette quasi le public sur scène!

* Superficie du plateau : 200m² (14m × 9m)
* Capacité : gradins avec 170 places assises / 200 pers. debout sur le plateau.

<div class="jcarousel" markdown>

![location](/images/img-18-19/Amphithéâtre_-_Balsamine_2_-_crédit_Hichem_Dahes.jpg){: .image-process-large}
:   © Hichem Dahes

![location](/images/img-18-19/Amphithéâtre_-_Balsamine_3_-_crédit_Hichem_Dahes.jpg ){: .image-process-large}
:   © Hichem Dahes

![location](/images/img-18-19/Grande_Salle.jpg){: .image-process-large}
:   © Hichem Dahes

</div>

### Le Studio

* Superficie du plateau: 130m²
* Capacité: gradins avec 70 places assises / 100 pers. debout (sans gradins)


<div class="jcarousel" markdown>

![location](/images/img-18-19/Architecture270818Balsamine22réduit.jpg ){: .image-process-large}
:   © Hichem Dahes

![location](/images/img-18-19/Petite_Salle.jpg){: .image-process-large}
:   © Hichem Dahes

</div>

### Pour toute demande de disponibilité et de devis

Morgan Brunéa  
[02 737 70 17](tel:+32 737 70 17)   
[morgan.brunea@balsamine.be](mailto:morgan.brunea@balsamine.be)  

</div>


