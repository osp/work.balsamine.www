Title: Ouverture de saison
Dates: 2019/09/28 
Time: 19h15
Slug: ouverture-de-saison
Key_image: img-19-20/giphy.gif
Key_image_detail_body: img-19-20/giphy.gif
Piece_author: Cabaret mademoiselle / DJ Voodoomama
Event_type: Soirée d'ouverture de la saison 2019-2020
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=119
Color: #ff7c48


Le samedi 28 septembre, la Balsa célèbre l'ouverture de sa nouvelle saison ! Une célébration en plusieurs temps en compagnie des artistes de cette saison, du Cabaret Mademoiselle et de Dj VoodooMama.  


Au programme :


**19h15 - Histoires d'une saison**

Les artistes de la saison vous racontent une histoire. 

**20h - Drink d'ouverture**

On trinque à la nouvelle saison !

**20h30 - Pink boys and old ladies**

**22h30 - Cabaret mademoiselle - Entrée libre** 

![Cabaret](/images/img-19-20/cabaret_mademoiselle.jpg){: .half}

A l’occasion de l’ouverture de saison du Théâtre La Balsamine, retrouvez celle par qui tout a commencé, impératrice des nuits bruxelloises, talons vertigineux, jambes interminables et verve incomparable : Mademoiselle Boop !


La grande dame vous amène ses filles, en tout cas une partie, et vous allez voir que le talent est une affaire de famille… Kimi Amen d’abord, dégenrée, à mi-chemin entre contemporaine et bdsm, dévoile ses univers aux influences disco-décadentes ; Athena Sorgelikis, ensuite, fille d’aujourd’hui, vogueuse épileptique, à l’humour absurde et acéré : une vraie gurl comme on en voit qu’à la télé ! Enfin, last but not least, Eve is sick of Adam : le nom annonce la couleur, la demoiselle a de la suite dans les idées… Elle redéfinit à elle seule les notions de genre et de sexualité, brise les cases, les codes et autres préjugés ! 

**23h30 - DJ Voodoomama - Entrée libre**

![DJVOODOO](/images/img-19-20/dj_voodoomama.jpg){: .half}

Du festival de Cannes aux Magrittes du cinema, VoodooMama frappe de sa magie tous ceux qu'elle croise. Ouvrons cette saison par une bonne vieille "boum maison"