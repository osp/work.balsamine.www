Title: Billetterie
Date: 2017/04
Translation: true
Lang: en
Slug: billetterie

[TOC]

## Tickets price

![tarifs-mecene](/images/img-19-20/tarifs/tarif-mecene.svg)

Being a sponsor means contributing to the durability of the Balsamine’s essentials, which are exchange and dialogue, creation and art and experimention.
It means helping us to support artistic creation. It means recognizing and providing back-up to a policy which supports young artists.
It means involving yourself with a wager on youth and the meeting of skills derived from different disciplines. 
It is a singular opportunity to enable us to continue a pricing policy accessible to all. 


![tarifs-plein](/images/img-19-20/tarifs/tarif-plein.svg)


![tarifs-reduit](/images/img-19-20/tarifs/tarif-reduit.svg)

Students, + 60, jobseekers, performing arts professionals, Schaerbeek residents.


![tarifs-enfants](/images/img-19-20/tarifs/tarif-enfant.svg)


![tarifs-art](/images/img-19-20/tarifs/tarif-etu-art.svg)


![tarifs-art](/images/img-19-20/tarifs/tarif-peter.svg)


![tarifs-art](/images/img-19-20/tarifs/tarif-noel.svg)


![tarifs-art](/images/img-19-20/tarifs/art27-arsene.svg)



![tarifs-groupes](/images/img-19-20/tarifs/tarif-groupe.svg)



![tarifs-groupes](/images/img-19-20/tarifs/tarif-groupe2.svg)

Group reservations must be paid 10 days before the date of the show.


## United Stages

In the context of the United Stages ![logo label united stages](/images/label-united-stages-mini.png), the Balsamine raised a lot of funds. The beneficiary is: 2euros50.
You can demonstrate your solidarity at any moment of the season. All you have to do is add the sum you wish to pay to that of the tickets bought. The funds collected will be donated in their entirety. 



## Le Pass TRIO

![passmisaison](/images/img-19-20/pass_mi_saison_essai_2.jpg)

1 gift / 2 possibilities : 

PASS TRIO (1 pers.) : 18 € & companion benefiting from a reduced rate of € 7


PASS TRIO (2 pers.) : 36 €

This Pass includes three shows:

### Backstage
12.02 → 15.02 et 17.02 → 21.02

### A show scheduled in the festival XX TIME
12.03 → 04.04

### Forêts paisibles
21.04 → 25.04 et 27.04 → 1.05


<br>
Interested? Contact our booking office. 

Please note, the Pass can only be bought between 9 december and 14 february. 
<br>


## Reservation

### Par téléphone

The booking office can be contacted on [+32 2 735 64 68](tel:+3227356468). From Monday to Friday from 14:00 to 18:00. La Balsamine takes your reservations and gives you additional information. Outside of these hours and at the weekend, an answering machine takes your reservations

### By e‑mail

By emailing [reservation@balsamine.be](mailto:reservation@balsamine.be), specifying your surname and first name, the title of the show, the date of the performance, the rate(s) that applies to you and the number of tickets you want to receive. You will then receive a confirmation email.   

### On line

Accessible 24/7, you can click on the icon ‘Reserve’, found on the page of each show, and fill in the form. You will then receive a confirmation email. This online reservation concerns both the presale tickets but also the tickets which will only be bought on the evening of the performance.

## Payment methods and picking up tickets

### Presale

By bank transfer to the following account number: IBAN BE15 0680 6267 2030 — BIC GKCCBEBB 
To benefit from the presales rate, the payment must be made 48 hrs before the date of the show. 
Please mention your surname, first name, the date of the show and the structured communication given to you either by email or by telephone.

### On site

The ticket office is open from 19:00 on the evenings that performances take place. 

