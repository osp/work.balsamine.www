Title: As a mother of fact
Slug: As-a-mother-of-fact
End_date: 2019/04/26
Time: 20h30
Date: 2019/04/25
Key_image: 
Key_image_detail_body: 
Piece_author: notch company / Oriane Varak
Event_type: Danse-Théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=91
Color: #fe5000

As a Mother of fact est un spectacle qui porte sur ces liens conscients et inconscients qui déterminent les relations entre femmes, à travers leur statut de fille, ou de mère. Il est question d’aliénation, tantôt librement consentie, tantôt subie ou rejetée. Jusqu’où acceptons-nous d’être manipulées ?


Concept et mise en scène
: Oriane Varak

Création et interprétation
: Jenna Jalonen, Audrey Lucie Riesen, Oriane Varak /Mercedes Dassy

Composition et musique live
: Guillaume Le Boisselier

Création lumières
: Laurence Halloy

Costumes
: Lieve Meeussen

Regard extérieur
: Gala Moody

Dramaturgie
: Hildegard De Vuyst 

Construction table
:  Cools (De Werf)

Développement
:  Co-laBo (Programme de résidence / Residency program from les Ballets c de la b)

Coproduction
: TAKT Dommelhof (BE), De Werf (Bruges, BE)

Production déléguée
:  Théâtre la Balsamine

Soutien
:  Pudding asbl, Théâtre la Balsamine


<div class="galerie photos-spectacle" markdown=true>
</div>

<div class="galerie photos-spectacle" markdown=true>
![asamof](/images/img-18-19/As_a_Mother_of_fact_2.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_3.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_5.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_1.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_6.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_7.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_10.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_11.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_12.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_13.jpg){: .image-process-large}
:   © Hichem Dahes

![asamof](/images/img-18-19/As_a_Mother_of_fact_14.jpg){: .image-process-large}
:   © Hichem Dahes

