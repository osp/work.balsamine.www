Title: Grou
Date: 2019/05/28
Dates: 2019/05/28 10h00
       2019/05/28 13h30
Slug: grou
Category: ReForm 
Key_image_detail_body: img-18-19/grou.jpg
Piece_author: Compagnie Renards
Event_type: Théâtre jeune public
Reservation_link:https://reform.be/nos-activites/en-classe/theatre-a-lecole/
Color: #ffffff

« J’ai une histoire à vous raconter. 
L’histoire de la nuit où j’ai eu 12 ans.
Alors, on imagine que je suis en pyjama, hein, et que j’ai exactement 11 ans et 364 jours. Je viens de me lever de mon lit, en pleine nuit, et sans faire de bruit je me suis faufilé dans la cuisine… 
L’horloge va bientôt sonner minuit, j’ai tout juste le temps de faire comme m’a appris ma Mamie, penser fort à mon voeu et souffler mes bougies... 
C’est parti!»

Texte
: Baptiste Toulemonde

Mise en scène et jeu
:   Arthur Oudar et Baptiste Toulemonde

Scénographie et costumes
:   Bertrand Nodet

Création Lumière
:  Amélie Géhin 

Création sonore
:   Guillaume Vesin

Production Undessix - Effet Mer
Soutiens Fédération Wallonie -Bruxelles, Théâtre Mercelis, Wolubilis, Théâtre de la Montagne Magique, Théâtre Molière (Sète) , Scène Nationale Archipel de Thau, Théâtre Le Hublot – Colombes, Ville de Ganges - Théâtre de l'Albarède, Collectif EnJeux, Réseau en scène Languedoc-Roussillon
{: .production }

Prix de la ministre de l'Enseignement fondamental - Huy 2018
En savoir plus sur [la Compagnie Renards](https://cierenards.wixsite.com/cierenards)
{: .pedagogique }
