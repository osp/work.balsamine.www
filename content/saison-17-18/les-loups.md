Title: Les loups
Date: 2018/03/26
End_date: 2018/03/30
Time: 20h30
Piece_author: Jean Le Peltier
Event_type: Création théâtre
Key_image: shows/2-Les-Loups/wolf7-hd.png
Key_image_detail_body: shows/2-Les-Loups/compo3.png
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=26&cntnt01returnid=44
Intro: Une histoire étonnante sur la solidarité entre les espèces, sur la survie en milieu hostile.
Color: #72572d

Une histoire étonnante sur la solidarité entre les espèces, sur la survie en milieu hostile.

Trois biologistes en Antarctique, éloignés de leur campement afin d’élargir leur champ de recherche, se perdent physiquement et mentalement. L’espace du désert blanc révèle soudainement leur animalité enfouie, leur esprit de meute refoulé.

C’est dans cette urgence, sous le joug de la survie que **Les Loups** nous parlent de leur choix. Des choix qui impliquent des orientations, des stratégies, des questions éthiques, des renoncements et qui témoignent de notre étourdissement devant le vide.


Texte et mise en scène
:   Jean Le Peltier

Avec
:   Pierrick De Luca
:   David Koczij
:   Jean Le Peltier
:   Cécile Maidon

Sculptures
:   Vincent Glowinski

Lumière
:   Émily Brassier et Stine Hertel

Musique, texte et chanson
: David Koczij

Costumes
:   Agathe Thomas

Diffusion
:   Entropie

Regards extérieurs
: Mohamed Boujarra, Vincent Lécuyer

Responsable technique 
: Benjamin van Thiel

Régie son
: Brice Agnès

Une production Ives &amp; Pony en coproduction avec le Théâtre la Balsamine et Le Vivat — Scène conventionnée Danse & Théâtre — Armentières.
Production déléguée Théâtre la Balsamine en collaboration avec Entropie Production.
{: .production }

**Autour du spectacle**

**AFTER SHOW**
Mardi 27 mars : à l’issue de la représentation, nous proposons un échange entre le public et l’équipe artistique. Petite particularité, cette rencontre est menée par Lucas Meister, autre artiste de la saison choisi par Jean Le Peltier.
{: .production }

**Concert de dernière** 
The Summer Rebellion

Vendredi 30 mars à 22h
{: .production }

Sorti d'une tanière dans laquelle le rock n'aurait rien perdu de son animalité et de sa sauvagerie authentique, le batteur David Koczij gronde d'une voix caverneuse et écorchée qui n'a rien à envier à Tom Waits. Son compère Arthur Bacon surprend, quant à lui, par son jeu d'accordéon et fait la démonstration brillante que, loin du musette, cet instrument se prête admirablement à l'énergie du punk et à la fougue débraillée du rock. Une énergie sauvage, roots et franchement communicative.
{: .production }

entrée libre
{: .production }

[Plus d’info sur le groupe](https://www.facebook.com/TheSummerRebellion/?fref=mentions)
{: .production }

<div class="galerie" markdown=true>
![loup](/images/shows/2-Les-Loups/paramecie1.png){: .image-process-large}

![loup](/images/shows/2-Les-Loups/compo4.png){: .image-process-large}

![loup](/images/shows/2-Les-Loups/tex_0_transparent.png){: .image-process-large}

![loup1](/images/lesloups.jpg){: .image-process-large}

<iframe class="video" src="<iframe class="video" src=" https://player.vimeo.com/video/260961452 "></iframe>

</div>
