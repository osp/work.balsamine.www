Title: L'étrange intérieur
Date: 2019/12/28
Dates: 2019/12/28 14:00
          2019/12/28 18:00
Slug: etrangeinterieur
Category: Noël au Théâtre
Age: 8 ans et plus
Key_image_detail_body: img-19-20/OKEtrange_Interieur_00345_Laureen_Thurin_Nal.jpg
Piece_author: Infusion asbl
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=149
Color: #ff7c48
Partner_logo: /images/logos/ctej.svg
              /images/banner_mail_juste_nat.jpg
Un spectacle poétique, dansant, ludique et absurde pour deux acteurs. Où l’on parle plusieurs langues dont la langue des chats. Un spectacle sur l’étrange, l’étranger, les étrangers en nous, nos étranges intérieurs.
« C’est l’histoire d’un enfant qui est né transparent, un enfant qui est né transparent. Pour le mieux voir. Pour le voir mieux. Sa maman lui met des moustaches... mais ce n’est plus un enfant, l’enfant transparent, avec les moustaches. C’est un chat. Un chat de Charleroi ! » 

Écriture et mise en scène
:   Florence A.L. Klein

Avec
: Aminata Abdoulaye Hama et Gordon Wilson 

Scénographie et Costumes
: Emilie Cottam 

Chorégraphies et assistanat à la mise en scène
: Milton Paulo Nascimento de Oliveira

Création sonore
: Daphné D’heur 

Création Lumières
: Alain Collet

Régie
: Isabelle Simon ou Krystel Okwelani 

Conseillers artistiques
: Laurent Capelluto et Serge Demoulin 

Construction décor
: Guy Carbonnelle - Quai 41 

Diffusion
: Anne Jaspard 

Production
: Infusion Asbl

Coproduction
: Pierre de Lune (Centre Scénique Jeunes Publics de Bruxelles)

Soutiens 
: Ministère de la Culture de la Fédération Wallonie-Bruxelles (service du théâtre), le  Centre Culturel du Brabant Wallon, le  Centre Culturel de Braine-l’Alleud ,  La Roseraie. 

Avec aussi le soutien et l’aide précieuse de ékla (Centre scénique de Wallonie pour l’enfance et la jeunesse) et l’aide de très nombreux enfants et enseignants. 
Remerciements chaleureux à Salima Sarah Glamine et Gaëtan D’Agostin .
{: .production }


<div class="galerie photos-spectacle" markdown=true>
![etrange](/images/img-19-20/Etrange_Interieur1.jpg){: .image-process-large}
:   © Laurent Thurin Nal

![etrange](/images/img-19-20/Etrange_Interieur2.jpg){: .image-process-large}
:   © Laurent Thurin Nal

![etrange](/images/img-19-20/Etrange_Interieur3.jpg){: .image-process-large}
:   © Laurent Thurin Nal

![etrange](/images/img-19-20/Etrange_Interieur4.jpg){: .image-process-large}
:   © Laurent Thurin Nal

![etrange](/images/img-19-20/Etrange_Interieur5.jpg){: .image-process-large}
:   © Laurent Thurin Nal

![etrange](/images/img-19-20/Etrange_Interieur6.jpg){: .image-process-large}
:   © Laurent Thurin Nal

</div>
