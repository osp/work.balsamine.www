Title: Diffusion
Date: 2019/04
Slug: diffusion
Translation: true
Lang: fr


[TOC]

##Spectacles disponibles à la diffusion
**Ørigine** *(théâtre - création 10/18)*
de Silvio Palomo / Le comité des fêtes

[voir le teaser](https://www.youtube.com/watch?time_continue=2&v=8ld8Lo9Rm7Y)

Contact : [Monica Gomes](mailto:monica.gomes@balsamine.be)

[Télécharger le dossier de diffusion](doc/Dossier_diffusion_-Origine-Silvio_Palomo.pdf)
![origine](/images/img-18-19/origine18.jpg)

* * *

**Où suis-je ? Qu’ai-je fait ?** *(théâtre - création 11/18)*
de Pauline d'Ollone

[voir le teaser](https://vimeo.com/297675612)

Contact : [Monica Gomes](mailto:monica.gomes@balsamine.be)

![ousuisje](/images/img-18-19/ouisuisjesite1.jpg)

* * *

**Les Carnets de Peter**(spectacle d'ombre et de musique - création 12/18 )*
Théâtre du Tilleul

Contact : [Clément Dallex Mabille](mailto:clement@theatredutilleul.be) - [Théâtre du Tilleul](http://www.theatredutilleul.be/)
![peter](/images/img-18-19/carnetsdepeter.jpg)

* * *

**Laboratoire poison** *(théâtre documentaire - création 01/19)*
d'Adeline Rosenstein

[voir le teaser](https://vimeo.com/littlebighorn/poisontrailer)

contact : [Little big horn](http://www.littlebighorn.be)
![labo](/images/img-18-19/labo1serge.jpg)


* * *

**Etna** *(danse - création 03/18)*
de Thi-Mai Nguyen

[voir le teaser](https://vimeo.com/315834289)

Contacts : [Thi-Mai Ngyuen](mailto:thimainguyencompany@gmail.com) / [Monica Gomes](mailto:monica.gomes@balsamine.be)
![etna](/images/img-18-19/etna2.jpg)


* * *

**i-clit** *(danse - création 02/18)*
de Mercedes Dassy

[voir le teaser](https://vimeo.com/321454743)

Contact : [AMA ART MANAGMENT AGENCY](http://amabrussels.org/)
![iclit](/images/img-18-19/iclit3.jpg)

* * *

**Ludum** *(danse - création 04/19)*
d'Anton Lachky

[voir le teaser](https://vimeo.com/326761496)

Contact : [Eleonore Valère Lachky](mailto:antonlachkycompany@gmail.com)
![ludum](/images/img-18-19/ludum5.jpg)

* * *

**As a Mother of Fact** *(danse-théâtre - création 06/18)*
d'Oriane Varak/notch company

[voir le teaser](https://vimeo.com/328656221)

Contact : [notch company](http://www.notchcompany.com/)
![asamof](/images/img-18-19/As_a_Mother_of_fact_2.jpg)

* * *

**Coeur obèse** *(performance - création 03/19)*
d'Amandine Laval

Contact : [Monica Gomes](mailto:monica.gomes@balsamine.be)
![coeurobese](/images/img-18-19/coeurobese1-crédit_Hicham_Riad.jpg)

* * *

**Wijckaert, une bombe** *(théâtre - création 10/15)*
de Martine Wijckaert

[voir le teaser](https://vimeo.com/184500899)

Contact : [Monica Gomes](mailto:monica.gomes@balsamine.be)
![wij](/images/img-18-19/photo_94.jpg)


* * *

**George Kaplan** *(théâtre - création 11/19)*
du Collectif RZ1GK

[voir le teaser](https://vimeo.com/374889246)

Contact : [Clément Dallex Mabille](mailto:production@balsamine.be)
![georgekaplan](/images/img-19-20/george_kaplan8.jpg)

* * *

**B4 summer** *(danse- création 02/20)*
de Mercedes Dassy

[voir le teaser](https://vimeo.com/383048700)

Contact : [AMA ART MANAGMENT AGENCY](http://amabrussels.org/)
![b4summer](/images/img-19-20/B4summer5191_web__c__Michiel_Devijver.jpg)

* * *

**Backstage** *(théâtre - création 02/20)*
du Collectif BXL Wild

[voir le teaser](https://vimeo.com/389698071)

Contact : [Clément Dallex Mabille](mailto:production@balsamine.be)

[Télécharger le dossier de diffusion](doc/Dossier_Diff_Backstage_V03.pdf)
![backstage](/images/img-19-20/Backstage_-_crédit_Hichem_Dahes__web1.jpg)

##BALSATOUR : tournée 19-20 des créations Balsa

###Danse

* * *

**I-CLIT** de Mercedes Dassy (création 02/18)  

> 2018 

- Prix Jo Dekmine du Théâtre des Doms, Avignon (Fr)
- Nomination au prix de la critique « Meilleur spectacle de danse »

> Novembre 2019

- Théâtre de l’Ancre, Charleroi (Be) 

> Février 2020  

- Le Central, Centre Culturel La Louvière (Be) 
- Mars – Mons arts de la scène (Be) 

> Mars 2020  

- Festival Dansfabrik, le Quartz, Brest (Fr)

> Juin 2020 

- Festival Latitudes contemporaine, Lille (Fr)
- Les Rencontres Chorégraphiques de Seine-St-Denis, Montreuil (Fr)

> Octobre 2020 

- Maison de la culture de Tournai, Tournai (Be)

> Janvier 2021

-  Pôle Sud, Strasbourg (Fr)

* * *

**B4 SUMMER** de Mercedes Dassy (création 02/20)  

> Avril 2020

- Mars-Mons Arts de la scène- festival Demain (Be) 


* * *

**ETNA** de Thi-Mai Nguyen (création 02/18) 

> 2018 

- Prix de la critique « Meilleur spectacle de danse » 

> Octobre 2019 

- Festival Zoa/L’étoile du Nord – Paris (Fr)
– Festival Zoa/Point éphémère - Paris (Fr) 

> Février 2020 

- Maison de la Création – Bruxelles (Be) 

* * *

**AS A MOTHER OF FACT** d'Oriane Varak / notch company (création 06/18) 

> Juin 2020 

- Atelier 210 – Bruxelles (Be) 

* * *

**DE LA POÉSIE, DU SPORT, ETC …** de Fanny Brouyaux et Sophie Guisset (création 02/19)

> Octobre 2019 

- La Raffinerie/ Objectif danse 9, Bruxelles (Be) 

> Février 2020 

- POLE-SUD – Strasbourg (Fr) 

* * *

**LUDUM** d'Anton Lachky (création 04/19)

> Octobre 2019 

- Bratislava v pohybe (Sk)
- Théâtre Marni, Bruxelles (Be)
- La Raffinerie/ Objectif danse 9, Bruxelles (Be)

> Novembre 2019

- Ainsi Theater, Maastricht (Nl) 

> Avril 2020  

- Gessi Zurich / Step festival (Ch) 

> Mai 2020

- Sur Mars/Mons (Be) 
- Théâtre Nebia, Biel/Bienne/Step festival (Ch) 
- Theater Phönix, Steckborn/Step festival (Ch) 
- Burghof, Lörrach/Step festival (Ch) 
- Château Rouge, Annemasse/Step festival (Ch) 

* * *

###Théâtre/performance

**OÙ SUIS-JE ? QU’AI-JE FAIT ?** de Pauline D’Ollone (création 11/18)

> 2019 

- Nomination au prix de la critique « Meilleur.e auteur.e »

> Octobre 2019 

- Atelier 210, Bruxelles (Be) 

* * *

**LABORATOIRE POISON** d'Adeline Rosenstein (création 01/20) 

> Novembre 2019

- Théâtre la Criée – Marseille (Fr) 

* * *

**LES CARNETS DE PETER** du Théâtre du Tilleul (création 12/18)

> Novembre 2019

- Théâtre la Montagne magique, Bruxelles (Be) 

> Décembre 2019 

- Théâtre Jean Vilar, Louvain-la-Neuve (Be) 

> Janvier 2020  

- Théâtre la Balsamine, Bruxelles (Be) 
- Service culturel de la ville de Maromme (Fr) 
- Foyer culturel de Saint-Ghislain (Be) 

> Février 2020

- Centre culturel de Braine-l’Alleud (Be) 
- Centre culturel d’Ottignies + Centre Culturel du Brabant Wallon (Be) 

> Mars 2020 

- Centre culturel de Tubize (Be) 
- Centre dramatique Pierre de Lune à la Maison de la Culture et de la Cohésion sociale de Molenbeek Saint Jean (Be)  
- Centre culturel de Waterloo (Be)

> Avril 2020

- Centre culturel Les Chiroux de Liège – Cité Miroir (Be) 

* * *

**CŒUR OBÈSE** d'Amandine Laval (création 03/2019)

> Novembre 2019 

- Théâtre de l’Ancre, Charleroi (Be) 

> Février 2020 

- Mars-Mons arts de la scène (Be) 

* * *

**CEREBRUM, LE FAISEUR DE RÉALITÉ** d'Yvain Juillard (création 01/15) 

> Septembre 2019

-  Lauréat du Label d'Utilité Publique - Service Public Francophone Bruxellois

> Novembre 2019 

- Théâtre de la Passerelle - Scène Nationale - Gap (Fr)
– Théâtre de Liège (Be)

> Janvier 2020

- Théâtre de Saint-Quentin-en- Yvelines (Fr) 
- Théâtre de Namur (Be) 

> Février 2020 

- Les Quinconces, l’espal - Scène Nationale -le Mans (Fr) 

> Mars 2020

- Théâtre des Martyrs – Bruxelles (Be) 

> Avril 2020 

- Waterloo (Be) 

* * *

**MIRABILIA** d'Isabelle Dumont (création 10/16) 

> Novembre 2019  

- Musée des Beaux-arts de Mons (Be) 

 


