Title: Mon p’tit coco
Date: 2019/11/28
Dates: 2019/11/28 10h00
       2019/11/28 13h30
Slug: monptitcoco
Category: Reform asbl
Age: 3 ans et plus 
Piece_author: La Berlue 
Key_image:
Key_image_detail_body: img-19-20/MG_8041_Web-705x500.jpg
Reservation_link:https://reform.be/nos-activites/en-classe/theatre-a-lecole/
Event_type: Théatre jeune public
Color: #6b89ed

Une histoire d’œufs, d’ailes, d’elles deux, d’œufs à la coque, de deux cocottes, de p’tits cocos à manger tout chaud ! Des poules qui couvent, des poussins qui rêvent de prendre leur envol. De coquille en becquée, blanc et jaune ne savent plus où donner de la tête ! Mais au fait, qui a commencé ? 

Avec
: Violette Léonard et Barbara Sylvain

Mise en scène
 :   Ariane Buhbinder
 
Scénario 
: Violette Léonard, Barbara Sylvain et Ariane Buhbinder

Scénographie, accessoires
:  Christine Flasschoen

Costumes 
: Christine Neuhuys

Musique originale 
: Martin Salemi

Machinerie 
: Arnaud Van Hammée

Eclairages 
: Léopold De Neve

Régie 
: Nicolas Fauchet,Anthony Vanderborght, Léopold De Neve ou Nicolas Kluge

Photos et production 
: Paul Decleire

Diffusion
 : Agathe Cornez

Illustration et graphisme 
: Diego Funck

Une production de La Berlue en coproduction avec L’Anneau Théâtre
en coproduction avec La Coop asbl, avec le soutien de SMART et du tax-shelter du gouvernement fédéral belge
en coproduction avec Pierre de Lune – Centre scénique Jeunes Publics de Bruxelles
avec le soutien de la Fédération Wallonie-Bruxelles – Direction du Théâtre
et avec le soutien de La Roseraie, de ékla, du Centre culturel de Braine-l’Alleud, de l’Espace Columban et du Collège des Bourgmestre et Échevins de la Commune d’Ixelles.
Merci à Yves Hauwaert et à David Quertigniez.
{: .production }


