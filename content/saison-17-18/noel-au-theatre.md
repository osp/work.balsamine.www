Title: Noël au Théâtre
Date: 2017/12/26
End_date: 2017/12/30
Color: #69351b
Partner_logo: /images/logos/ctej.svg
            : /images/NAT RVB.jpg
Status: draft

La création « jeune public » est dans tous ses états à l’occasion du Festival Noël au Théâtre : des spectacles, des créations, des chantiers, des lectures et plusieurs animations débarquent du 26 au 30 décembre à Bruxelles.

Cette saison encore, la Balsamine accueille le festival et ouvre grandes ses portes à la magie et la chaleur du théâtre pour tous.
