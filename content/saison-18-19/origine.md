Title: Ørigine
Dates: 2018/10/3 → 2018/10/5 20h30
       2018/10/9 au 2018/10/13 20h30
Slug: origine
Time: 20h30
Key_image: img-18-19/svg/origine-01.svg
Key_image_detail_body: img-18-19/svg/origine-01.svg
Piece_author: Silvio Palomo/ Le comité des fêtes
Event_type: Création théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=78
Color: #ff51a0

Les protagonistes de cette aventure sont un homme et une femme. Un pique-nique s'organise. Un ours et un pingouin s'invitent. Du coup, leur évènement s'en trouve bouleversé.


Une création de
: Le comité des fêtes

Conception et mise en scène
:   Silvio Palomo

Scénographie
:   Itzel Palomo

Avec
:   Avec Léonard Cornevin, Aurélien Dubreuil-Lachaud, Ophélie Honoré, Antonin Jenny, Manon Joannotéguy, Jean-Baptiste Polge et Nicole Stankiewiscz

Création lumière
:   Léonard Cornevin

Régie générale
: Quentin Pechon

Remerciement
: Gaël Renard

Production déléguée / diffusion
:   Théâtre la Balsamine


En coproduction avec le Théâtre la Balsamine et La Coop asbl. Avec les soutiens de la Fédération Wallonie-Bruxelles – Aide aux projets théâtraux, de Shelterprod, de taxshelter.be, d’ING et du Tax-shelter du gouvernement fédéral belge
{: .production }

<div class="galerie" markdown=true>
![origine](/images/img-18-19/svg/origine-02.svg)
![origine](/images/img-18-19/svg/origine-03.svg)
![origine](/images/img-18-19/svg/origine-04.svg){: .demi}
![origine](/images/img-18-19/svg/origine-05.svg)
![origine](/images/img-18-19/svg/origine-06.svg)
</div>

<div class="galerie photos-spectacle" markdown=true>
![origine](/images/img-18-19/origine1.jpg){: .image-process-large}
:   © Hichem Dahes

![origine](/images/img-18-19/origine12.jpg){: .image-process-large}
:   © Hichem Dahes

![origine](/images/img-18-19/origine13.jpg){: .image-process-large}
:   © Hichem Dahes

![origine](/images/img-18-19/origine14.jpg){: .image-process-large}
:   © Hichem Dahes

![origine](/images/img-18-19/origine15.jpg){: .image-process-large}
:   © Hichem Dahes

![origine](/images/img-18-19/origine16.jpg){: .image-process-large}
:   © Hichem Dahes

![origine](/images/img-18-19/origine17.jpg){: .image-process-large}
:   © Hichem Dahes

![origine](/images/img-18-19/origine18.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe class="video" src=https://player.vimeo.com/video/291096709></iframe>

<iframe class="video" src="https://player.vimeo.com/video/291459467"></iframe>
</div>

En savoir plus sur [Silvio Palomo](https://www.silviopalomo.com/)
{: .pedagogique }
