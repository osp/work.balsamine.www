Title: 7.A March
Date: 2020/03/17
Slug: xx-time-march
End_date: 2020/03/18
Time: 20h30
Key_image_detail_body: img-19-20/photo_March_-_crédit_Victoriano_Moreno.jpg
Piece_author: Nathalie Rozanes <br/> Spectacle annulé
Event_type: Performance <br/> Dans le cadre du XX Time
Status: draft
Color: #ffffff

*Je crois que le seul espace d’honnêteté qui nous reste est l’espace individuel* Julia Kristeva dans Revolt, She Said.

En passant du stand-up à la poésie du « spoken word » et au journalisme gonzo, d’extraits de romans autobiographiques aux carnets de voyages, ou de manifestes écosexuels à des appels Skype, Nathalie Rozanes invite les danseurs  Elizabeth Ward et Tarek Halaby à juxtaposer ensemble différentes formes de langages à la première personne du singulier. Sur scène, le « je » se déploie dans toute sa pluralité, rendant compte de la nature complexe et fragmentée de notre identité. La performance aborde alors les inconforts politiques et privés ressentis depuis le début de cette recherche en mars 2017. Un portrait croisé accompagné en live par le musicien électronique Frédéric Altstadt.
{: .push-down}

Idée et Concept  
:   Nathalie Rozanes

Performance et creation
:   Frédéric Alstadt, Nathalie Rozanes, Elizabeth Ward, Tarek Halaby

Texte
:   Nathalie Rozanes, Elizabeth Ward,Tarek Halaby, Leonard Cohen, George Perec, Ben Lerner, Michael Ondaatje

Montage texte
:   Nathalie Rozanes

Son
:   Frédéric Altstadt

Chorégraphie  
:   Elizabeth Ward, Tarek Halaby

Espace et lumières
:   Simon Siegmann

Dramaturgie
:   Jellichje Reijnders

Assistante
:  Solène Valentin

Animation vidéo
:   Thomas Depas

Résidences
:   workspacebrussels, Campo

Soutiens
:   VGC, workspacebrussels, WIPCOOP / Mestizo Arts


[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)
