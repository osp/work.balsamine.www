Title: Les carnets de Peter
Dates: 2018/12/27 19h30
       2018/12/28 → 2018/12/29 14h00
       2019/01/03 → 2019/01/06 15h00
Slug: les-carnets-de-peter
Category: Noël au Théâtre
Age: pour tous à partir de 7 ans <br/> Toutes les représentations sont complètes. Nous ouvrons une liste d'attente sur place chaque jour, une heure avant la représentation.
Key_image: img-18-19/svg/carnets-00.svg
Key_image_detail_body: img-18-19/svg/carnets-00.svg
Piece_author: Théâtre du Tilleul
Event_type: Création -  spectacle d'ombres et de musique
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=82
Color: #006cdb

Peter, 87 ans, tente de se souvenir de son enfance : l'Allemagne , Munich 1936, les défilés militaires, les drapeaux, l'émigration en Amérique...Il se souvient , qu'un jour, seul dans la grande bibliothèque de son père, il décide d'inventer les étranges aventures d'un petit garçon nommé Donald, un petit garçon rêveur qui lui ressemble énormément sauf que...

Avec
:   Carine Ermans, Carlo Ferrante, Sylvain Geoffray et Alain Gilbert

Conception du spectacle
:   Carine Ermans et Sylvain Geoffray

Conseil dramaturgique
:   Louis-Dominique Lavigne

Musique
:   Alain Gilbert

Mise en scène
:   Sabine Durand

Lumière
:   Mark Elst

Régie
:   Thomas Lescart

Scénographie
:   Pierre-François Limbosh et Alexandre Obolensky

Peintures
:   Alexandre et Eugénie Obolensky aidés de de Malgorzota Dzierzawska et Tancrède de Ghellinck

Costumes
:   Silvia Hasenclever

Travail du mouvement
:   Isabelle Lamouline

Images animées
:  Patrick Theunen et Graphoui

Accessoires
:  Amalgames

Production
:  Mark Elst

En coproduction avec le Théâtre la Balsamine avec la collaboration du Théâtre La montagne magique, du CDWEJ et de la Maison des Cultures et de la Cohésion Sociale de Molenbeek-Saint-Jean.
Le Théâtre du Tilleul est en résidence artistique et administrative au Théâtre la Balsamine.
{: .production }

Les représentations sont suivies des ateliers de La Bibliothèque rêvée de Peter :

> 28/12 et 29/12 : ateliers à 15h15

> 3, 4, et 6 /01 : ateliers à 16h15

> 5 /01 : goûter des rois, suivi des ateliers à 16h30 - en présence de Peter Neumeyer et de Pascal Lemaître qui dédicaceront l’album Dick le Lambin, Dick l’éclair paru chez Pastel.

<div class="galerie" markdown=true>
![carnets](/images/img-18-19/svg/carnets-01.svg)
![carnets](/images/img-18-19/svg/carnets-02.svg)
</div>

<div class="galerie photos-spectacle" markdown=true>
![carnets](/images/img-18-19/carnet1.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet2.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet3.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet4.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet5.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet6.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet7.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet8.jpg){: .image-process-large}
:   © Théâtre du Tilleul
