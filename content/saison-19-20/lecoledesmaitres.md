Title: École des maîtres 2019
Date: 2019/09/23
Time: 20h
Slug: ecoledesmaitres
Key_image: img-19-20/angelica-liddell-c3a9cole-des-mac3aetres.jpg
Key_image_detail_body: img-19-20/angelica-liddell-c3a9cole-des-mac3aetres.jpg
Piece_author: Angelica Liddell
Event_type: Cours international itinérant de perfectionnement théâtral
Subhead: Entrée libre
Reservation_link: COMPLET
Color: #6b89ed
Partner_logo: /images/logos/ecole-maitres.svg

Certaines scènes peuvent heurter la sensibilité des spectateurs. Déconseillé aux moins de 18 ans.

L'École des Maîtres est une académie théâtrale européenne itinérante, regroupant des comédiens venus de Belgique, d'Italie, de France et du Portugal. 16 d’entre eux se retrouveront sous le regard de la dramaturge, metteure en scène et actrice espagnole Angélica Liddell. Elle travaillera avec les stagiaires sur un projet intitulé « HISTOIRE DE LA FOLIE À L’ÂGE CLASSIQUE DE MICHEL FOUCAULT.
LE NERF DU CRAPAUD. »

Les phases de travail et des présentations publiques ont lieu dans les différents pays européens partenaires du projet : Rome du 8 au 11 septembre, Lisbonne du 12 au 15 septembre, Coimbra du 16 au 19 septembre, La Balsamine - Bruxelles du 20 au 23 septembre et Caen du 24 au 27 septembre.	 
{: .push-down}

Partenaires du projet et direction artistique: CSS - Teatro stabile di innovazione del Friuli Venezia Giulia (Italie), CREPA - Centre de Recherche et d'Expérimentation en Pédagogie Artistique (CFWB/Belgique), Teatro Nacional D.Maria II, TAGV - Teatro Académico de Gil Vicente (Portugal), La Comédie de Reims - Centre Dramatique National, La Comédie de Caen, Centre Dramatique National de Normandie (France)
{: .production }

Avec le soutien de MIBACT — Direzione Generale Spettacolo dal vivo, , Regione Autonoma Friuli Venezia Giulia – Direzione centrale culture, sport e solidarietà (Italie).
{: .production }

Avec la participation de: Accademia Nazionale d'Arte Drammatica "Silvio d'Amico", Short Theatre, Teatro di Roma, ERPAC – Ente Regionale Patrimonio Culturale Friuli Venezia Giulia (Italie) ; Théâtre de Liège - Centre européen de création théâtrale et chorégraphique, Centre des Arts scéniques, Ministère de la Communauté française -Service général des Arts de la scène, Wallonie-Bruxelles International (CFWB/Belgique) ; Ministère de la Culture et de la Communication, Fonds d'Assurance Formation des Activités du Spectacle (France) ; Universidade de Coimbra (Portugal)
{: .production }
