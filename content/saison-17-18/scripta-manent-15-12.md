Title: Scripta Manent 15 Décembre
Date: 2017/12/15
Status: draft
Slug: scripta-manent-15
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=47

# Vendredi 15 décembre

## 20h30 : Mimesis -I’ll be forever young or I’ll be gone

Exercice anthropotechnique pour un performeur et un vidéoprojecteur.
 
Si la mimesis peut s’entendre comme une imitation du réel, cette
imitation n’est-elle pour autant qu’une vulgaire copie ou un acte
innocent ?
 
Pietro Marullo, performeur atypique, use de son corps comme d’un
catalyseur d’images. Par le mouvement, il réécrit une certaine histoire
de l’humanité en la dénudant de son environnement. *Mimesis* met en
tension plusieurs archives provenant de notre mémoire collective et les
enchaîne dans un montage fracturé. Sorte de grand zapping universel, ce
flot d’images chorégraphiquement reproduites souligne l’impulsion
dynamique et la force motrice de nos corps et témoigne de notre posture
belliqueuse envers la réalité.

Idée, dispositif, mise en scène, chorégraphie et performance
: Pietro Marullo

Vidéo
: Giulio Boato

Son
: Jean-Noël Boissé

Lumière
: Julie Petit-Etienne

Assistante et costumière
: Noémi Knecht

Coach et doublure
: Paola Di Bella et Paola Madrid

Production: Cie INSIEMI IRREALI (ITA/BE)
Aides et soutiens: Théâtre la Balsamine, LMTA (Vilnius), RAMDAM (Lyon),
TanzHaus (Zurich).
{: .production }


## 21h30 : Pink Boys and Old Ladies

Lecture

Norman, qui n’est pas normand, est un petit garçon assez banal, mais
Norman aime porter des robes. Un jour, le père de Norman décide
d’accompagner son fils en robe à l’école.

A partir de ce fait divers réel, Marie Henry dépeint une famille
ordinaire empêtrée dans une situation délicate et qui exprime mal son
malaise. Beaucoup de choses seront dites, mais certainement pas
l’essentiel. Car dans beaucoup de familles on aime parler de rien parce
qu’on ne veut surtout pas parler de tout. 


Texte
: Marie Henry

Mise en scène
: Clément Thirion

Avec 
: Gwen Berrou, Marie Henry, Lucas Meister, Clément Thirion, Simon
Thomas, Mélodie Valemberg.

## Dans le foyer : Lagunas

Fiction & Interactive Installation

***Lagunas** est une installation* narrative et interactive. Un
triptyque entre la mémoire, la mort et l’eau. Des paysages
préhistoriques qui appellent la mort et racontent l'histoire d'une mort,
d'une mort dans l'eau ; mais aussi la mort de l'eau sur la planète
Terre.

Conception, direction et production
:    Laura Colmenares Guerra

Modélisation 3D et Animation
:    Rémy Gosselin / Abdoul Diallo / Samson Michel/ Laura Colmenares Guerra

Simulations 3D d’eau
:    Remy Gosselin - Compositing : Rémy Gosselin / Laura Colmenares Guerra

Logiciel et design d’interaction
:    Yacine Sebti - Musique : Rafael Muñoz Gomez / Loup Mormont

Tournage Sous-l’eau
:    Acteur : Juan Bernardo Martinez - Camera sous-marine: Wim Michiels - Assistant camera sous-marine : Jan Lemmens - Plongeur de securite : Karen Jensen - Assistant camera en surface : Ruby van der Kooij - Phantom Operator : Stijn Berghman - Assistant de direction : Kristina Ianatchkova

Tournage Parque Nacional Natural Chingaza
:    Conchita Guerra / Laura Colmenares Guerra
