Title: Scripta Manent
Date: 2017/12/14
End_date: 2017/12/15
Time: 20h30
Piece_author: Mimesis - I’ll be forever young or I’ll be gone / Néant / Pink Boys and Old ladies / Lagunas
Event_type: Cycle nouvelles écritures
Key_image: shows/1-Helpers/helper-pen-grid.png
Key_image_detail_body: shows/1-Helpers/helper-pen-grid.png
Color: #c5b083
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=47


De nouvelles façons de voir le monde et de concevoir l’acte d’écrire. Textes, processus narratifs, l’occasion d’apercevoir de nouveaux visages, d’entendre et de voir des liens qui se nouent, des lianes qui s’entrelacent les unes aux autres afin de les faire vivre. L'écriture, au sens large.

**Mimesis, I’ll be forever young or I’ll be gone ** - performance de Pietro Marullo 

**Néant**-  lecture de Martine Wijckaert  

**Pink Boys and Old Ladies ** - lecture du texte de Marie Henry

**Lagunas**- Installation de Laura Colmenares Guerra 

* [Voir le détail de la soirée du 14 Décembre](/saison-17-18/scripta-manent-14.html)

* [Voir le détail de la soirée du 15 Décembre](/saison-17-18/scripta-manent-15.html)

**Entrée + soupe /soir** : 5 euros 

