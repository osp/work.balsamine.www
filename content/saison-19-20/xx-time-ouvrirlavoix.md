Title: Ouvrir la voix
Subhead: La projection de 20h est complète. Nous ouvrons une liste d'attente sur place dès 19h. 
Date: 2020/03/24
Dates: 2020/03/24 17h30
       2020/03/24 20h00
Slug: xx-time-ouvrir-la-voix
Key_image_detail_body: img-19-20/ouvrirlavoix_affiche.jpg
Piece_author: Amandine Gay<br/> Projections annulées
Event_type: Film documentaire<br/> Dans le cadre du XX Time<br/> entrée gratuite sur réservation
Status: draft
Color: #ffffff

Ouvrir la Voix est un documentaire sur les Afro-descendantes noires d'Europe francophone (France et Belgique) écrit et réalisé par Amandine Gay. C’est aussi le premier long métrage de la réalisatrice et, surtout, le résultat de près de trois ans de travail en autoproduction.  

Ouvrir la Voix est un documentaire de création qui bouscule les codes du film d’entretiens. Pensé et réalisé comme une grande conversation entre vingt-quatre femmes noires de France et de Belgique, le film débute le jour où elles se sont découvertes noires, en contexte minoritaire, et se termine avec leurs aspirations pour le futur. C'est aussi un hommage aux artistes noires que la réalisatrice a rencontrées à l’époque où elle était encore comédienne, c’est pourquoi le film est ponctué de répétitions et de performances. 

Ouvrir la Voix est aussi une façon, pour Amandine Gay, de célébrer l’histoire de la résistance des femmes noires qui ont toujours inventés des outils leur permettant de ne pas être réduites au silence.  

écriture et réalisation
:   Amandine Gay

production
:   Bras de fer

[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)