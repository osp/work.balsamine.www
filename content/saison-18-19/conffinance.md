Title: Désorceler la finance #Conférence
Date: 2019/06/14 
Time: 20h30 
Slug: conférence_désorceler
Key_image_detail_body:img-18-19/desorceler-la-finance-_c_Cyrille-Cauvet.jpg
Piece_author: Luce Goutelle & Emmanuelle Nizou (de Désorceler la finance)
Event_type: Conférence - L'intégralité de la billetterie sera dédiée à financer les prochaines étapes de travail de Désorceler la finance.
Subhead: Dans le cadre du PIF4
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=116
Status: draft
Color: #009933

Loin de sa prétendue rationalité, la finance serait-elle un système sorcier à désenvouter ?
Cette conférence vous propose une immersion dans les explorations du laboratoire sauvage de recherches expérimentales Désorceler la finance.

La crise traversée par les pays occidentaux est la crise d’un système tout entier. Pour en sortir, il semble urgent que les citoyen.ne.s, artistes et activistes, se réapproprient les enjeux liés au fonctionnement de ce système et de la société qui l’abrite. Finance et sorcellerie sont deux mondes qui paraissent à première vue aux antipodes, pourtant, suivant le terme utilisé par l’anthropologue Jeanne Favret Saada, « désorceler » serait une manière de retourner le maléfice à l’envoyeur, de lui renvoyer l’envers de ses pouvoirs pour contribuer à se libérer de son emprise et nous redonner une capacité d’agir : un pouvoir de faire.
{: .production }

En savoir plus sur [Désorceler la finance](http://desorcelerlafinance.org)
{: .pedagogique }
