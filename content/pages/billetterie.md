Title: Billetterie
Date: 2017/04
Translation: true
Lang: fr
Slug: billetterie

[TOC]

## Tarifs

![tarifs-mecene](/images/img-19-20/tarifs/tarif-mecene.svg)

Être mécène, c’est participer à la pérennité des fondamentaux de la Balsamine que sont l’échange et le dialogue, la création et l’enjeu artistique et expérimental.
C’est nous aider à soutenir la création artistique. C’est reconnaître et accompagner une politique qui soutient les jeunes artistes.
C’est vous associer au pari de la jeunesse et de la rencontre des savoirs issus de différentes disciplines.
C’est une opportunité singulière de nous permettre de poursuivre une politique tarifaire accessible à tous.

![tarifs-plein](/images/img-19-20/tarifs/tarif-plein.svg)


![tarifs-reduit](/images/img-19-20/tarifs/tarif-reduit.svg)

Étudiants, + de 60 ans, demandeurs d’emploi, professionnels du spectacle, schaerbeekois.


![tarifs-enfants](/images/img-19-20/tarifs/tarif-enfant.svg)


![tarifs-art](/images/img-19-20/tarifs/tarif-etu-art.svg)


![tarifs-art](/images/img-19-20/tarifs/tarif-peter.svg)


![tarifs-art](/images/img-19-20/tarifs/tarif-noel.svg)


![tarifs-art](/images/img-19-20/tarifs/art27-arsene.svg)



![tarifs-groupes](/images/img-19-20/tarifs/tarif-groupe.svg)



![tarifs-groupes](/images/img-19-20/tarifs/tarif-groupe2.svg)

Le paiement des réservations de groupe doit s’effectuer 10 jours avant la date de représentation choisie.


## United Stages

Au sein du Label United Stages ![logo label united stages](/images/label-united-stages-mini.png), la Balsamine réalise également une grande collecte de fonds à destination d’associations qui accompagnent au quotidien les plus fragilisés. Vous pouvez être solidaire de cette initiative à tout moment de la saison. Il vous suffit d’ajouter le montant que vous souhaitez verser à celui des places achetées. 

## Le Pass TRIO

![passmisaison](/images/img-19-20/pass_mi_saison_essai_2.jpg)

Et si vous tentiez le pass Trio ? 
Offrez et célébrez toutes les occasions de la vie en offrant ce sésame culturel  !

1 cadeau / 2 possibilités : 

PASS TRIO (1 pers.) : 18 € & accompagnant(e) bénéficiant d’un tarif réduit de 7 €

PASS TRIO (2 pers.) : 36 €

Ce Pass inclut trois spectacles :

### Backstage
12.02 → 15.02 et 17.02 → 21.02

### Un spectacle programmé dans le festival XX TIME
12.03 → 04.04

### Forêts paisibles
21.04 → 25.04 et 27.04 → 1.05


<br>
Intéressé.e ? Contactez notre bureau des réservations au 02 735 64 68 ou par mail à [reservation@balsamine.be].(mailto:reservation@balsamine.be)
Le pass Trio est en vente du 9 décembre 2019 au 14 février 2020. 
Le destinataire du cadeau pourra choisir ultérieurement auprès de nous les dates de son choix. 

Tout détenteur du Pass doit nous communiquer les dates de son choix avant le 14 février.
<br>


## Réservations

### Par téléphone

Le bureau des réservations est joignable au [+32 2 735 64 68](tel:+3227356468). Du lundi au vendredi, de 14 h à 18 h, la Balsamine prend vos réservations ou vous donne des renseignements complémentaires. En dehors de ces heures et le week-end, un répondeur prend vos réservations.

### Par e‑mail

En envoyant un e‑mail à l’adresse [reservation@balsamine.be](mailto:reservation@balsamine.be) en précisant votre nom et votre prénom, le nom du spectacle, la date de représentation, le(s) tarif(s) dont vous bénéficiez et le nombre de places souhaitées. Un e‑mail de confirmation vous sera ensuite renvoyé.

### Par le site internet

Accessible 24h/24, vous pouvez cliquer sur le lien « réserver » présent sur chaque page de spectacle du site et compléter le formulaire. Un e‑mail de confirmation vous sera ensuite envoyé. Cette réservation en ligne concerne à la fois les places au tarif prévente mais aussi les places qui ne seront payées que le soir de votre venue.

## Moyens de paiement et retrait des places

### Préventes

Pour bénéficier du tarif prévente, le paiement doit avoir lieu 48h avant la date de représentation choisie.
Par virement bancaire au numéro de compte suivant : IBAN&nbsp;BE15&nbsp;0680&nbsp;6267&nbsp;2030 — BIC&nbsp;GKCCBEBB

Nous vous remercions d’indiquer en communication votre nom, votre prénom, la date de représentation choisie et la communication structurée qui vous a été transmise soit par e‑mail, soit par téléphone.

### Sur place

La billetterie est ouverte les soirs de représentation à partir de 19h00.

### Règle
Toute place achetée ne peut en aucun cas bénéficier d'un remboursement. Cependant, un report de place est possible sur un autre spectacle de la même saison.
