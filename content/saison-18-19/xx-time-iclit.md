Title: i-clit
Date: 2019/03/20
Slug: xx-time-iclit
Translation: true
Lang: fr
End_date: 2019/03/22
Time: 20h30
Key_image: img-18-19/svg/xx-time-01.svg
Key_image_detail_body: img-18-19/svg/iclit-side-00-2.svg
Piece_author: Mercedes Dassy
Event_type: Reprise danse <br/> Dans le cadre de Brussels, dance ! Focus on contemporary dance.
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=87
Partner_logo: /images/logos/brussels-dance.svg
Status: draft
Color: #fe5000

Une nouvelle vague du féminisme est née - ultra-connectée , ultra sexuée et plus populaire. Mais face au pouvoir ambivalent de la pop culture, où et comment se placer dans ce combat en tant que jeune femme ? **i-clit** traque ces moments de fragilité , où l'on oscille entre nouvelles formes d'oppression et affranchissement.

Concept, chorégraphie, interprétation
:   Mercedes Dassy

Dramaturgie, regard extérieur
:  Sabine Cmelniski

Création lumière
:  Caroline Mathieu

Costumes, scénographie
:  Mercedes Dassy et Justine Denos

Création sonore
:  Clément Braive

Diffusion
:  Art Management Agency (AMA)

Production déléguée
:  Théâtre la Balsamine

En coproduction avec le Théâtre la Balsamine et Charleroi danse. Avec l'aide de la Fédération Wallonie-Bruxelles - service de la danse, de la S.A.C.D., du Théâtre Océan Nord, du B.A.M.P., de Project(ion) Room and Friends with benefits.
{: .production }

<div class="galerie" markdown=true>
![xx](/images/img-18-19/svg/iclit-side-01.svg)
![xx](/images/img-18-19/svg/iclit-side-02.svg)
![xx](/images/img-18-19/svg/iclit-side-03.svg)
</div>

<div class="galerie photos-spectacle" markdown=true>
![iclit1](/images/img-18-19/iclit1.jpg){: .image-process-large}
:   © Hichem Dahes

![iclit2](/images/img-18-19/iclit2.jpg){: .image-process-large}
:   © Hichem Dahes

![iclit3](/images/img-18-19/iclit3.jpg){: .image-process-large}
:   © Hichem Dahes

![iclit4](/images/img-18-19/iclit4.jpg){: .image-process-large}
:   © Hichem Dahes

![iclit5](/images/img-18-19/iclit5.jpg){: .image-process-large}
:   © Hichem Dahes

![iclit6](/images/img-18-19/iclit6.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe class="video" src=" https://player.vimeo.com/video/255039212 "></iframe>
</div>
