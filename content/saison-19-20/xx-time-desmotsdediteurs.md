Title: Des mots d'éditeurs
Date: 2020/04/01
Slug: xx-time-des-mots-editeurs
Time: 20h30
Key_image_detail_body: img-19-20/Sabine_Wespieserweb.jpg
Piece_author: Rencontre avec l'éditrice Sabine Wespieser<br/> Séance annulée
Event_type: Dans le cadre du XX Time <br/>entrée gratuite sur réservation
Status: draft
Color: #ffffff

Le projet "Des mots d'éditeurs" propose une série de rencontres littéraires itinérantes et éphémères.
Ces rencontres mettent en lumière le travail souvent peu connu et pourtant formidable de maisons d'éditions indépendantes.
Chaque rencontre se tient dans un lieu inattendu et insolite qui, fait office de librairie éphémère d'un soir dont les étagères ne seront remplies que par les livres de l'éditeur invité.

Pour la soirée du 1er avril, l'éditrice Sabine Wespieser est mise à l'honneur. Il s'agira de l'épisode 4 de la saison 3 du projet "Des Mots D'Editeurs", une saison consacrée uniquement aux éditrices.

Durant cette soirée, nous reviendrons sur le parcours riche et inspirant de Sabine Wespieser, sa ligne éditoriale, les textes, les auteurs et tous les enjeux liés à la création éditoriale aujourd'hui.
Cette rencontre- interview sera ponctuée de multiples lectures et lectures musicales qui nous permettront d'entendre et apprécier les textes et les auteurs dont nous parlerons.
Les livres seront en vente à l'issue de la soirée.


Sabine Wespieser, née en 1961, a d’abord enseigné les lettres classiques avant d’entrer, en 1987, aux éditions Actes Sud. D’abord assistante éditoriale du fondateur de la maison, Hubert Nyssen, elle y est devenue éditrice – de Michèle Lesbre et de Vincent Borel notamment – et responsable de la collection Babel. En 2000, elle a été directrice littéraire de la collection Librio chez Flammarion.
C’est en 2001 qu’elle a créé sa propre maison d’édition, Sabine Wespieser éditeur, dont les premiers titres sont parus à l’automne 2002.
Sabine Wespieser éditeur est une structure littéraire indépendante, qui publie des textes de fiction, française et étrangère, à la cadence d'une dizaine de titres par an. 
La maison compte à ce jour plus de cent cinquante titres pour une soixantaine d’auteurs, et a obtenu le Prix Femina étranger en 2006 pour L’Histoire de Chicago May de Nuala O’Faolain, le Grand Prix des lectrices de Elle en 2007 pour Terre des oublis de Duong Thu Huong, le prix Femina 2014 pour Bain de lune de Yanick Lahens, le prix des Libraires et le prix RTL/Lire en 2015 pour Amours de Léonor de Récondo et, en 2019 un prix Femina spécial pour l’ensemble de son oeuvre à Edna O’Brien ainsi que le Prix de la Langue française 2019 pour Mur Méditerranée de Louis-Philippe Dalembert. 

[pour en savoir plus sur les rencontres des mots d'éditeurs](https://www.desmotsdediteurs.com/)

[pour en savoir plus sur les éditions Sabine Wespieser éditeur](https://www.swediteur.com)

[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)