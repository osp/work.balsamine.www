Title: Respire
Date: 2018/12/28
End_date: 2018/12/29
Time: 11h30
Slug: Respire
Category: Noël au Théâtre
Age: 10 ans et plus
Key_image_detail_body: img-18-19/respire.jpg
Piece_author: Théâtre de la Guimbarde
Event_type: Théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=105
Color: #006cdb

A travers le regard vif de Lucy, une petite fille de neuf ans, le spectacle « RESPIRE » interroge notre rapport au temps. 
Il nous interpelle, sans lourdeur, avec vivacité et humour, sur la nécessité de prendre le temps, le temps de s’arrêter, le temps de s’écouter, le temps de rêver… celui de RESPIRER. 

« RESPIRE » c’est l’histoire de la rencontre, au quotidien, entre deux mondes, celui des adultes et celui des enfants, deux univers qui se croisent et s’entrecroisent, se disent, se parlent, sans pour autant s’inverser. 
Une histoire moderne, presque banale mais sortie du réalisme brut par une théâtralisation ludique, un peu caricaturale, un jeu vivant et très corporel, dans un décor léger. 
Une histoire profondément optimiste d’une petite fille qui interpelle des adultes un peu décalés dans un monde qui court, un rien enfermés dans leurs propres contradictions, mais en attente de pouvoir respirer et VIVRE…


Texte et mise en scène
:  Daniela Ginevro

Accompagnement 
:   Gaëtane Reginster, Camille Sansterre

Création lumières
:   Vincent Stevens

Scénographie
:   Aurélie Borremans, Valérie Perin

Habillage  sonore
:   Olivia Carrère

Musique 
:   Yvan Murenzi

Vidéo
:   Yoann Stehr

Costumes
: Isabelle De Cannière

Régie
:   Vincent Steven, Julien Palcentino, Damien Rullaert

Photographies et affiche
: Gregory Dekens

Interprétation
:   Olivia Carrère, Laurent Denayer, Coralie Vanderlinden


Une production du Théâtre de la Guimbarde en corproduction avec la Coop asbl, avec le soutien de Shelterprod, taxshelter.be, ING et du Tax Shelter du Gouvernement fédéral belgen des Centres culturels de Braine l'Alleud, Ottignies et Rixensart.
{: .production }

<div class="galerie photos-spectacle" markdown=true>
![respire](/images/img-18-19/respire.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire2.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire3.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire4.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire5.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire6.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire7.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire8.jpg){: .image-process-large}
:   © Nicolas Bomal

![respire](/images/img-18-19/respire9.jpg){: .image-process-large}
:   © Nicolas Bomal

</div>