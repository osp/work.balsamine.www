Title: index
Date: 2015
Category: Noël au Théâtre
Slug: index_noel-au-theatre

La création « jeune public » est dans tous ses états à l’occasion du Festival Noël au Théâtre : des spectacles, des créations, des chantiers, des lectures et plusieurs animations débarquent du 26 au 30 décembre à Bruxelles.

Cette saison encore, la Balsamine accueille le festival et ouvre grandes ses portes à la magie et la chaleur du théâtre pour tous.

![](/images/logos/ctej.svg){.quarter}
![](/images/logos/grenouille.jpg){.quarter}

#### [plus d’infos sur le Festival Noël au Théâtre](https://ctej.be/festival-noel-au-theatre/presentation/)

Festival Noël au Théâtre organisé par la Chambre des Théâtres pour l’Enfance et la Jeunesse en partenariat avec le Théâtre la Balsamine, le Centre culturel Jacques Franck, Pierre de Lundi / Au Botanique, le Théâtre La montagne magique, le Théâtre National, le Théâtre Les Tanneurs, la Maison des Cultures et de la Cohésion Sociale de Molenbeek, la Salle Polyvalente Mercelis et la Roseraie.
{.production}
