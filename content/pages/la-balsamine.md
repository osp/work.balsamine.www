Title: La Balsamine
Date: 2017/04
Slug: la-balsamine
Translation: true
Lang: fr
Subhead: Un théâtre à l’état P.U.R.
Subsubhead: Partagé. Utopique. Responsable.

<!-- Photo balsa -->


[TOC]


#Saison 19–20 <br/>Du courage 

#Introduction à l’histoire d’une saison, toutes les histoires d’une saison.

Comment conter l’histoire d’une saison ?
Comment résumer toutes les histoires d’une saison ?
Comment donner à voir, avant l’heure, alors que rien n’est créé ?
Comment dire ce qui, dans ces fictions, interpelle. Comment le partager, susciter la curiosité ?
Par où commencer ?
{: .intro}

* * *

> « Il faut commencer par le commencement, et le commencement de tout est le courage. Il faut dire que le courage est la vertu inaugurale du commencement. » Vladimir Jankélévitch

* * *
Le théâtre est un art vivant qui demande du courage. Ce temps citoyen particulier travaille à la consolidation de nos émancipations individuelles et collectives.
La scène intimide, elle se gagne, se mérite. Elle est cet espace des possibles où nos diversités trouvent nourriture à nos engagements communs.
{: .intro}

Si les nouveaux langages émeuvent et sidèrent, c’est parce qu’ils tissent les liens du futur, ils osent une forme de désobéissance face à ce qui paraissait, hier encore, indéboulonnable. Les artistes de demain sont une nouvelle espèce, tous genres confondus, qui garde en elle la possibilité de transgresser. Ils font preuve de ...
{: .intro}


En attendant le développement des créations jusqu’à leur forme spectaculaire, vous découvrirez sur ce site indiscipliné, trois traversées possibles de la saison à venir :
{: .intro}


* l’une, intuitive, imagée voire poétique.
* l’autre, informative et pratique.
* la troisième (à vivre) sera la vôtre, elle demandera votre venue, votre présence physique, votre regard.


<br>
Haut les cœurs !
Belle errance, soyez intrépides !<br/>
{: .intro}


<br>Monica Gomes<br/>
{: .author}


# Un projet éthique pour les 5 saisons à venir.

Une Balsa à la mine épurée, apurée, en purée, certains soirs et selon les
menus. Pur jus, pur théâtre. En l’état.

Partagé, parce qu’ouvert sur le monde, prêt à recevoir l’autre dans sa
diversité, dans sa complexité. En dialogue, car c’est la seule voie de
l’apaisement.

Le seul chemin possible, c’est l’accessibilité à tous, même aux abeilles qui
peuplent notre toit. La Balsa est un lieu qui change et grandit avec les
artistes qui l’habitent, avec les publics qui la traversent. La Balsa mûrit et
contamine par son esprit libertaire. Nous sommes une terre pour les langues
étranges et réfléchissantes, un endroit où l’on peut se perdre et mieux se
retrouver.

Utopique, quoi de plus logique. Le risque de voir, le risque de faire, le
risque de dire et d’entendre, c’est ça l’utopie : offrir un développement à des
œuvres singulières. Une scène de toutes les aventures. Un lieu qui risque car
c’est vivre plus et il n’y a pas de risque sans l’humain qui le prend, qui le
court. Des créations qui mettent en scène les narrations que notre époque
fabrique. Des narrations fragmentaires, hybrides. Leur donner du temps, de
l’espace.

Responsable, enfin. C’est une maison verte, qui tente par ses choix de
respecter les hommes et la nature, de vivre dans un développement durable et
d’investir ses moyens dans tout ce que la créativité a de plus riche. Hier,
aujourd’hui et demain.

# Incubations et résidences

Un espace et du temps donnés à des artistes pour affiner, forger un projet. Moments de partage et d'expérimentations physiques et métaphysiques. Certaines incubations seront ouvertes au public.

* * *

##Au pied des montagnes *(Théâtre d’ombres – jeune public)*
d'Une tribu collectif

* * *

##Je suis venu voir les gens danser autour de moi ... *(Danse)*
d'Anna Ten et Louis Labadens


Le 17 septembre 2019 à 14h30 et 19h30 : sortie de résidence publique pour le projet Je suis venu voir les gens danser autour de moi de Anna Ten et Louis Labadens. Un spectacle mêlant danse contemporaine et théâtre documentaire, basé sur l’histoire singulière d’un spectateur.

[Retrouvez toutes les infos ici](/saison-19-20/jesuisvenuvoirlesgens.html)   

* * *

##Forces *(Danse)*
de Leslie Mannès
{. type}

* * *

##La prisonnière des Sargasses *(Théâtre)*
de Judith Ribardière

* * *

##La bande de la lande *(Théâtre)*
de Nelly Latour

* * *

##The violin Goat *(Cirque)*
d'Anna Nilsson - Petri Dish
{. type}

* * *

##Materia matrix *(Danse)*
d'Oriane Varak /notch company  

* * *

##Bâtard Festival
Depuis 10 ans, le Bâtard festival représente une vitrine significative pour les artistes contemporains émergents. Depuis février 2019, Bouchra Lamsyeh et Sabine Cmelniski ont pris en charge sa curatelle et sont bien déterminées à rendre à cette plateforme sa qualité « Batarde », tout en renforçant sa mission initiale : donner une visibilité à celleux qui n'en ont pas dans ces réseaux. Elles exploreront donc le concept d'Urgence, en tant que symptôme généralisé du secteur culturel mais aussi dans sa portée sociale, politique et environnementale. Il s'agit surtout d'observer de près ce qui émerge de cette urgence. Nombre d'artistes inventent des formes de résistances indirectes, créatives mais pas moins radicales comme stratégie de survie. Revenir au mot Bâtard, c'est aussi reconnecter le festival à la scène alternative bruxelloise et adopter des méthodes de travail toujours plus inclusives.

Cette saison, la Balsamine accueillera les artistes programmé.e.s en résidences artistiques et techniques. 

[Plus d'infos...](http://www.batard.be/)

* * *

##Échange européen de résidences
Cette saison, nous nous associons avec le Théâtre de Poche de Hédé-Bazouges (Bretagne - FR) et le Théâtre du Grütli (CH) dans le cadre d’un échange européen de résidences d’artistes.

* * *

# United Stages

La Balsamine est signataire de la charte United Stages.

UNITED STAGES ce sont une cinquantaine de structures et associations culturelles qui s’engagent publiquement à défendre et promouvoir un projet de société progressiste, solidaire et respectueux des droits humains.

La création du label United Stages est consécutive à la volonté du secteur culturel d’exprimer haut et fort son soutien à toutes les personnes fragilisées par les effets des politiques inhumaines et discriminatoires. L’article 23 de notre Constitution garantit à chacun “*le droit de mener une vie conforme à la dignité humaine*”. Alors ensemble, nous refusons d’être les spectateurs passifs du déni de ce droit fondamental. Sans accès au logement, à la santé, ou à la formation nulle possibilité de participer à la vie sociale et culturelle. Luttons contre la résignation, luttons contre la haine et le mépris mais surtout luttons pour les droits essentiels de ces femmes, de ces hommes et de ces enfants, nos pairs et nos égaux !

Plus d’informations sur United Stages Belgium [ici](https://unitedstages.wordpress.com/?fbclid=IwAR3FMm0aXJWsXyR5yz2HeLq6T-yG9kK9X2KIBlSTpg9yo95KQjRUpdQxFig) et sur [la page facebook](https://www.facebook.com/unitedstagesbelgium/)

# Partenaires

La Balsamine est subventionnée par la Fédération Wallonie-Bruxelles et fait partie du réseau des Scènes chorégraphiques de la Commission Communautaire française de la Région de Bruxelles-Capitale.

La Balsamine reçoit aussi le soutien de Wallonie-Bruxelles Théâtre/Danse et de Wallonie-Bruxelles International.

![](/images/img-19-20/logos/logos-all-large-19-20.svg)

![](/images/img-19-20/logos/logo_plan_marnix_nb.jpg){: .demi}

# Équipe

<!-- Alex -->
<!-- :   <a href="mailto:&#117;%73er%40h%6f%73t.&#110;a%6de">user@host.name</a> -->

Direction générale et artistique
:   Monica Gomes [monica.gomes@balsamine.be](mailto:monica.gomes@balsamine.be)

Direction financière et administrative
:   Morgan Brunéa [morgan.brunea@balsamine.be](mailto:morgan.brunea@balsamine.be)

Coordination générale, communication et accueil compagnies
:   Fanny Arvieu [fanny.arvieu@balsamine.be](mailto:fanny.arvieu@balsamine.be)

Presse, promotion et relations publiques
:   Irène Polimeridis, momentanément absente, remplacée par Marion Birard [relations.publiques@balsamine.be](mailto:relations.publiques@balsamine.be) 

Comédienne et romaniste - Médiation écoles et associations
:   Noemi Tiberghien [noemi.tiberghien@balsamine.be](mailto:noemi.tiberghien@balsamine.be)

Chargé de production
:   Clément Dallex Mabille [production@balsamine.be](mailto:production@balsamine.be)

Metteur en scène et auteur - Artiste associée
:   Martine Wijckaert [martine.wijckaert@balsamine.be](mailto:martine.wijckaert@balsamine.be)

Résidence artistique et administrative
:   [Théâtre du Tilleul](http://www.theatredutilleul.be)

Direction technique
:   Jef Philips [jef.philips@balsamine.be](mailto:jef.philips@balsamine.be)

Régisseur
:   Rémy Urbain [remy.urbain@balsamine.be](mailto:remy.urbain@balsamine.be)

Stagiaire régie
:   Alice Spenlé

Responsable bar
:   Inge Van audenaerde

Photographe de plateau associé
:   Hichem Dahes

Designers graphique associés
:   [Open Source Publishing](http://osp.kitchen)


# Location des espaces

Située à quelques pas des institutions européennes, la Balsamine vous propose plusieurs de ses espaces intégrant une infrastructure technique et professionnelle ainsi qu’un bar convivial.

Séminaires, workshops, mariages, anniversaires, formations… Proposez-nous vos événements et nous vous conseillerons.

* * *

[Plus d'infos](/location.html)


# Historique

Le théâtre de la Balsamine est fondé par [Martine
Wijckaert](http://martine-wijckaert.be) en 1974. Il s’agit au départ d’une simple structure juridique, une asbl, permettant l’obtention de subsides à la création.

Le théâtre ainsi créé ne dispose d’aucun lieu ni forme de résidence au sein d’une autre structure existante. Il obtient cependant des subsides au coup par coup permettant la réalisation des premiers spectacles de [Martine Wijckaert](http://martine-wijckaert.be). Ces réalisations naissent d’un mode de nomadisme urbain: des sites à l’abandon deviennent autant de décors naturels sur lesquels les spectacles opèrent une intervention.

En 1981, le théâtre de la Balsamine s’installe de manière provisoirement définitive dans la friche des anciennes casernes Dailly.

![historique 1](/images/historique1.jpg)

Ces casernes ont été abandonnées depuis plusieurs années et elles sont découvertes dans leur jus. Un impressionnant périmètre de bâtiments fin XIX<sup>e</sup> cerne un parc intérieur redevenu sauvage. Le site a toutes les vertus d’une cité de la fiction et cette qualité invite le théâtre à s’y installer.

Commence alors une période dite sauvage durant laquelle de très nombreux lieux seront investis pour leur valeur spatiale brute et où naîtront des spectacles sur mesure, comme autant d’interventions uniques et
totalement éphémères sur l’architecture.

Théâtre, art plastique et musique se mêlent et rassemblent sur le site de nombreux artistes y trouvant résidence de travail et d’expérimentation.

Tout en dirigeant la Balsamine installée dans les casernes, [Martine Wijckaert](http://martine-wijckaert.be) y poursuit son travail artistique propre.

L’activité va également se centraliser sur un lieu particulier de la caserne, baptisé « amphithéâtre » puis rapidement « amphi », en raison de ses caractéristiques architecturales : il s’agit en effet d’un ancien auditorium militaire, structuré en hémicycle.

<div class="jcarousel" markdown>
![historique 2](/images/historique2.jpg)
:   -

![historique 3](/images/historique3.jpg)
:   -
</div>

Les premiers aménagements de l’amphi se feront selon la technique du recyclage absolu, le matériel nécessaire et encore utilisable étant démonté dans d’autres ailes de la caserne et remonté après transformation dans l’amphi.

En 1994, [Martine Wijckaert](http://martine-wijckaert.be) confie la direction artistique du théâtre à Christian Machiels et y demeure en qualité d’artiste associée. Le théâtre actuel, d'un style contemporain ayant conservé toutefois la singularité de la salle primitive, date de 2001 et est l'œuvre de l'architecte Francis Metzger, pour laquelle il a reçu le 3<sup>e</sup> prix d’architecture à la biennale du Costa Rica. La programmation de Christian Machiels a été tout entière
dévolue aux arts du spectacle et aux jeunes compagnies de théâtre et de danse contemporaine.

2011 marque l'arrivée d’une nouvelle direction artistique composée de Fabien Dehasseler et Monica Gomes.Tout en marquant davantage la particularité de l’identité de la Balsamine dans le paysage théâtral, de nouveaux partenariats se construisent. La Balsamine s’inscrit aussi progressivement dans une politique de développement durable (placement de ruches sur le toit depuis 2014, remplacement progressif de l’équipement lumière par des leds).

# Archives site web

* [Saison 2011-2012](http://2011-17.balsamine.be/index.php/Archives/Saison2011-2012.html)
* [Saison 2012-2013](http://2011-17.balsamine.be/index.php/Archives/Saison2012-2013.html)
* [Saison 2013-2014](http://2011-17.balsamine.be/index.php/Archives/Saison2013-2014.html)
* [Saison 2014-2015](http://2011-17.balsamine.be/index.php/Archives/Saison2014-2015.html)
* [Saison 2015-2016](http://2011-17.balsamine.be/index.php/Archives/Saison2015-2016.html)
* [Saison 2016-2017](http://2011-17.balsamine.be)
* [Saison 2017-2018](https://2017-18.balsamine.be)
* [Saison 2018-2019](https://2018-19.balsamine.be)
