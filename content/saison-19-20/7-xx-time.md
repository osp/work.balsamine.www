Title: 7. XX Time
Date: 2020/03/12
End_date: 2020/04/04
Slug: xx-time
Translation: true
Lang: fr
Time: 20h30
Category: annulé
Key_image: img-19-20/festival_xx_time_-_Balsamine_-_image_seule.jpg
Key_image_detail_body: img-19-20/festival_xx_time_-_Balsamine_-_image_seule.jpg
Piece_author: le corps comme territoire / ({:}) / March / ouvrir la voix / In my big fireworks I play to you the final bouquet / défense verbale / Mon nom est clitoris / Des mots d'éditeurs / Tout doit disparaître / Serial lovers / Cœur obèse / Laure SofLove <br/> Festival annulé
Event_type: création, théâtre, danse, performance, installation, rencontres, débats, projections documentaires, atelier, concert           
Color: #ffffff


### [Le corps comme territoire](/saison-19-20/xx-time-carolinegodart.html)

## rencontre avec Caroline Godart

12.03
{: .dates }


### [({:})](/saison-19-20/xx-time-imprononçable.html)

## de Lorette Moreau - performance

12.03 → 13.03
{: .dates }


### [March](/saison-19-20/xx-time-march.html)

## de Nathalie Rozanes - Performance

17.03 → 18.03
{: .dates }


### [Ouvrir la voix](/saison-19-20/xx-time-ouvrir-la-voix.html)

## d' Amandine Gay - Film documentaire

24.03
{: .dates }


### [In my big fireworks I play to you the final bouquet](/saison-19-20/xx-time-soeurh.html)

## de Les soeurs h - Concert performé

27.03 → 28.03
{: .dates}


### [Défense verbale](/saison-19-20/xx-time-defenseverbale.html)

## atelier pratique avec asbl Garance

30.03
{: .dates}


### [Mon nom est clitoris](/saison-19-20/xx-time-mon-nom-est-clitoris.html)

## de Lisa Billuart Monet et Daphné Leblond - film documentaire

31.03
{: .dates }

### [Des mots d'éditeurs](/saison-19-20/xx-time-des-mots-editeurs.html)

## rencontre avec Sabine Wespieser

1.04
{: .dates }


### [Tout doit disparaître](/saison-19-20/xx-time-tout-doit-disparaitre.html)

## d'Ophélie Mac - Performance - démo (+18 ans)

3.04
{: .dates }


### [Serial Lovers](/saison-19-20/xx-time-serial-lovers.html)

## Bibi Lona - concert jazz/blues

3.04
{: .dates }


### [Cœur obese](/saison-19-20/xx-time-coeur-obese.html) 

## d'Amandine Laval - Performance (+18 ans)

3.04 → 4.04
{: .dates }

### [Laure SoftLove](/saison-19-20/xx-time-laure-softlove.html)

## démonstration d'objets sexy chics

4.04
{: .dates }


