Title: Coeur obèse
Date: 2019/03/14
Slug: xx-time-coeur
Translation: true
Lang: fr
End_date: 2019/03/16
Time: 20h30
Key_image: img-18-19/svg/xx-time-01.svg
Key_image_detail_body: img-18-19/svg/coeur-00.svg
Piece_author: Amandine Laval
Event_type: Création - performance
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=86
Subhead: Toutes les représentations sont complètes. Nous ouvrons une liste d'attente sur place, chaque soir, à partir de 19h.
Status: draft
Color: #fe5000


En ôtant sa culotte, en dézippant sa robe de soirée, en dégrafant sa lingerie sans en avoir l'air mais bien l'envie, en traquant le désir d'inconnus, en faisant jaillir leur excitation, en simulant le coup de foudre, le respect, l'amour véritable, compose-ton finalement un spectacle comme un autre ? Une strip-teaseuse amateure, seule en scène, se décharge des fictions accumulées.

Texte et interprétation
:   Amandine Laval

Musique
:   Pierre-Alexandre Lampert

Création lumière
:   Alice De Cat

Une production du Théâtre la Balsamine.
{: .production }

<div class="galerie" markdown=true>
![xx](/images/img-18-19/svg/coeur-01.svg)
![xx](/images/img-18-19/svg/coeur-02.svg)
![xx](/images/img-18-19/svg/coeur-03.svg)
</div>
<div class="galerie photos-spectacle" markdown=true>

</div>
