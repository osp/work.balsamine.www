Title: 3. George Kaplan
title_detail: de Frédéric Sonntag
Dates: 2019/11/26 14h00
       2019/11/26 → 2019/11/30
       2019/12/03 → 2019/12/04
Time: 20h30
Slug: georges
Key_image: img-19-20/programme/chapitre-3/pexels-photo-1172187-90.jpeg
Key_image_detail_body: img-19-20/programme/chapitre-3/pexels-photo-1172187-90.jpeg
Piece_author: Collectif RZ1GK
Event_type: Création théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=121
Color: #ff7c48

Quel est le lien entre un groupe activiste clandestin en pleine crise, une équipe de scénaristes en quête de nouveaux concepts pour un projet de série télé et un gouvernement invisible aux prises avec un danger menaçant la sécurité intérieure du pays ? Un seul nom : George Kaplan. Le collectif RZ1GK questionne en endossant ces figures multiples de la paranoïa, la mince frontière qui sépare le réel de la fiction.


Partition théâtrale ludique et pleine d’humour, *George Kaplan* dissèque 3 sphères d’influence différentes que sont la militance, l’écriture et le pouvoir pour mieux en révéler les dysfonctionnements.
{: .push-down}

Avec 
:   George Cagna, George Cheverry, George Dehasseler, George Garnier-Fourniguet, George Texier

Création lumières 
:   Rémy Urbain

Production déléguée 
:   Théâtre la Balsamine

Coproductions 
:   Théâtre la Balsamine, La Coop asbl et Shelter Prod

Soutiens 
:   taxshelter.be, ING, Tax-shelter du gouvernement fédéral belge, Buktapaktop, Epongerie

###  Action Tarif Ramène ton George

Pour les représentations de George Kaplan, nous vous proposons le tarif "Ramène ton George" :
Ramenez un.e ami.e se prénommant George à l'une des représentations et bénéficiez d'un tarif réduit de 7 euros et d'une boisson offerte.
Une belle occasion de mettre à l'honneur vos George d'amouuuur !

<div class="galerie photos-spectacle" markdown=true> 


<div class="galerie photos-spectacle" markdown=true>
![georgekaplan](/images/img-19-20/george_kaplan1.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan2.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan3.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan4.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan5.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan6.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/geroge_kaplan7.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan8.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan9.jpg){: .image-process-large}
:   © Hichem Dahes

![georgekaplan](/images/img-19-20/george_kaplan10.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe class="video"src="https://player.vimeo.com/video/371419784"></iframe>

<iframe class="video"src="https://player.vimeo.com/video/374889246"></iframe>

</div>


