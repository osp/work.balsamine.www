Title: Laure SoftLove
Date: 2020/04/04
Slug: xx-time-laure-softlove
Time: 19h00
Key_image_detail_body: img-19-20/photo_Laure_SoftLove.jpg
Event_type: Démonstration d’objets Sexy Chic dans le foyer de la Balsamine <br/> présenté dans le cadre du XX Time <br/> annulé
Status: draft
Color: #ffffff

« Là, tout n’est qu’ordre et beauté, luxe, calme et volupté. » L’invitation au voyage, Charles Baudelaire
 
Et c’est bien à un voyage que je vous invite, celui du monde de la sensualité… Un monde merveilleux, essentiel, accessible à tous, que notre vie trépidante nous fait parfois oublier… et pourtant, il suffit de peu de choses pour ajouter une touche de glamour à notre quotidien !
Venez découvrir nos produits, trucs et astuces pour une sexualité assumée et épanouie.
 
Présentés avec amour et humour par Laure, en collaboration avec Soft Love, complice de votre plaisir


[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)