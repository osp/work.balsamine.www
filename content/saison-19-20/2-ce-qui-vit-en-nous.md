Title: 2. Ce qui vit en nous
Date: 2019/11/05
End_date: 2019/11/09
Time: 20h30
Slug: ce-qui-vit
Key_image: img-19-20/programme/chapitre-2/stephie.png
Key_image_detail_body: img-19-20/programme/chapitre-2/stephie.png
Piece_author: Stéphane Arcas, Baudouin de Jaer et Martijn Dendievel
Event_type: Opéra de chambre en 3 actes - en français, surtitré en français et néerlandais
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=120
Partner_logo: /images/logos/ars-musica.svg


Au beau milieu d’un espace inconnu, un jeune homme amnésique tente de rassembler des bribes de souvenirs pour y trouver la raison de sa présence. Une infirmière le rejoint mais ne parvient pas à l’éclairer sur sa situation. Après diverses hypothèses absurdes, le rideau se lève sur la réalité : il s’agit d’une salle d’autopsie.


*Ce qui vit en nous* est une comédie chantée dans laquelle chacun des personnages pose la question de la mort et de son omniprésence dans l’esprit de tout être vivant. Tour à tour, ils dressent à leur façon le portrait de cette sensation d’« inquiétante étrangeté », comme la nommait Freud. Elle est le lieu où le quotidien se mue en ombre et où le vivant et l’inanimé entrent en collusion. Elle se loge ainsi dans tout ce qui nous est familier. Absente de nos pensées, dans le tourbillon de nos activités, la Faucheuse apparaît sans prévenir et l’on s’aperçoit, alors, qu’elle était là, depuis le début. Elle est ce qui vit en nous.
{: .push-down}


Livret, mise en scène et scénographie 
:   Stéphane Arcas

Composition
:   Baudouin de Jaer

Direction musicale
:   Martijn Dendievel

Chanteurs et comédiens
:   Sarah Théry (mezzo), Xavier de Lignerolles (ténor), Nicolas Luçon (acteur), Thomas Van Caekenberghe (baryton)

Musiciens
:   Ensemble Besides, Romy-Alice Bols (flûte), Jasper Braet (électronique), Toon Callier (guitare électrique), Fabian Coomans (clavier), Sam Faes (violoncelle), Nele Geubels (saxophone), Stefanie Van Backlé (violon), Jeroen Stevens (percussions), Jutta Troch (harpe)

Assistanat mise en scène
:   Cécile Chèvre

Concept spatial et dispositif
:   Stéphane Arcas, Claude Panier et Marie Szernovicz

Lumières
:   Julie Petit-Etienne

Ingénieur du son
:  Jean-Maël Guyot 

Assistante scénographie
:   Alice Guillou

Captation
:   Mathieu Haessler et Sonia Ringoot

Production déléguée 
:   Théâtre la Balsamine

Coproductions
:   BLACK FLAG ASBL, NOODIK productions, Théâtre la Balsamine, Ars Musica, La Coop asbl et Shelter Prod

Soutiens 
:   Forum des compositeurs, Fédération Wallonie-Bruxelles- Service du théâtre, taxshelter.be,  ING, Tax-shelter du gouvernement fédéral belge

### [Initiation à l'opéra et à la composition musicale](/saison-19-20/initiation.html)

## Baudouin de Jaer - entrée libre

5.11 → 9.11
{: .dates }

<div class="galerie photos-spectacle" markdown=true>
![cqven](/images/img-19-20/cequivit.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven2.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven3.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven4.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven5.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven6.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven7.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven8.jpg){: .image-process-large}
:   © Hichem Dahes

![cqven](/images/img-19-20/cqven9.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe class="video"src="https://player.vimeo.com/video/366967279"></iframe>

<iframe class="video"src="https://player.vimeo.com/video/370035246"></iframe>

<iframe class="video"src="<iframe width="560" height="315" src="https://www.youtube.com/embed/oCxpmmxRMVE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</div>

