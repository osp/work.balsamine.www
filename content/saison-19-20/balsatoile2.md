Title: Coco 
Date: 2019/11/13
Time: 15h00
Slug: balsatoile2
Category: balsatoiles
Length: 105 minutes
Age: 6 ans et plus
Piece_author: Lee Unkrich & Adrian Molina
Key_image:
Key_image_detail_body: img-19-20/coco.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=132
Event_type: Projection gratuite
Color: #6b89ed


Depuis déjà plusieurs générations, la musique est bannie dans la famille de Miguel. Un vrai déchirement pour le jeune garçon dont le rêve ultime est de devenir un musicien aussi accompli que son idole, Ernesto de la Cruz. 
Bien décidé à prouver son talent, Miguel, par un étrange concours de circonstances, se retrouve propulsé dans un endroit aussi étonnant que coloré : le Pays des Morts. Là, il se lie d’amitié avec Hector, un gentil garçon mais un peu filou sur les bords. Ensemble, ils vont accomplir un voyage extraordinaire qui leur révèlera la véritable histoire qui se cache derrière celle de la famille de Miguel…


Entrée libre mais réservation souhaitée !

Chaque film sera précédé, à 14h, d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=N3chVJm1XYI) 

Avec le soutien du Fonds Baillet Latour, dans le cadre de l'initiative "L’Extrascolaire au cœur de l’intégration" gérée par la Fondation Roi Baudouin.
{: .production }

Prochaines séances : 
- le 4 décembre 
- le 29 janvier 
- le 1er avril 
- le 6 mai
- le 3 juin
{: .pedagogique }