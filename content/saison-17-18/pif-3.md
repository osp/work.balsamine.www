Title: PIF 3 - Pauvre et Impudent Festival 3
Date: 2018/06/12
End_date: 2018/06/16
Event_type: Installations, performances, théâtre, danse, cirque, musique
Key_image: shows/1-Helpers/helper-cam-alone.png
Key_image_detail_body: gradient-char.png
Color: #ffee9e


Aujourd’hui, dans nos villes, la militarisation de l’espace public est devenue la réponse politique pour procurer un sentiment de sécurité. Puisque l’ennemi peut être partout et nulle part à la fois, nous sommes entrés dans une logique guerrière. Qu’en est-il de nos droits fondamentaux ? Dans une société de contrôle, l’art peut-il encore se déployer en toute impudence ? L’art se révèle une arme non-conventionnelle, elle déstabilise, questionne, réinvente, bouleverse. Pour cette 3ème édition du PIF, l’artillerie lourde est de sortie. Nous nous confronterons à notre propre mort, à celle de nos pères, aux liens maternels qui nous immobilisent à force de bienveillance et d’amour. Comment sortir de ces héritages qui nous écrasent ? Comment reformuler clairement nos espérances les plus fondamentales, les plus urgentes et les plus actuelles? Aux arts, citoyens, la vie est à nous.

> <footer>Monica Gomes, direction générale et artistique de la Balsamine</footer>


## Au programme :

**Le 12 juin** :

> 20h : [The Ghost Army](/saison-17-18/the-ghost-army.html) - Isabelle Bats / Boris Dambly

> 21h : [As a Mother of Fact](/saison-17-18/as-a-mother-of-fact.html) - notch company / oriane varak


**Le 13 juin** :

> 20h : [Nature Morte ou Naturellement Mort](/saison-17-18/nature-morte-ou-naturellement-mort.html) - Karin Vyncke

> 21h : [As a Mother of Fact](/saison-17-18/as-a-mother-of-fact.html) - notch company / oriane varak

> 22h : [Nature Morte ou Naturellement Mort](/saison-17-18/nature-morte-ou-naturellement-mort.html) - Karin Vyncke


**Le 14 juin** :

> 20h : [Jusqu'à preuve](/saison-17-18/jusqua-preuve.html)- Simon Loiseau / Marion Menan

> 21h : [Valhalla ou le crépuscule des dieux](/saison-17-18/valhalla-ou-le-crepuscule-des-dieux.html) - Petri Dish / Anna Nilsson / Sara Lemaire


**Le 15 juin** :

> Dès 19h : [Le Salon](/saison-17-18/le-salon.html)- Marine Prunier

> 20h30 : [Pattern](/saison-17-18/pattern.html) - Emilie Maréchal/ Camille Meynard


**Le 16 juin** :

> 20h : [Pattern](/saison-17-18/pattern.html) - Emilie Maréchal/ Camille Meynard

> 21h : [De-Siderium](/saison-17-18/de-siderium.html)- Chloé Winkel

> 22h : [Brussels Balkan Orchestra](/saison-17-18/brussels-balkan-orchestra.html ) 

> 23h : [The Ghost Army](/saison-17-18/the-ghost-army.html) - Isabelle Bats / Boris Dambly

> 23h30 : Dj Urba









