Title: 5. B4 summer
Subhead: In the context of Brussels dance ! Focus on contemporary dance
Date: 2020/02/04
End_date: 2020/02/08
Time: 20h30
Slug: b4-summer
Translation: True
Lang: en
Key_image: img-19-20/programme/chapitre-5/October_Rebellion_topless_protest_1-high.jpg
Key_image_detail_body: img-19-20/programme/chapitre-5/October_Rebellion_topless_protest_1-high.jpg
Piece_author: Mercedes Dassy
Event_type: Création danse
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=123
Color: #ff7c48
Partner_logo: /images/logos/brussels-dance.svg

Confused? Losing your bearings? Tired? Anxious? Or undecided but unsubdued? If you feel both the disillusion of revolutions as well as an indomitable feeling of revolt, welcome to *B4 summer* – The Game. In this creative dimension, you will be able to invent new fictions to counter our declining system. *B4 summer* is a permanent celebration of the body and the mind that is trying to free itself from imposed models.

In her previous solo *i-clit*, choreographer Mercedes Dassy explored a new wave of hyper-connected and hyper-sexual feminism, and questioned its ambivalent power of liberation and oppression. Drawing inspiration from both real and fictional contemporary representatives to question our (in)ability to act and commit ourselves, B4 summer is a natural and logical continuation. But where do you start when everything has to be taken apart and rebuilt? The clock is ticking. Answers expected before the summer.
*B4 summer* is a permanent celbration of the body and the mind that is trying to free itself from imposed models.
{: .push-down}

Concept, chorography and performance 
:   Mercedes Dassy

Dramturgy, artistic advice
:   Sabine Cmelniski

Sound design
:   Clément Braive

lighting design
:   Caroline Mathieu

costumes, scenography, accessories
:   Justine Denos

REPETITOR, outside view, voice coach
:   Judith Williquet

outside view
:   Jill De Muelenaere

DISTRIBUTION
:  Arts Management Agency (AMA)

EXECUTIVE PRODUCER
:   Théâtre la Balsamine / Arts Management Agency <br/>

CO-PRODUCERS
:   Théâtre de Liège, Théâtre la Balsamine, Kunstencentrum Vooruit, Charleroi danse - Centre chorégraphique de Wallonie-Bruxelles, Mars-Mons arts de la scène, La Coop asbl et Shelter Prod

SUPPORT 
:   Fédération Wallonie-Bruxelles - Service de la danse, [e]utopia, Bamp, taxshelter.be, ING et Tax-shelter du gouvernement fédéral belge


<div class="galerie photos-spectacle" markdown=true>
![b4summer](/images/img-19-20/visuel_b4_summer_-c-_Caroline_Mathieu__002_.jpg){: .image-process-large}
:   © Caroline Mathieu

![b4summer](/images/img-19-20/B4summer5109_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

![b4summer](/images/img-19-20/B4summer5138_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

![b4summer](/images/img-19-20/B4summer5191_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

![b4summer](/images/img-19-20/B4summer5481_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

<iframe class="video"src="https://player.vimeo.com/video/383048700"></iframe>

</div>