Title: Mon nom est clitoris
Subhead: La projection est complète. Nous ouvrons une liste d'attente sur place dès 19h. 
Date: 2020/03/31
Slug: xx-time-mon-nom-est-clitoris
Time: 20h00
Key_image_detail_body: img-19-20/Affiche_MNEC_A3.jpg
Piece_author: Lisa Billuart Monet et Daphné Leblond <br/>Projection annulée
Event_type: Film documentaire<br/> Dans le cadre du XX Time<br/>Entrée gratuite sur réservation
Status: draft
Color: #ffffff


Le clitoris, cet organe féminin pouvant atteindre onze centimètres de long et ayant pour unique fonction de procurer du plaisir, est encore trop méconnu. Pourquoi ? C’est ce que les deux réalisatrices ont tenté de comprendre. Douze jeunes femmes de 20 à 25 ans racontent le parcours de leur sexualité depuis l’enfance. Dans leur chambre, face caméra, elles s’adressent aux deux réalisatrices en proie aux mêmes questions. Elles se remémorent les premières sensations, les explorations hasardeuses, les conversations dans le noir et les obstacles inattendus.  

Toutes sont mues, chacune à leur manière, par un même élan : la quête d’une sexualité épanouissante, libre et égalitaire. Le film reconstruit un dialogue absent ou trop tardif et offre un espace pour repenser des inégalités qui se sont érigées en système social. 

écriture et réalisation
:  Lisa Billuart Monet et Daphné Leblond

production
:   Iota


[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)