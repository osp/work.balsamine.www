Title: i-clit
Date: 2018/02/27
Slug: i-clit
Translation: true
Lang: en
End_date: 2018/03/03
Time: 20h30
Key_image: shows/5-i-clit/shoes.png
Key_image_detail_body: shows/5-i-clit/scene-setup.png
Piece_author: Mercedes Dassy
Event_type: Dance show
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=24&cntnt01returnid=44
Subhead: In the context of Brussels, dance! Focus on contemporary dance.
Partner_logo: /images/logos/brussels-dance.svg
Intro: A spectacle-manifesto of the body, flesh, and in which the sexual object becomes the subject.
Color: #fce761

A spectacle-manifesto of the body, flesh, and in which the sexual object becomes the subject.

**i-clit** attempts a choreographed appraisal of the condition of contemporary feminism.
A new wave of feminism is born – ultra-connected, ultra-sexual and more popular. But faced with the ambivalent power of pop culture, where and how to position yourself in this battle as a young woman? What weapons should be used?

**i-clit** tracks these moments of fragility, where we easily slip from emancipation to a new form of oppression.

Concept, choreography, performance
:   Mercedes Dassy

Dramaturgy, outside opinion
:   Sabine Cmelniski

Scenography and light creation
:   Caroline Mathieu

Sound creation
:   Clément Braive

Costumes, scenography
:   Justine Denos

A Hotenslig production, jointly produced with Théâtre la Balsamine with the support of Théâtre Océan Nord, L’Escaut, B.A.M.P., Project(ion) Room and Friends with benefits.
{: .production }

<div class="galerie" markdown=true>
![iclit](/images/shows/5-i-clit/shoes-pillows-hd.png){: .image-process-large}

![iclit](/images/shows/5-i-clit/i-clit-composition-gamma-ok.png){: .image-process-large}
</div>
