Title: Pratique
Date: 2017/04
Slug: pratique
Translation: true
Lang: fr

[TOC]


## Accès
Tous les chemins mènent à la Balsa.

La Balsamine
:   Avenue Félix Marchal, 1
:   1030 Bruxelles

![plan d'accès de la balsamine](/images/img-19-20/pratique/map-19-20.png){: class="map"}

![plan d'accès de la balsamine](/images/img-19-20/pratique/map-19-20-zoom.png){: class="map"}

* * *
* * *

Administration
:   [+32&nbsp;2&nbsp;732&nbsp;96&nbsp;18](tel:+3227329618)
:   [info@balsamine.be](mailto:info@balsamine.be)

Réservation
:   [+32&nbsp;2&nbsp;735&nbsp;64&nbsp;68](tel:+3227356468)
:   [reservation@balsamine.be](mailto:reservation@balsamine.be)

![logo accessibilite](/images/pictogramme-personne-a-mobilite-reduite.jpg.svg) L’accessibilité de nos salles aux personnes à mobilité réduite est possible mais n’est pas aisée. Lors de votre réservation, nous vous remercions de faire une demande pour cet accès.
{: .footnote }


## Bar et restauration

Le bar est ouvert à 19h00 chaque soir de représentation. Une petite restauration délicieuse et faite sur place vous est proposée avant chaque spectacle.


## Autour des spectacles

Défendre la nécessité de l’Art pour l’Art mais le rendre accessible au plus
grand nombre est une utopie qui nous anime et dont nous entendons prendre
responsabilité. Le Théâtre la Balsamine est une maison de création qui
abrite et accompagne des artistes confrontés quotidiennement à la
nécessité de s’interroger sur le sens, sur le cadre dans lequel ils
exercent leur pratique, sur le style qu’ils élaborent et sur les valeurs
qu’ils représentent. Quand un artiste entreprend de donner forme à sa
perception, à sa compréhension du monde et à ce qu’il souhaite énoncer,
il n’a pas pour projet d’être inoffensif, de conforter le consensus. La
création contemporaine est une forme mouvante et en perpétuel
questionnement, qui renvoie le spectateur à sa capacité à créer du sens
et à vivre son rapport au monde. Elle nous interpelle sur notre propre
créativité et sur notre capacité à exprimer qui nous sommes. Elle
interroge les normes, y compris celles du goût. C’est un questionnement
précieux que nous entendons partager avec le grand public.

Contact conseils et organisation des activités pédagogiques
:  Noemi Tiberghien
:  [+32 2 737 70 18](tel:+3227377018)
:  [noemi.tiberghien@balsamine.be](mailto:noemi.tiberghien@balsamine.be)

* * *

### Les aftershows

À l’issue de toutes les deuxièmes représentations des créations, la
Balsamine vous propose une rencontre avec l’équipe artistique. Un moment
privilégié pour débattre et échanger en toute convivialité. Petite
particularité, cette rencontre est menée par un autre artiste de la saison.


### Le public scolaire

Si le théâtre classique offre des repères rassurants, il ne traduit pas
toujours le caractère hybride et complexe du monde dans lequel nous
sommes amenés à évoluer.

Emmener les jeunes au théâtre pour y voir de la création contemporaine
est une occasion de les familiariser avec des codes qui leurs sont
inhabituels, les amener à déchiffrer des représentations de l’aujourd’hui.

Faire vivre l’expérience du contemporain aux jeunes peut être aussi une
façon de les aider à démonter un préjugé trop souvent répandu selon
lequel l’art théâtral serait une discipline poussiéreuse et archaïque,
étrangère à leur culture et à leurs questionnements, qui ne leur
appartiendrait pas, qui leur serait inaccessible.

N’hésitez donc pas, si un spectacle de notre saison vous donnait l’envie
de mettre en relation vos étudiants avec la pratique artistique, à nous
faire part de vos idées.

Nous organisons régulièrement des dynamiques d’échanges entre étudiants
et professionnels, sous différentes formes en fonction des projets
spécifiques. Pour exemples: captations vidéo, stages d’observation, interviews
d’artistes, séances de croquis, ateliers créatifs…

<!--[Télécharger le guide pédagogique]()-->

##  Logos

- [logo balsamine 19-20](/images/img-19-20/logos/balsa19-20-logo-couleur.svg){: download }
- [logo Label United Stages](/images/label-united-stages-txt.svg){: download }
<!-- - [fonts]() -->
<!-- - [dossiers de diffusion]() -->

<!-- Nouveau logo Balsa, Label United Stages, fonts et dossiers de
diffusion à télécharger-->

## Technique

Ici, vous trouverez tous les plans techniques de la Balsamine.
{: .en-block}

- [Balsa Petite salle.pdf](/images/shows/Balsa%20Petite%20salle.pdf){: download }
- [Balsa sol amphi.pdf](/images/shows/Balsa%20sol%20amphi.pdf){: download }
- [coupe grande salle.pdf](/images/shows/coupe%20grande%20salle.pdf){: download }
- [Grande Salle vierge.pdf](/images/shows/Grande%20Salle%20vierge.pdf){: download }
- [Grill grande salle.pdf](/images/shows/Grill%20grande%20salle.pdf){: download }
- [Grill petite salle.pdf](/images/shows/Grill%20petite%20salle.pdf){: download }
- [sol grande salle.pdf](/images/shows/sol%20grande%20salle.pdf){: download }
- [Sol petite salle.pdf](/images/shows/Sol%20petite%20salle.pdf){: download }
