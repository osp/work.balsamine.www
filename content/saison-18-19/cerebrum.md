Title: CEREBRUM, le faiseur de réalités
Dates: 2019/05/29 20h30
Slug: cerebrum
Key_image:
Key_image_detail_body:
Piece_author: Compagnie Yvain Juillard
Event_type: Théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=108
Color: #006cdb

Ancien biophysicien spécialisé dans la plasticité cérébrale, aujourd’hui acteur, Yvain Juillard nous propose lors d’une conférence spectacle d’interroger le fonctionnement de notre cerveau afin de questionner la nature multiple de la réalité. Le cerveau, siège de notre mémoire, de nos perceptions, de notre identité, demeure cet organe à la fois intime, mystérieux car méconnu par la plupart d’entre nous. Ce spectacle, à travers des expériences simples et ludiques, désire transmettre au public les dernières connaissances scientifiques en la matière. Une occasion unique de débattre simplement  des récentes découvertes des neurosciences.

Avec
:  Yvain Juillard

Conception, écriture et mise en scène
: Compagnie Yvain Juillard

Régie son
:   Marc Doutrepont

Régie générale et lumières
:   Vincent Tandonnet

Graphisme
:   Robin Yerlès

Conseils neuroscientifiques
:   Yves Rossetti (CNRS-INSERM)

Production, diffusion, presse
: Isabelle Jans et Stéphanie Barboteau

Merci à 
: Laurent Wanson, Joseph Lacrosse et Olivier Boudon pour leurs regards.


Les étapes qui ont conduit au spectacle ont bénéficié du soutien du Théâtre de Namur/Centre Dramatique, du CORRIDOR et de la Fabrique de Théâtre, d’Aube Boraine/Mons 2015, du Théâtre de la Balsamine, du Centre Culturel de Colfontaine, du Théâtre Varia, de la Fédération Wallonie-Bruxelles/Service des projets pluridisciplinaires et transversaux
et de Wallonie-Bruxelles International.
Bourse SACD 2015 « Un ticket pour Avignon ».
Lauréat 2016 d’une bourse d’écriture à la Chartreuse de Villeneuve-lès-Avignon.
{: .production }


<div class="galerie photos-spectacle" markdown=true>
</div>

<div class="galerie photos-spectacle" markdown=true>
![cerebrum](/images/img-18-19/Cerebrum32.jpg){: .image-process-large}
:   © Hichem Dahes

![cerebrum](/images/img-18-19/Cerebrum29.jpg){: .image-process-large}
:   © Hichem Dahes

![cerebrum](/images/img-18-19/Cerebrum30.jpg){: .image-process-large}
:   © Hichem Dahes

![cerebrum](/images/img-18-19/Cerebrum27.jpg){: .image-process-large}
:   © Hichem Dahes

![cerebrum](/images/img-18-19/Cerebrum10.jpg){: .image-process-large}
:   © Hichem Dahes

![cerebrum](/images/img-18-19/Cerebrum15.jpg){: .image-process-large}
:   © Hichem Dahes

![cerebrum](/images/img-18-19/Cerebrum12.jpg){: .image-process-large}
:   © Hichem Dahes
