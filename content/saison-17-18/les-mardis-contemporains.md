Title: Les mardis contemporains
Date: 2017/10/03
Dates: 2017/10/03 20h30, complet
       2017/11/14 20h30
       2017/11/21 20h30
Time: 20h30
Status: Draft
Subhead: En partenariat avec Ars Musica
Partner_logo: /images/logos/ars-musica.svg
Event_type: Musique
Key_image: shows/1-Helpers/helper-wind-grid.png
Key_image_detail_body: shows/1-Helpers/helper-wind-grid.png
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=30&cntnt01returnid=44
Color: #46cea0

**Les mardis contemporains** proposent aux oreilles curieuses de découvrir des facettes de la musique d’aujourd’hui, dans un programme pluriel qui dialogue avec l’histoire, la philosophie, la sociologie, mais aussi le cirque, le cinéma, la science ou l’architecture…
Le temps de 3 concerts-rencontres, **les mardis contemporains** prennent leurs quartiers à la Balsamine.

* * *

* * *

## Octobre rouge

### Le 3 octobre à 20h30

Seconde phase de la révolution bolchévique, ce mois d’octobre 1917 changera la face du monde. La Russie, affaiblie par une entrée en guerre désastreuse pour son armée, laisse le pouvoir aux soviets, mettant en place le premier régime communiste dans le sens marxiste du terme. Au fil des soubresauts du fil politique soviétique, les compositeurs russes ont oscillé entre résignation oui exil face au pouvoir. Hôte des mardis contemporains, le quatuor Amôn interprètera ce répertoire aux esthétiques plurielles et créera une œuvre de la jeune compositrice Alice Hebborn et c’est l’historien Jean-Jacques Marie, auteur de nombreux ouvrage sur l’U.R.S.S, qui évoquera cet octobre 1917, véritable changement de paradigme du monde.
{: .production }

Invité
:    Jean-Jacques Marie (historien, auteur de Lénine, la révolution permanente, Ed. Payot)

Œuvres
:    Deux pièces pour quatuor à cordes / Elégie — Polka de Dmitri Chostakovitch (1974)
:    Quatuor à cordes N°1 de Dmitri Kabalevski — extrait (1928)
:    Quatuor N°3 d’Arthur Lourié — extrait (1926)
:    Quatuor N°3 de Nikolai Roslavets (1920)
:    Trio à cordes d’Edison Denisov (1969)
:    Création de Alice Hebborn pour quatuor à cordes (commande Ars Musica 2017)

Interprétation
:    Quatuor Amôn

* * *

* * *

## Architecture

### Le 14 novembre à 20h30

Invités
:    Francis Metzger et François Schuiten

Œuvres
:    Tetras de Iannis Xenakis (1983)
:    Quatuor à cordes n°2 de Luis Naon extrait du cycle Urbana (1999–2001)
:    City Life de Steve Reich (1995)
:    Création de Stéphane Orlando (commande Ars Musica 2017)

Direction
:    Thomas Van Haeperen

Interprétation
:    Quatuor TANA / Ensemble Sturm & Klang

* * *

* * *

## Cirque

### Le 21 novembre à 20h30

Invités
:     Julien Rosemberg, Directeur Général adjoint — Pôle National Cirque et Arts de la rue

Œuvres
:    Sonate n°2 pour violoncelle de Gyorgy Ligeti (1953)
:    Circus Polka d’Igor Stravinsky (1942)
:    March for Piano: The Circus Band de Charles Ives (1898-1899)
:    Création de Stefan Hejdrowski (commande Ars Musica 2017)

Interprétation
:    Guy Danel, violoncelle
:    Gabi Sultana, piano
:    Elsa Bouchez et Philippe Droz, artistes de cirque

<div class="galerie" markdown=true>
![mardi contenporains ars musica](/images/Visuel_ars-musica.jpg){: .image-process-large}
</div>
