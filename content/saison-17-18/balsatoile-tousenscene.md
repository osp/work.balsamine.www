Title: Tous en scène (Sing)
Date: 2018/05/02
Time: 15h00
TimeEnd: 16h50
Category: balsatoiles
Length: 108'
Age: 6 ans et plus
Piece_author: Garth Jennings
Event_type: Balsatoiles
Color: #ce84b5
Key_image_detail_body:  Tous_en_scene.jpg 
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=55

Buster Moon, un koala, est le propriétaire d'un théâtre au bord de la faillite. Ses spectacles n'ont jamais attiré grand monde et le bâtiment menace de s'écrouler. Buster décide alors, pour renflouer ses caisses, d'organiser un grand concours de chant afin de redonner de la gloire à son théâtre. Seulement, une faute de frappe et la récompense du concours passe de 1 000 à 100 000 dollars, attirant une foule d'animaux prêts à tenter leur chance pour réaliser leurs rêves.

Un atelier créatif, animé par un artiste plasticien, est proposé de 14h à 15h.
(inscription obligatoire – nombre de place limité)


Projet réalisé avec le soutien du Fonds Baillet Latour, dans le cadre de l’initiative L’extrascolaire au cœur de l’intégration gérée par la Fondation Roi Baudouin. Avec l’aide du Réseau Action Culturelle Cinéma (R.A.C.C.) 
{: .production }

[Voir la bande-annonce du film](http://www.allocine.fr/video/player_gen_cmedia=19565998&cfilm=227066.html)
{: .production }
