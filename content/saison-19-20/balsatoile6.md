Title: La Tortue rouge   
Date: 2020/05/06
Time: 15h00
Slug: balsatoile6
Category: annulé
Length: 81 minutes
Age: 6 ans et plus
Piece_author: Michael Dudok de Wit <br/> Projection annulée
Key_image:
Key_image_detail_body: img-19-20/tortue4.jpg
Event_type: Projection gratuite
Color: #6b89ed


Un homme échoue sur une île déserte tropicale. Seul, il doit apprendre à survivre grâce à la nature, pas toujours accueillante, avec pour seuls compagnons les oiseaux et de petits crabes facétieux. Mais alors qu’il tente de s’enfuir sur son radeau d’infortune, il fait la rencontre d’une mystérieuse tortue sortie de l’eau...

Entrée libre mais réservation souhaitée !

Chaque film sera précédé, à 14h, d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=kLHMcYygArc) 

Avec le soutien du Fonds Baillet Latour, dans le cadre de l'initiative "L’Extrascolaire au cœur de l’intégration" gérée par la Fondation Roi Baudouin.
{: .production }


Prochaine séance : 
- le 3 juin
{: .pedagogique }