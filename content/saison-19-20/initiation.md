Title: Initiation à l'opéra et à la composition musicale
Date: 2019/11/05
Slug: initiation
End_date: 2019/11/09
Time: 19h30
Key_image_detail_body: img-19-20/baudoin_-_crédit_Noodik_production.JPG
Piece_author: Baudouin de Jaer
Event_type: entrée libre  <br/> avant chaque représentation de "Ce qui vit en nous" 
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=148
Status: draft
Color: #7fa22a

Composer de la musique paraît souvent inaccessible et pourtant ! Baudouin de Jaer, compositeur et chef d’ensemble, a développé une méthode bien à lui pour nous permettre de s’y essayer avec assurance et bonne humeur.

Après une première introduction à Ce qui vit en nous, pour lequel il a composé la musique et le livret, Baudouin de Jaer nous fera traverser 7 années de solfège en 7 minutes (il faut le voir pour le croire, vous ne serez pas décu.e.s) pour ensuite passer directement de la théorie à la pratique avec la création d’un nano opéra.

Une initiation ludique, extrêmement instructive et  qui risquerait bien de susciter de nouvelles vocations...
