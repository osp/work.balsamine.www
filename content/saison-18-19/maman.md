Title: Maman (je vois sans yeux et sans bouche je crie) 
Date: 2018/11/28
Slug: maman
Time: 20h30
Key_image: 
Key_image_detail_body:img-18-19/maman13.jpg
Piece_author: Hamadi
Event_type: Théâtre 
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=98
Color: #ffffff
Status: draft

Hamadi est un artiste complet ; il est écrivain, metteur en scène et acteur. Au cours de ces 30 dernières années, il a influencé de son empreinte la scène théâtrale belge avec des textes forts à l’esthétique puissante et efficace. Avec ses créations précédentes « Papa est en voyage », « Barbarians », « Sans ailes et sans racines », Hamadi jette un regard clair et tranchant sur les thèmes aujourd’hui au cœur du débat.
{: .pedagogique }

Maman, c’est une histoire d’amour universelle et intemporelle, la plus vieille qui soit…
Maman, c’est cette figure qui vous marque au fer rouge. Une présence vitale dont on cherche à s’affranchir, mais dont on regrette déjà le départ trop rapide et pourtant inéluctable…
Maman, c’est cette mère immigrée, qui suscite l’admiration de son fils devenu adulte, mais qui a généré bien des moments de honte au petit garçon qui aurait tant aimé passer inaperçu.
Maman, c’est aussi cette ville qu’un jeune berbère marocain de 10 ans va adopter, à moins que ce ne soit le contraire...
Maman, c’est une kyrielle de personnages ancrés dans un terroir bruxellois, sa langue, son accent, sa jovialité et son syncrétisme.
Hamadi rend un vibrant hommage à cette ville et ses habitants anonymes qui ont émaillé son enfance et ont laissé en lui une trace indélébile.
{: .pedagogique }

Texte, interprétation, chants et mise en scène 
:  Hamadi

Regard extérieur 
:   Soufian El Boubsi

Création lumières et vidéo
:  Frédéric Nicaise 

Chants et musique 
:   Morgiane El Boubsi & Romain Dandoy

Production
:  Compagnie Hamadi

Diffusion 
: La Charge du Rhinocéros

Avec l’aide de la SACD
{: .production }

> Avant la représentation : 
{: .pedagogique }

> 15h : Ahmed Massaïa: introduction “théâtre au Maroc – des origines jusqu'au XXIe siècle" + conversation avec Ahmed Massaïa - en français 
{: .pedagogique }

> entrée libre ( réservation souhaitée ) - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=101)
{: .pedagogique }

> 16h30 - Table ronde avec Ahmed Massaïa, le metteur en scène Mohamed Elhor et une comedienne du Akoun Theater. Moderation: Soraya Amrani - en français 
{: .pedagogique }

> entrée  libre ( réservation souhaitée ) - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=102)
{: .pedagogique }

<div class="galerie" markdown=true>
![maman](/images/img-18-19/maman1.jpg){: .image-process-large}
![maman](/images/img-18-19/maman2.jpg){: .image-process-large}
![maman](/images/img-18-19/maman3.jpg){: .image-process-large}
![maman](/images/img-18-19/maman4.jpg){: .image-process-large}
![maman](/images/img-18-19/maman5.jpg){: .image-process-large}
![maman](/images/img-18-19/maman6.jpg){: .image-process-large}
![maman](/images/img-18-19/maman8.jpg){: .image-process-large}
![maman](/images/img-18-19/maman9.jpg){: .image-process-large}
![maman](/images/img-18-19/maman10.jpg){: .image-process-large}
![maman](/images/img-18-19/maman13.jpg){: .image-process-large}
![maman](/images/img-18-19/maman14.jpg){: .image-process-large}
</div>

