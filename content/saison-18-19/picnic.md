Title: Pic-Nic Rendez-vous
Date: 2019/04/30
Slug: pic nic
Time: 10h15
Category: ReForm 
Key_image_detail_body: img-18-19/picnic2.png
Piece_author: Zirk Théâtre
Event_type: Spectacle théâtral et musical jeune public - en collaboration avec ReForm asbl
Reservation_link: https://reform.be/nos-activites/en-classe/theatre-a-lecole/
Color: #ffffff

Missis Flower passe chaque jour à la boutique de Mister Daffodil pour acheter un petit bouquet de fleurs. 
Un jour, celui-ci prend son courage à deux mains et l'invite à un pic-nic, elle lui a confié qu'elle adore ça !

Nos deux célibataires se retrouvent donc dans un petit coin de campagne anglaise pour ce fameux "Pic-nic Rendez-vous".
Les évènements fabuleux qui les attendent - un éléphant de passage, une famille de grenouilles égarée, une drôle de tourterelle et un drôle de pigeon, une attaque de mouches, une fanfare… – vont les pousser à faire connaissance, d’une manière bien plus étonnante qu’autour d’une "nice cup of tea". 

Durant cette journée ensemble, où rien ne se passe comme prévu, ils vont faire face à leurs timides maladresses, surmonter leurs peurs et écouter leur irrésistible envie de découvrir ce qui pourrait peut être devenir une romance. 
Ce spectacle musical inspiré des chefs d'œuvres tel que "Mary Poppins" et "Singing in the rain" sera chanté en français et en anglais. Un rendez-vous au parfum de campagne anglaise, plein de légèreté et d’humour.


De et avec
: Rachel Ponsonby & Perry Rose

Régie & création lumière
:   Antoine Clette

Écriture & composition originales
:   Perry Rose & Rachel Ponsonby

Regard exterieur & coaching
:  Christophe Thellier & Jean-François Brion. 

Décor & peinture sur costumes
:  Maria Brouillard

Accessoires 
: Rachel Ponsonby

Remerciement
: Louis Spagna, Micheline Vandepoel, Aurélie Coppens, Laure Saupique, Adam Brooking, Barbara Demaret, Arnaud Van Hammée


ce spectacle a remporté 2 prix de la part de la Ministre Alda Greoli à la Vitrine en Chansons 2018.
{: .production }

En savoir plus sur [le Zirk Théâtre](http://zirktheatre.be/fr/)
{: .pedagogique }
