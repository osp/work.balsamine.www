Title: Should i stay or should i stay
Date: 2017/11/16
Dates: 2017/11/16 20h30
       2017/11/20 20h30
       2017/11/22 20h30
       2017/11/23 20h30
       2017/11/24 20h30
Piece_author: Simon Thomas
Event_type: Reprise Théâtre<br/>Spectacle en français surtitré en anglais.
Key_image: shows/7-should-i-stay-or-should-i-stay/should-i-stay-composition1.png
Key_image_detail_body: shows/7-should-i-stay-or-should-i-stay/anim-chat.gif
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=22&cntnt01returnid=44
Intro: Les quatre fantastiques reviennent et reprennent leur rengaine : Should I…
Color: #3b4086
Translation: true
Lang: fr
Slug: should-i-stay-or-should-i-stay

## Toutes les représentations sont complètes. Nous ouvrons une liste d'attente chaque soir, dès l'ouverture des portes à 19h.

Les quatre fantastiques reviennent et reprennent leur rengaine : Should I…

Ils sont quatre, ils sont enfermés dans une pièce dont les portes ne sont pas verrouillées et pourtant ils ne peuvent pas sortir. Un casse-tête bien surréaliste et qui nous pose des questions très existentialistes.

Qui sont-ils, d’abord? Comment les appelle-t-on? Gabor? Paola? Héraclès? Falbala? Téflon? Gargan? Lazlo? Axel? Cyrus? Mia? Edmée? Augustine?

Où sont-ils coincés? Près d’un lac? Dans une fissure du temps? Sur une scène? Dans une autre dimension?

Ils parlent de mort, d’exil sur Mars, d'accès à la vie éternelle, du temps qui passe, d'enlisement, de violence gratuite… Peu à peu, un système se met en place entraînant les protagonistes dans un trou noir. La suite des évènements, l’alignement des planètes, le hasard, et même la chance sont pour beaucoup dans la résolution de leur impasse. Les carottes sont-elles cuites ou pas? À revoir, donc, puisqu’il s’agit d’une reprise. Non? Mais oui, mais oui.

Écriture et mise en scène
:    Simon Thomas

Avec
:    Jules Churin
:    Héloïse Jadoul
:    Manon Joannotéguy
:    Lucas Meister

Costumes 
:    Camille Flahaux

Surtitrage
:   Chloe McCarty, Simon Thomas

Premières élaborations au sein de l'Institut National Supérieur des Arts du Spectacle et des Techniques de Diffusion de la Fédération Wallonie-Bruxelles. Réalisé avec les soutiens du Théâtre la Balsamine, avec l'aide de la Fédération Wallonie-Bruxelles — Service du Théâtre et le soutien du Fonds Marie-Paule Delvaux Godenne, géré par la Fondation Roi Baudouin.
{: .production }

<div class="galerie" markdown=true>
![chat](/images/shows/7-should-i-stay-or-should-i-stay/cat-bake-tex-split-3.png){: .image-process-detail}

![chat](/images/shows/7-should-i-stay-or-should-i-stay/chat-3-queues.png){: .image-process-large}

![chat](/images/shows/7-should-i-stay-or-should-i-stay/cat-bake-tex-split-1.png){: .image-process-large}

![chat](/images/shows/7-should-i-stay-or-should-i-stay/chat-3-queues-transparent.png){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis1.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis2.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis3.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis4.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis5.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis6.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis7.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis8.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis9.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis10.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis11.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis12.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis13.jpg){: .image-process-large}

<iframe class="video" src="https://player.vimeo.com/video/242549969"></iframe>

</div>
