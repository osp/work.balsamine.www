Title: Paddington 2
Date: 2019/02/06
Time: 15h00
Slug: balsatoile3
Category: balsatoiles
Length: 1h43
Age: pour tous à partir de 8 ans 
Piece_author: de Paul King
Key_image:
Key_image_detail_body: img-18-19/paddington_2_2017-wide-1366x750.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=94
Event_type: Projection gratuite
Color: #ffffff

Paddington coule des jours heureux chez les Brown, sa famille d’adoption, dans un quartier paisible de Londres, où il est apprécié de tous. Alors qu’il recherche un cadeau exceptionnel pour les cent ans de sa tante adorée, il repère un magnifique livre animé, très ancien, chez un antiquaire. Pas de temps à perdre : il enchaîne les petits boulots pour pouvoir l’acheter ! Mais lorsque le précieux ouvrage est volé, Paddington est accusé à tort et incarcéré. Convaincus de son innocence, les Brown se lancent dans une enquête pour retrouver le coupable…
Entrée libre mais réservation souhaitée !

Chaque film sera précédé d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](http://www.allocine.fr/video/player_gen_cmedia=19575228&cfilm=242201.html) 
{: .production }

Prochaines séances : 
- le 23 mars 
- le 24 avril
- le 15 mai
{: .pedagogique }
