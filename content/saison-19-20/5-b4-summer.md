Title: 5. B4 summer
Subhead: Dans le cadre de Brussels dance ! Focus on contemporary dance. Toutes les représentations sont complètes. Une liste d'attente est ouverte chaque soir de représentation dès 19h.
Date: 2020/02/04
End_date: 2020/02/08
Time: 20h30
Slug: b4-summer
Key_image: img-19-20/programme/chapitre-5/October_Rebellion_topless_protest_1-high.jpg
Key_image_detail_body: img-19-20/programme/chapitre-5/October_Rebellion_topless_protest_1-high.jpg
Piece_author: Mercedes Dassy
Event_type: Création danse
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=123
Color: #ff7c48
Partner_logo: /images/logos/brussels-dance.svg

Dans un processus ultra-désiré d’engagement pour une lutte globale, B4 summer fait résonner un cri. Un cri de labeur affectif, face à la question infinie de l’engagement. Comment affronter la complexité de cette matière à 100% ? Comment se laisser submerger par ce chaos sans reculer ? Comment négocier honnêtement avec la vulnérabilité et le sentiment d’impuissance dans lesquels ces questions peuvent faire plonger ? Comment estimer l’authenticité ou la légitimité de son engagement ?

Mercedes Dassy utilise son corps comme arme, comme outil, comme obstacle, comme espace d’exploration et encore comme sujet-objet, pour tenter d’articuler ce cri.

Influencée par des sources esthétiques et artistiques très diverses (la culture pop, la musique, l’art post-internet, etc), la chorégraphe et danseuse bruxelloise Mercedes Dassy présente B4 summer comme la deuxième partie d’un cycle de performances. Dans i-clit, son oeuvre précédente, elle explorait une nouvelle vague de féminisme ultra-connectée et ultra-sexuée afin d’en interroger le pouvoir ambivalent d’émancipation et d’oppression. La danseuse inscrit sa nouvelle création dans la continuité de sa quête, en s’inspirant de représentantes réelles, symboliques ou fictionnelles pour sonder notre (in)capacité à agir et à nous engager.

«  Changer le monde. Une idée, un projet, des organisations, pendant un moment il a été même question de victoire. L’histoire semblait enceinte de vérité. Nous sommes venus après, enfants de la désillusion et des mauvais rêves. Le renversement du capitalisme était avorté, son désir même rentrait en phase de désintégration symbolique. Les mots étaient trop usés quand ils n’étaient pas trop sages. Alors ils nous ont appris à crier. » Printemps précaires des peuples, Maria Kakkogianni.
{: .push-down}

Concept, chorégraphie et interprétation
:   Mercedes Dassy

Dramaturgie, conseil artistique
:   Sabine Cmelniski

Création sonore
:   Clément Braive

Création lumières
:   Caroline Mathieu

Costumes, accessoires, scénographie
:   Justine Denos

Répétitrice, regard extérieur, coach voix
:   Judith Williquet aka Judith Kiddo

Regard extérieur
:   Jill De Muelenaere

Diffusion
:   Arts Management Agency (AMA)

Production déléguée 
:   Théâtre la Balsamine / Arts Management Agency <br/>

Coproductions 
:   Théâtre de Liège, Théâtre la Balsamine, Kunstencentrum Vooruit, Charleroi danse - Centre chorégraphique de Wallonie-Bruxelles, Mars-Mons arts de la scène, La Coop asbl et Shelter Prod

Soutiens 
:   Fédération Wallonie-Bruxelles - Service de la danse, [e]utopia, Bamp, taxshelter.be, ING et Tax-shelter du gouvernement fédéral belge

##avant premières: les 22 et 23.01 au Kunstencentrum Vooruit

##premières: les 31.01 et 01.02 au Théâtre de Liège dans le cadre de Pays de Danses 2020

##tournée: le 3.04 au Mars-Mons Arts de la scène dans le cadre de l'éco festival Demain


<div class="galerie photos-spectacle" markdown=true>
![b4summer](/images/img-19-20/visuel_b4_summer_-c-_Caroline_Mathieu__002_.jpg){: .image-process-large}
:   © Caroline Mathieu

![b4summer](/images/img-19-20/B4summer5109_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

![b4summer](/images/img-19-20/B4summer5138_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

![b4summer](/images/img-19-20/B4summer5191_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

![b4summer](/images/img-19-20/B4summer5481_web__c__Michiel_Devijver.jpg){: .image-process-large}
:   © Michel Devijver

<iframe class="video"src="https://player.vimeo.com/video/383048700"></iframe>

</div>
