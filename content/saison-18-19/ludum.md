Title: Ludum
Slug: Ludum
End_date: 2019/04/05
Time: 20h30
Date: 2019/04/02
Key_image: img-18-19/svg/ludum-00.svg
Key_image_detail_body: img-18-19/svg/ludum-00.svg
Piece_author: Anton Lachky
Event_type: Création danse <br/> Dans le cadre de Brussels, dance ! Focus on contemporary dance.
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=88
Partner_logo: /images/logos/brussels-dance.svg
              /images/logos/wbtd-banner-positif-842x292.png
Color: #fe5000

Les habitants de Ludum présentent tous une particularité, une bizarrerie : ils sont libres d'être et de faire ce qu'ils veulent. À Ludum, société ultra intégrante où les lubies des uns et des autres coexistent en parfaite harmonie, le jeu est loi.

Avec
:    Guilhem Chatir, Lewis Cooke,  Àngel Duran,  Anna Karenina Lambrechts, Hyaejin Lee, Maria Manoukian, Patricia Rotondaro et Ioulia Zacharaki

Concept et chorégraphie
:   Anton Lachky

Lumière / Son 
: Tom Daniels

Costumes 
: Britt Angé

Production et diffusion 
: Éléonore Valère-Lachky


Production déléguée
: Théâtre la Balsamine

Une production d'Anton Lachky Company, en coproduction avec le Théâtre la Balsamine, Charleroi danse, Le Step Festival/Migros et La Coop asbl.Avec le soutien de la Fédération Wallonie-Bruxelles- Service de la danse, de Shelterprod, de taxshelter.be, d’ING et du Tax-shelter du gouvernement fédéral belge.
{: .production }

<div class="galerie" markdown=true>
![ludum](/images/img-18-19/svg/ludum-01.svg)
![ludum](/images/img-18-19/svg/ludum-02.svg)
![ludum](/images/img-18-19/svg/ludum-03.svg)

</div>
<div class="galerie photos-spectacle" markdown=true>
![ludum](/images/img-18-19/ludum_équipe_5.jpg){: .image-process-large}
:  -

![ludum](/images/img-18-19/ludum1.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/Ludum2.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/Ludum3.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/Ludum4.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum5.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum6.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum7.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum8.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum9.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum10.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum11.jpg){: .image-process-large}
:   © Hichem Dahes

![ludum](/images/img-18-19/ludum12.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe class="video" src=https://player.vimeo.com/video/326761496></iframe>

<iframe src="https://player.vimeo.com/video/326762987"></iframe>

</div>
