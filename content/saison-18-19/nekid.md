Title: néKid#2
Date: 2019/06/11 
Time: 20h30
Slug: ne-kid
Key_image_detail_body:img-18-19/tom_malmendier.jpg
Piece_author: Tom Malmendier & Sab Nicole
Event_type: Performance acoustique et picturale 
Subhead: Dans le cadre du PIF4 - Entrée libre
Status: draft
Color: #009933

Tom Malmendier tout de blanc vêtu et sa batterie assortie se font malmener et secouer par les interventions plutôt musclées de Sab Nicole, une artiste plasticienne bien décidée à y mettre un peu de couleurs. 
Une ouverture percussive et haute en couleurs ! 


Concept et batterie 
:  Tom Malmendier

Attaques de peinture 
:   Sab Nicole

Soutien
:   Théâtre la Balsamine
{: .production }

