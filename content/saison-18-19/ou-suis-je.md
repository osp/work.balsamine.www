Title: Où suis-je&nbsp;? Qu'ai-je fait&nbsp;?
Dates: 2018/11/2 → 2018/11/3 20h30
       2018/11/5 au 2018/11/10 20h30
Slug: ou-suis-je
Key_image: img-18-19/svg/ou-suis-je-01.svg
Key_image_detail_body: img-18-19/svg/ou-suis-je-01.svg
Piece_author: Pauline d'Ollone
Event_type: Création théâtre
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=79
Color: #ff51a0

Après Reflets d’un Banquet*, Pauline d’Ollone dresse le portrait d’êtres en quête d’amour et de reconnaissance, prêts à s’aliéner pour fuir un réel trop décevant.

Des amoureux fanatiques, une apprentie-despote, une coach en développement personnel, un quadragénaire qui cherche l’amour sur Internet… Où suis-je ? Qu’ai-je fait ?, trois histoires, trois formes d’asservissement volontaire dans lesquelles les personnages espèrent trouver un sens à leur destinée ! A travers un texte original, Pauline d’Ollone interroge l’aliénation et les stratégies imperceptibles de contrôle de notre société moderne. De quoi aiguiser notre esprit critique avec une lucidité rieuse !

*Reflets d’un Banquet, première création de Pauline d’Ollone a été nommé pour le Prix de la critique 2016, catégorie Meilleure découverte


Avec
:  Pierange Buondelmonte, Héloïse Jadoul, Sarah Messens et Jérémie Siska.

Texte et mise en scène
:   Pauline d'Ollone

Scénographie
:   Pierange Buondelmonte

Création lumière
: Renaud Ceulemans

Assistanat à la mise en scène
: Anthony Scott

Construction du décor
: Didier Rodot

Production déléguée
: Théâtre la Balsamine

En coproduction avec Les étrangers, L’Ancre–Théâtre Royal et La Coop asbl. Avec les soutiens du Centre des Arts Scéniques, de la Fédération Wallonie-Bruxelles – Aide aux projets théâtraux, de Shelterprod, de taxshelter.be, d’ING et du Tax-shelter du gouvernement fédéral belge.
{: .production }


Tournée : du 14 au 23 novembre à [L'Ancre-Théâtre Royal](https://www.ancre.be/show/2018-2019-o%C3%B9_suis_je_qu'ai_je_Fait)
{: .pedagogique }

Infos pour les professeurs : Grâce au soutien de la Cocof (programme initiation théâtrale), ce spectacle est proposé aux élèves du secondaire de la Région Bruxelles-Capitale au tarif de 2€ la place.
{: .pedagogique }

<div class="galerie" markdown=true>
![ou-suis-je](/images/img-18-19/svg/ou-suis-je-02.svg){: .illustration}
![ou-suis-je](/images/img-18-19/svg/ou-suis-je-03.svg){: .illustration}
![ou-suis-je](/images/img-18-19/svg/ou-suis-je-04.svg){: .illustration}
![ou-suis-je](/images/img-18-19/svg/ou-suis-je-05.svg){: .illustration .demi}
![ou-suis-je](/images/img-18-19/svg/ou-suis-je-06.svg){: .illustration .demi}
</div>

<div class="galerie photos-spectacle" markdown=true>
![ou-suis-je](/images/img-18-19/ousuisje7.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje8.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje9.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje10.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje11.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje12.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje13.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje14.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje15.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/ousuisje16.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe class="video" src=https://player.vimeo.com/video/297098148></iframe>

<iframe class="video" src=https://player.vimeo.com/video/297675863></iframe>
</div>

