Title: Soirée solidaire - au bénéfice de BXLRefugees
Date: 2017/12/02
Time: 19h30
Piece_author: Les fanfoireux
Event_type: Concert-buffet
Reservation_link: https://balsamine.billetterie.it//index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=43
Intro: Nous vous proposons une soirée concert / buffet au profit de BXLRefugees, Plateforme citoyenne de soutien aux réfugiés Bruxelles. 
Color: #588aa0

Les réservations sont complètes ! 

Nous vous proposons une soirée concert / buffet au profit de BXLRefugees, Plateforme citoyenne de soutien aux réfugiés Bruxelles. 

AU PROGRAMME

19h30 : ouverture du bar + buffet déjà dressé, composé de plats et desserts "maison" concoctés par l'équipe Balsa et les artistes de la saison.

21h : Concert du groupe Les Fanfoireux. Ils avaient assuré le 18 juin dernier lors de la marche citoyenne et solidaire « salut à toi ! », et sont de retour, cette fois-ci à la Balsa, pour un concert plus festif que jamais ! 

Les Fanfoireux délivrent une forme rare d'Esperanto Afro-Brasilo-colombien, un mix créolisé de Swing, de Jungle et de Rock. Formé dans les squats bruxellois en 2001, les Fanfoireux sont devenus des incontournables dans le monde de la fête en Belgique, avec à leur actif plus de 800 concerts, propulsés par une puissante énergie tropicale ! Ils sont aujourd'hui électrifiés, sur scène, et sèment plus que jamais le soleil et le chaos festif partout sur leur passage.
{: .production }

<div class="galerie" markdown=true>
![fanfoireux1](/images/fanfoireux1.jpg ){: .image-process-detail}

![fanfoireux2](/images/fanfoireux2.jpg ){: .image-process-detail}
</div>
