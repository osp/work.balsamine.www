Title: 7.C Cœur obèse
Date: 2020/04/03
Slug: xx-time-coeur-obese
End_date: 2020/04/04
Time: 20h30
Key_image_detail_body: img-19-20/coeurob_visuel.jpg
Piece_author: Amandine Laval<br/> Spectacle annulé
Event_type: Performance (+18 ans) <br/> Dans le cadre du XX Time
Status: draft
Color: #ffffff

En ôtant sa culotte, en dézippant sa robe de soirée, en dégrafant sa lingerie sans en avoir l'air mais bien l'envie, en traquant le désir d'inconnus, en faisant jaillir leur excitation, en simulant le coup de foudre, le respect, l'amour véritable, compose-t-on finalement un spectacle comme un autre ? Une strip-teaseuse amateure, seule en scène, se décharge des fictions accumulées.


Texte et interprétation
:   Amandine Laval

Musique
:   Pierre-Alexandre Lampert

Création lumière
:   Alice De Cat

Production 
:   Théâtre la Balsamine
{: .production}


[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)