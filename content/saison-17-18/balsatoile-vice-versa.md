Title: Vice versa
Date: 2017/10/11
Time: 15h00
Category: balsatoiles
Length: 1h30
Age: 5 ans et plus
Piece_author: Pete Docter
Event_type: Balsatoiles
Color: #dfe59e
Key_image_detail_body: 556f270ec9234-1-vice-versa-illustration.jpeg
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=35
Grandir n'est pas de tout repos, et la petite Riley ne fait pas exception à la règle. À cause du travail de son père, elle vient de quitter le Midwest et la vie qu'elle a toujours connue pour emménager avec sa famille à San Francisco. Comme nous tous, Riley est guidée par ses émotions - la Joie, la Peur, la Colère, le Dégoût, la Tristesse. Ces émotions vivent au Quartier Général, le centre de contrôle de l'esprit de Riley, et l'aident et la conseillent dans sa vie quotidienne. Tandis que Riley et ses émotions luttent pour se faire à leur nouvelle existence à San Francisco, le chaos s'empare du Quartier Général. Si la Joie, l'émotion dominante de Riley, tente de rester positive, les différentes émotions entrent en conflit pour définir la meilleure manière de s'en sortir quand on se retrouve brusquement dans une nouvelle ville, une nouvelle école et une nouvelle maison.

Un atelier créatif, animé par un artiste plasticien, est proposé de 14h à 15h.
(inscription obligatoire – nombre de place limité)


Projet réalisé avec le soutien du Fonds Baillet Latour, dans le cadre de l’initiative L’extrascolaire au cœur de l’intégration gérée par la Fondation Roi Baudouin.
{: .production }

[Voir la bande-annonce du film "Vice versa"](http://www.allocine.fr/video/player_gen_cmedia=19550127&cfilm=196960.html)
{: .production }
