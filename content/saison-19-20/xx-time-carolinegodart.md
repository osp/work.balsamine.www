Title: Le corps comme territoire
Subhead: La conférence est complète mais une liste d'attente est ouverte sur place dès 18h30. 
Date: 2020/03/12
Slug: xx-time-carolinegodart
Time: 19h
Piece_author: Rencontre avec Caroline Godart
Event_type: entrée libre sur réservation 
Key_image_detail_body: img-19-20/caroline_godart_site.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=154
Status: draft
Color: #ffffff

Caroline Godart est dramaturge, auteure, enseignante et co-directrice éditoriale de la revue Alternatives théâtrales. De 2005 à 2014, elle étudie et vit aux États-Unis où elle obtient un doctorat en littérature comparée (Rutgers University). Son premier ouvrage, The Dimensions of Difference: Space, Time and Bodies in Women’s Cinema and Continental Philosophy, paraît en 2015 chez Rowman and Littlefield. De retour en Belgique, Caroline devient chargée de cours à l’IHECS et se plonge dans les arts de la scène. Elle élargit son terrain d’expérimentation intellectuelle et esthétique en collaborant fréquemment avec des artistes en tant que dramaturge, ainsi qu'avec des institutions comme a.pass et la Maison du spectacle de la Bellone.  

[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)