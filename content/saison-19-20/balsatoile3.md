Title: Astérix: Le secret de la potion magique  
Date: 2019/12/04
Time: 15h00
Slug: balsatoile3
Category: balsatoiles
Length: 86 minutes
Age: 5 ans et plus
Piece_author: Alexandre Astier & Louis Clichy
Key_image:
Key_image_detail_body: img-19-20/astérix.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=133 
Event_type: Projection gratuite
Color: #6b89ed


À la suite d’une chute lors de la cueillette du gui, le druide Panoramix décide qu’il est temps d’assurer l’avenir du village. Accompagné d’Astérix et Obélix, il entreprend de parcourir le monde gaulois à la recherche d’un jeune druide talentueux à qui transmettre le Secret de la Potion Magique…

Entrée libre mais réservation souhaitée !

Chaque film sera précédé, à 14h, d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=klnu4ps7C8w) 

Avec le soutien du Fonds Baillet Latour, dans le cadre de l'initiative "L’Extrascolaire au cœur de l’intégration" gérée par la Fondation Roi Baudouin.
{: .production }


Prochaines séances : 
- le 29 janvier 
- le 1er avril 
- le 6 mai
- le 3 juin
{: .pedagogique }