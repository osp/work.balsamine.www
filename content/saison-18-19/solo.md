Title: Solo
Date: 2018/11/29
Slug: solo
Time: 20h30
Key_image: 
Key_image_detail_body:img-18-19/solo2.jpg
Piece_author: Akoun theater
Event_type: Théâtre 
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=99
Color: #ffffff
Status: draft


D'après ‘La nuit sacré’ de Tahar Ben Jelloun
{: .pedagogique }

La pièce de théâtre « Solo » est avant tout une histoire et un témoignage sur la vie d’une femme exceptionnelle. C’est Zahra qui nous donne sa propre version des faits. Sa seule erreur ? Être née fille dans une famille qui espérait un garçon. Quand son père, le cadet de six frères et sœurs, la vit, il décida de tester le destin : elle allait vivre comme un homme et s’appeler Ahmed. Cet être asexué est aujourd’hui adulte. Au milieu de la vingt-septième nuit, sacrée, du Ramadan, son père décède. Sa fin approchant, il se repent et décide de libérer son enfant. C’est alors que Zahra commence une quête pour se retrouver avec elle-même, laissant derrière elle les vestiges d’un passé douloureux.
{: .pedagogique }


Mise en scène
:  Mohamed Elhor

Assistants à la mise en scène
:   Hajar El Hamidi, Mohammed Bassalah

Dramaturgie
:  Hajar El Hamidi, Mohamed Elhor

Avec 
:  Hajar El Hamidi, Amal Ben Haddou, Saïd El Harrassi

Scénographie 
:   Houda Zabid

Costumes
:  Badr Soud Hassani

Univers sonore 
: Gustavo Santaollala

Régie
: Mohamed Assila, Salah Khouri

Infographie et documentation
: Mehdi Doukkali

Chargé de communication
: Rachid Elbelgiti

Chargée de production
: Hajar Elhamidi

> Avant la représentation : 
{: .pedagogique }

> 19h : Présentation du nouveau texte de théâtre 'Donia' de Taha Adnan: lecture et discussion - en français entrée libre
{: .pedagogique }

> entrée libre ( réservation souhaitée ) - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=103)
{: .pedagogique }

<div class="galerie" markdown=true>
![solo](/images/img-18-19/solo1.jpg){: .image-process-large}
![solo](/images/img-18-19/solo2.jpg){: .image-process-large}
![solo](/images/img-18-19/solo3.jpg){: .image-process-large}
![solo](/images/img-18-19/solo5.jpg){: .image-process-large}
</div>


