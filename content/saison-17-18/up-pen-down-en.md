Title: Up Pen Down
Date: 2017/10/14
Time: 20h30
Piece_authors: OSP
               Adva Zakaï
Event_type: Coded and commented dance
Subhead: In the frame of la Saison numérique 2017
Key_image: shows/1-Helpers/helper-uppendown.png
Key_image_detail_body: shows/1-Helpers/helper-uppendown.png
Partner_logo: /images/logos/saison-numerique.svg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=33
Color: #fac533
Slug: up-pen-down
Translation: true
Lang: en

Within [la saison numérique](http://saisonnumerique.be/) the typographers of [Open Source Publishing ](http://osp.kitchen/) and the choreographer Adva Zakaï return with a new presentation of their research between body and writing. Dance and typography that osscillates between moments in types of conferences, movement prototypes, and demonstrations of software working and plotter movements.

![](http://ospublish.constantvzw.org/images/var/albums/Up-pen-down--01/2015-10-25%2016_44_47.jpg?m=1492279209){: .half}

This event is inspired by an ongoing research focused on that which is between digital type design and bodies. We will share our findings and open questions with the public - letters and movements, dance notation and programing, digital codes and gestural language, plotters and body parts will duet with each other and hopefully confuse, even if just a little, the distinction between choreographic and digital practices. We will perform a publication, or rather print a performance. We will welcome you on stage, where we will move, type, talk, draw, read and write, until eventually our actions will find their way into a booklet - a present for you, with which we will greet you goodbye.

![](https://cloud.osp.kitchen/s/fC0mEtLVrKrlEEp/download){: .third}
![](https://cloud.osp.kitchen/s/Y5x60v93o96fzfz/download){: .third}

See also the Up Pen Down research page on the OSP website: [http://osp.kitchen/live/up-pen-down/](http://osp.kitchen/live/up-pen-down/)

Concept, research, performance
:    Gijs De Heij, Pierre Huyghebaert, Ludi Loiseau, Adva Zakaï

External eye
:    Sarah van Lamsweerde, Louise Baduel

With the support from Fédération Wallonie-Bruxelles - Arts numériques, Vlaamse Gemeenschapscommissie and le Théâtre la Balsamine.
{: .production}
