Title: A Notional History
Dates: 2020/05/12 → 2020/05/15 20h30
       2020/05/16 16h00
Slug: kunsten
Event_type: Théâtre - Kuala Lumpur - Première
Category: annulé
Color: #6b89ed
Partner_logo: /images/logos/kunsten.png
Piece_author: Mark Teh / Five Arts Centre <br/> Spectacle annulé


Depuis l’indépendance de la Malaisie en 1957, le parti Barisan National a remporté toutes les élections, jusqu’en 2018, quand un nouveau gouvernement est élu pour la première fois en 61 ans. À ce moment charnière pour l’avenir de la Malaisie, l’artiste Mark Teh se penche sur le passé de son pays. Avec une équipe hybride composée de journalistes, d’artistes et de militantes, il se concentre sur la révolution inachevée du parti communiste malaisien en exil. À travers les images d’un documentaire inachevé qui, quarante ans plus tard, recherche les anciennes combattantes communistes dans la jungle, nous sommes transportées au plus près de ce passé tragique. Cela contraste fortement avec la façon dont les gouvernements précédents ont invisibilisé ce récit dans les livres d’histoire. Avec la nomination du nouveau gouvernement, le débat sur l’histoire a éclaté au grand jour et cet élan est mis à profit pour rédiger un nouveau manuel sur l’histoire du XXe siècle. Comment tirer les leçons d’un passé truffé d’angles morts, de tragédies oubliées, d’épisodes censurés ? Comment écrire ensemble sur l’avenir multiple de la nouvelle Malaisie ? 

PRESENTATION 
: Kunstenfestivaldesarts - Théâtre la Balsamine

DIRECTION 
: Mark Teh

PRODUCTION DESIGN
: Wong Tay Sy

LIGHT DESIGNER 
: Syamsul Azhar

PERFORMERS 
: Fahmi Reza, Faiq Syazwan Kuhiri, Rahmah Pauzi

PRODUCTION 
: June Tan

STAGE MANAGER
: Veeky Tan

EXECUTIVE PRODUCTION
: Five Arts Centre (Malaysia)

COPRODUCTION 
: TPAM-Performing Arts Meeting Yokohama 

Possibilité de soutenir le festival en achetant des tickets ou pass fantôme. Plus d'infos sur le [site du festival](https://www.kfda.be/fr)

