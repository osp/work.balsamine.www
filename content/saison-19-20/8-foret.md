Title: 8. Forêts paisibles
title_detail: vaudeville mythologique
Date: 2020/04/21
End_date: 2020/05/01
Time: 20h30
Slug: forets
Category: annulé
Key_image: img-19-20/programme/chapitre-8/Botticelli_Prado_103-warm.jpg
Key_image_detail_body: img-19-20/programme/chapitre-8/Botticelli_Prado_103-warm.jpg
Key_image_detail_body_caption:  L'Histoire de Nastagio degli Onesti, Sandro Botticelli
Piece_author: Martine Wijckaert <br/> Spectacle annulé
Event_type: Création théâtre
Color: #7fa22a


Dans une construction précaire en bois brut, se joue un vaudeville mythologique : voici qu’un couple de satyres s’est vu affligé de la naissance d’une enfant anormale, les velus ongulés ont engendré du glabre en baskets ...
*Forêts paisibles* porte à la scène un trio familial, affligeant de médiocrité, télescopé par le jeu du fantasme, dans un monde archaïque où les interdits du meurtre et de l’inceste ne sont pas encore tout à fait clairement démêlés.
Le spectacle est une pantalonnade traitant de la dégénérescence et s’emparant pour la raconter des mécanismes du vaudeville où l’absurde le dispute avec la trivialité la plus plate. Ira-t-on jusqu’à la transgression absolue : l’assassinat perpétré par les géniteurs sur leur progéniture ?

Écrit pour Alexandre Trocki, Véronique Dumont et Héloïse Jadoul, la chose aurait pu être un petit archétype familial de base, papa, maman et la post-ado pré-adulte. C’en est presque banal. Sauf que l’auteur qui se cache derrière ce texte écrit au rasoir pour des acteurs hors-normes, n’est autre que Martine Wijckaert. Et donc, c’est de la Bombe théâtrale assurée ! D’avance, on jubile ...
{: .push-down}

Avec
:   Véronique Dumont, Héloïse Jadoul, Alexandre Trocki

Écriture et mise en scène&nbsp;
:   Martine Wijckaert

Assistante à la mise en scène&nbsp;
:   Astrid Howard

Scénographie
:   Valérie Jung

Lumières
:   Stéphanie Daniel

Costumes et accessoires
:   Laurence Villerot

Collaboration et réalisation des costumes
: Virginie Breger

Création son
:   Thomas Turine

Direction technique et régie lumières
:   Mathieu Bastyns

Régie plateau
:   Olivier Vincent

Construction
:   Fred Op de Beeck

Regard extérieur
:   Sabine Durand

Production
:   Théâtre la Balsamine

Coproductions
:   La Coop asbl et Shelter Prod

Soutiens
:   taxshelter.be, ING et Tax-shelter du gouvernement fédéral belge. Un merci tout particulier aux Entreprises générales Dherte S.A.




