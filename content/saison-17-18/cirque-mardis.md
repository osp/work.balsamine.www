Title: Cirque
Date: 2017/11/21
Time: 20h30
EndTime: 23h00
Category: mardis contemporains
Key_image: shows/1-Helpers/helper-wind-grid.png
Key_image_detail_body: shows/1-Helpers/helper-wind-grid.png
Color: #bd7c7d
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=30&cntnt01returnid=44

Les mardis contemporains proposent aux oreilles curieuses de découvrir des facettes de la musique d’aujourd’hui, dans un programme pluriel qui dialogue avec l’histoire, la philosophie, la sociologie, mais aussi le cirque, le cinéma, la science ou l’architecture… Le temps de 3 concerts-rencontres, les mardis contemporains prennent leurs quartiers à la Balsamine.

## Cirque

### Le 21 novembre à 20h30

A mi-chemin entre l’enfance et les étoiles, le Cirque reste une fontaine de rêves pour tous les âges. Il a traversé les époques depuis l’antiquité, puisant dans les évolutions technologiques et sociétales des souffles nouveaux qui jamais n’attirent la poussière de la désuétude. Pour nous parler de son histoire et de ses codes, nous recevons Julien Rosemberg, auteur de l’ouvrage « Les arts du cirque ».
La pianiste Gabi Sultana et le violoncelliste Guy Danel, revisiteront un certain répertoire contemporain en rencontre avec le Cirque, qui plongera dans l’univers de Ligeti ou Stravinsky, qui composa pour un numéro d’éléphant du cirque et la création de Stefan Hejdrowski pour un numéro inédit de main à main d’Elsa Bouchez et Philippe Droz.
{: .production }

Invités
:     Julien Rosemberg, Directeur Général adjoint — Pôle National Cirque et Arts de la rue

Œuvres
:    Sonate n°2 pour violoncelle de Gyorgy Ligeti (1953)
:    Circus Polka d’Igor Stravinsky (1942)
:    March for Piano: The Circus Band de Charles Ives (1898-1899)
:    Création de Stefan Hejdrowski (commande Ars Musica 2017)

Interprétation
:    Guy Danel, violoncelle
:    Gabi Sultana, piano
:    Elsa Bouchez et Philippe Droz, artistes de cirque
