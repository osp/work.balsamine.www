Title: Le Voyage
Date: 2018/04/20
End_date: 2018/04/21
Time: 20h
Piece_author: Brice Cannavo
Event_type: Pièce radiophonique
Key_image_detail_body: ESSAIS.jpg
Color: #cc9c8c
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=63
Subhead: Prix d'entrée libre. L'argent récolté est reversé à la Plateforme citoyenne de soutien aux réfugiés.


Le Voyage nous mène, à travers la fiction du « Voyage au centre de la Terre » de Jules Verne, à la rencontre d’une femme, bel et bien réelle, ayant fui son pays par instinct de survie. Sa voix nous parle, dans le noir, pendant que les protagonistes de la fiction descendent… Jusqu’où ? 
Le voyage, acte de bravoure lorsqu’on en lit les péripéties des plus audacieux dans les textes épiques et romanesques, que devient-il, confronté à la réalité de celles et ceux qui le vivent malgré eux ?

Avec
:  Désiré, Sabine Durand, Blanche Monfé, Aurore Fattier, Balthazar Monfé, Peter Palasthy, Nicolas Sanchez, Tchawa, Delphine Benhamou, Emilie Maquest. 

Réalisation
: Brice Cannavo  

Photo
: Johan Legraie 

Une production l’Autre a.s.b.l.
Avec  le soutien des fonds d’aide à la création radiophonique de la fédération Wallonie-Bruxelles et  l’aide de la Promotion pour la citoyenneté et l'interculturalité de la Fédération Wallonie-Bruxelles.
{: .production }







