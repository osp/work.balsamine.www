Title: Notre arme sera l'allégorie
Date: 2019/06/15 20h00
Dates: 2019/06/15 16h00
       2019/06/15 18h00
       2019/06/15 20h00
Slug: notre_arme
Key_image_detail_body:img-18-19/Annie_allégorie.jpg
Piece_author: t.r.a.n.s.i.t.s.c.a.p.e / Pierre Larauza + Emmanuelle Vincent
Event_type: Performance - sculpture/maçonnerie - atelier - entraînement
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=114 
Subhead: Dans le cadre du PIF4
Status: draft
Color: #009933

" **On a tous un mur dans notre vie, qu’on n'a pas su franchir, qu’on ne sait pas franchir, qu’on ne pourra pas franchir.**"
 
Sur le plateau transformé en terrain de jeu, des sauteurs en hauteur et des personnes âgées cohabitent en espérant franchir un obstacle physique ou dépasser leur essoufflement. Un mur est construit en temps réel, comme allégorie de toutes nos frontières.
Corps sportifs mesurés et corps usés cohabitent le temps d’un entraînement.
Et si on s’éduquait à sauter ? A dire “je peux” ? A tenter l’impossible ? Apprendre à sauter serait-il la clef pour déjouer les frontières ? 

Avec 
:    Gaston Barbe, Antoine Bruart, Annie Burgheim, Jacky Deholn, Manuela Fiori, Gene Francart, Pierre Larauza, Emmanuelle Vincent

Soutien 
:    Théâtre la Balsamine

Remerciements à la maison de repos Les Heures Douces, au Royal Cercle Athlétique de
Schaerbeek, au Centre Adeps de Prêt de Matériel Sportif, à la faculté des Sciences de la
motricité de l'UCL et à toute l’équipe de t.r.a.n.s.i.t.s.c.a.p.e.
{: .production }

Au programme&thinsp;:
{: .programme }

16h: Initiation à la Maçonnerie (1h) : tout public, tout niveau, gratuit
{: .programme }

18h: Initiation au Saut en hauteur  (1h) : tout public, tout niveau, gratuit
{: .programme }

20h: Performance - sculpture/maçonnerie - atelier - entraînement
{: .programme }