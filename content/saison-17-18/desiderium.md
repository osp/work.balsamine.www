Title: De-siderium
Date: 2018/06/16
Time: 21h00
Piece_author: Chloé Winkel
Event_type: Théâtre
Key_image_detail_body: repastro.jpg
Status: draft
Reservation_link: http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=72
Subhead: Les réservations pour les soirées combinées se font uniquement par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68

 **De-Siderium** prend racine dans une histoire personnelle, très partagée : la perte d’une grand-mère, comme première rencontre avec la mort, opérant une forme de rituel de passage à l’âge adulte. Une famille, ce tout incommensurable, se voit amputée d’un élément fédérateur. 
Entremêlant passé, présent et futur, réels et fantasmés, faisant dialoguer les êtres et les choses, cette échappée théâtrale cherche littéralement le moyen de sortir d’un système aveugle, d’une histoire aliénante. Sur fond de musique balkanique, la force des corps et des images parle là où les mots échouent à trouver l’apaisement.

Conception, écriture et mise en scène
: Chloé Winkel

Avec 
: Delphine De Baere, Thomas Dubot, Boris Prager, Ghislain Winkel, Chloé Winkel

Créatrice lumière 
: Octavie Piéron

Costumes 
: Pauline Miguet
