Title: XX Time
Date: 2019/02/12
Slug: xx-time
Translation: true
Lang: fr
End_date: 2019/03/22
Time: 20h30
Key_image: img-18-19/svg/xx-time-01.svg
Key_image_detail_body: img-18-19/svg/xx-time-01.svg
Piece_author: Etna / De la poésie, du sport, etc. / Looking for the putes mecs / Coeur obèse /i-clit
Event_type: Danse, théâtre, performance               
Reservation_link:
Subhead:
Partner_logo:
Color: #fe5000

**XX Time**, temps privilégié dévolu à quelques artistes femme interrogeant le féminin, la sexualité, les rapports dominants dominés. Corps souillé, corps poubelle, corps outil. Ce que les femmes font de leurs corps , au coeur de leur sujet.

Au programme&thinsp;:
{: .programme }

### [Etna](/saison-18-19/xx-time-etna.html)

de Thi-Mai Nguyen - reprise danse

12.02
{: .dates }

### [Droit à un toit ?](/saison-18-19/xx-time-droitauntoit.html)

DoucheFLUX - Débat 

12.02 - 21h30
{: .dates }


### [De la poésie, du sport, etc.](/saison-18-19/xx-time-poesie.html)

de Fanny Brouyaux et Sophie Guisset

19.02 → 21.02
{: .dates }

### [Le corps de la femme peut-il s'émanciper? ](/saison-18-19/xx-time-corpsdefemme.html)

Nathalie Grandjean

20.02 - de 19h à 20h
{: .dates }


### [Erratum](/saison-18-19/xx-time-erratum.html)

de Petite Poissone

19.02 → 22.03
{: .dates }

### [Looking for the putes mecs](/saison-18-19/xx-time-looking.html)

de Diane Fourdrignier et Anne Thuot

1.03 → 2.03
{: .dates }

### [Cœur obèse](/saison-18-19/xx-time-coeur.html)

d'Amandine Laval

14.03 → 16.03
{: .dates }

### [This is not mine](/saison-18-19/xx-time-thisisnotmine.html)

Workshop de twerk pour débutant.e.s
avec Gaspard Rozenwajn

18.03 - de 19h à 21h
{: .dates }

### [i-clit](/saison-18-19/xx-time-iclit.html)

de Mercedes Dassy

20.03 → 22.03
{: .dates }

### [Katcross](/saison-18-19/xx-time-katcross.html)

Concert - duo electro pop

22.03 à 22h
{: .dates }





<div class="galerie" markdown=true>
![xx](/images/img-18-19/svg/poesie-01.svg)
![xx](/images/img-18-19/svg/looking-01.svg)
![xx](/images/img-18-19/svg/coeur-00.svg)
![xx](/images/img-18-19/svg/iclit-01.svg)
</div>
</div>

<div class="galerie photos-spectacle" markdown=true>
