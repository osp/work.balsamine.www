Title:  Le Salon
Date: 2018/06/15
Time: 19h00
Piece_author: Marine Prunier
Event_type: Installation - Performance
Key_image_detail_body:  lesalon-visuel4.jpg
Status: draft
Subhead: Entrée libre, mais réservation indispensable, soit par mail à l'adresse : reservation@balsamine.be ou par téléphone au 02 735 64 68. 

Le salon funéraire sert à rendre hommage, à mettre en lumière, une toute dernière fois, le corps du défunt. À cercueil ouvert ou fermé, selon la demande familiale ou l’état du corps. L’espace du salon funéraire est un espace de représentation. Cette mise en scène, à quelques fleurs et rideaux prêts, se répète, répète, répète. À la manière d’un espace de méditation, offrez-vous aujourd’hui la possibilité de faire l’expérience du cercueil en tant qu’objet. 

Création
: Marine Prunier

Avec 
: Marine Fontaine et Marine Prunier

Lumière et aide précieuse
: Thomas Buisson

Soutien
: Théâtre la Balsamine 
