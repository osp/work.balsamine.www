Title: Stage multimédia
Date: 2018/08/20
Slug: Stage 
End_date: 2018/08/24
Time: 13h30
Event_type: 12-18 ans
Reservation_link: 
Color:#ffffff

Le stage est animé par Noemi Tiberghien ( comédienne )  et Brice Agnès ( régisseur  son) .
Les jeunes seront invités à expérimenter différents médias : photo, documentaire radio, écriture, jeu face caméra, montage son, chant, mouvement… dans l’objectif de réaliser une sorte de clip personnel, sur un morceau de musique choisi.

A prévoir :
    Venir en tenue souple.
    Prendre son pique-nique, sa collation et des boissons en suffisance chaque jour.

[cliquez ici pour vous inscrire](https://www.schaerbeek.be/fr/stage-clip-tournage-musique-et-montage).
Tarif  :  Schaerbeekois : 40€ / Non Schaerbeekois : 100€
