Title: Serial lovers
Date: 2020/04/03
Slug: xx-time-serial-lovers
Time: 21h30
Key_image_detail_body: img-19-20/serial_lovers_-_crédit_Fabrice_Rhodes___002_.jpg
Piece_author: Bibi Lona ( aka Barbara Mavro Thalassitis) <br/> Concert annulé
Event_type: Concert <br/> Dans le cadre du XX Time
Status: draft
Color: #ffffff

Serial Lovers parcourt l’épopée d’une Serial Killeuse ou plutôt Loveuse… Une sorte de Calamity Janine des temps modernes qui décide de se rendre justice et de se débarrasser définitivement, en les éliminant un à un et de sang-froid, tous les salopards qui ont peuplé sa vie… Une revanche pour le moins drôle et jouissive tout en crime. 

Ce nouvel album aux sonorités Jazz ou blues avec des ambiances folk et des textes majoritairement en français, est construit tel un road movie. 

Chant, écriture et composition 
:   Bibi Lona aka Barbara Mavro Thalassitis 

Guitare, violon, chant et composition 
:   Medhi Mahassine 

Basse 
:   Edis Hennebick 

[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)