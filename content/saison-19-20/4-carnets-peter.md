Title: 4. Les carnets de Peter
Subhead: Les représentations des 3 et 5 janvier sont complètes 
Date: 2020/01/3
End_date: 2020/01/5
Time: 15h
Slug: carnets-peter
Key_image: img-19-20/programme/chapitre-4/GettyImages-503434214.jpg
Key_image_detail_body: img-19-20/programme/chapitre-4/carnetsdepeter.jpg
Piece_author: Théâtre du Tilleul
Event_type: Théâtre d’ombre et de musique
Age: pour tous à partir de 7 ans
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=126
Color: #7fa22a

Peter, 87 ans, tente de se souvenir de son enfance : l'Allemagne, Munich 1936, les défilés militaires, les drapeaux, l'émigration en Amérique...Il se souvient , qu'un jour, seul dans la grande bibliothèque de son père, il décide d'inventer les étranges aventures d'un petit garçon nommé Donald, un petit garçon rêveur qui lui ressemble énormément sauf que...
{: .push-down}

Avec
:   Carine Ermans, Carlo Ferrante, Sylvain Geoffray, Alain Gilbert

Conception et texte
:   Carine Ermans et Sylvain Geoffray

Mise en scène
:   Sabine Durand

Conseil dramaturgique
:   Louis-Dominique Lavigne

Musique
:   Alain Gilbert

Lumière
:   Mark Elst

Régie
:   Thomas Lescart

Scénographie
:   Pierre-François Limbosh et Alexandre Obolensky

Peintures
:   Alexandre et Eugénie Obolensky aidés de Malgorzota Dzierzawska et Tancrède de Ghellinck

Costumes
:   Silvia Hasenclever

Travail du mouvement
:   Isabelle Lamouline <br/>

Images animées
:   Patrick Theunen et Graphoui

Accessoires
:   Amalgames

Production
:   Mark Elst

Coproduction
:   Théâtre la Balsamine

Soutiens  
:   Théâtre La montagne magique, le centre dramatique Ekla, Maison des Cultures et de la Cohésion Sociale de Molenbeek-Saint-Jean


Le Théâtre du Tilleul est en résidence artistique et administrative au Théâtre la Balsamine.
{.production}

###  ATELIERS autour du STOEJGNPF, le mystérieux animal de compagnie de Donald
A l'issue des représentations, le Théâtre du Tilleul vous propose des ateliers : 

Atelier Impression d’affiches : On a perdu le Stoejgnpf 
animé par Marjorie Vander Meiren en alternance avec Alice Waterkeyn 

Atelier mini-livres dessin et collage : La journée du Stoejgnpf 
animé par Mélanie Rutten 

Atelier Couture, Broderie et boutons : Le Stoejgnpf et son nouveau collier
animé par Stéphanie Vander Meiren 

Atelier cyanotypes : Stoejgnpf et ficelles
animé par Nicolas Bovesse et Angelika Szydlowska.


<div class="galerie photos-spectacle" markdown=true>

![carnets](/images/img-18-19/carnet1.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet2.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet3.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet4.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet5.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet6.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet7.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnet](/images/img-18-19/carnet8.jpg){: .image-process-large}
:   © Théâtre du Tilleul

![carnets](/images/img-19-20/tilleul0.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul1.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul2.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul3.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul4.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul5.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul7.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul10.jpg){: .image-process-large}
:   © Danielle Pierre

![carnets](/images/img-19-20/tilleul13.jpg){: .image-process-large}
:   © Danielle Pierre


</div>
