Title: Rent la Balsamine
Date: 2017/04
Slug: location
Lang: en
Translation: true
Status: hidden

[TOC]

Situated in a stone's throw from the European institutions, La Balsamine can offer you several of its spaces, comprising a technical and professional infrastructure as well as a convivial bar.

Whether you are organizing a seminar, a workshop, a training day, a birthday or a wedding, contact us with your query and we will advise you. We will put at your disposal the most suitable space, equipment of the highest quality and a professional technical team.

* * *

[Download all information](doc/Renting_La_Balsamine.pdf)

<!--
 (le dossier en anglais est en pièce jointe – dossier provisoire en
attendant des photos de nos espaces)
-->

## Our spaces

### The Foyer
Heart of the theatre, where everything happens and through which everyone passes, the Foyer has a bar and a kitchen. It gives out onto a public park on the same level.

* Surface area: 200m²
* Capacity: 120 people seated / 200 people standing
<!--photo-->

### The Auditorium
From a former military auditorium built in a semicircle, the architects have designed a tailor-made costume for theatre-goers. The Auditorium includes a proscenium arch so wide that it almost pitches the audience onto the stage!

* Surface area: stage area 200m² (14m × 9m)
* Capacity: tiers with 170 people seated / stage area: 200 people standing

<!--photo--><!--photo-->

### The Studio

* Surface area: 130 m² without tiers / 90 m² with tiers
* Capacity: tiers with 70 people seated / 100 people standing without tiers

<!--photo-->

## Contact
We are at your disposal to show you around the theatre and help you organize your event. If you wish to receive a cost estimate, do not hesitate to contact us!

Contact
:   Morgan Brunéa
:   [02 737 70 17](tel:+32 737 70 17)
:  [morgan.brunea@balsamine.be](mailto:morgan.brunea@balsamine.be)  
