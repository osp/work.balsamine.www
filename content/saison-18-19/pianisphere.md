Title: Pianisphère
Date: 2018/11/12
Slug: pianisphère
Time: 20h30
Key_image:
Key_image_detail_body:
Piece_author:
Category: Ars Musica
Event_type: Concert pour piano-toy, piano et électronique<br/>En partenariat avec Ars Musica
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=89
Subhead:
Partner_logo: /images/logos/ars-musica.svg
Color: #ffffff

Comme une sorte de planisphère pianistique, **Pianisphère** plonge l'auditeur dans un large répertoire contemporain, aux facettes multiples. Ainsi dans cette immersion sonore, les musiciens explorent un langage musical où les syntaxes et les temps se chevauchent. Du duo iconoclaste formé de Xenia Pestova et Pascal Meyer, qui se joue des pianos-jouets aux  étonnantes expérimentations du duo mythique David Shea / Jean-Philippe Collard-Neven, les pianistes cartographient de nouveaux territoires, comme un planisphère en suspension, comme un univers timbrique en ébullition.

### Duo Xenia Pestova / Pascal Meyer

Ce duo désarçonnant, formé de Xenia Pestova et Pascal Meyer, cultive des jardins sonores qui explorent la contemporanéité de la création musicale sous des facettes inattendues.  Depuis 2003, le duo affine une quête qui se nourrit d’une littérature musicale contemporaine récente, telle que le célèbre Mantra de Karlheinz Stockhausen pour deux pianos et électronique live et en suscite des nouvelles pièces auprès de jeunes compositeurs. Ardents défenseurs de l’oeuvre de Cage, ils ont fait du piano-jouet, un instrument à part entière, triturant ses timbres, anamorphosant ses articulations pour échafauder un langage singulier : le leur.

### Duo David Shea / Jean-Philippe Collard-Neven

De nombreuses années après leurs premières collaborations, qui avaient donné alors naissance à des opus discographiques remarquables, David Shea et Jean-Philippe Collard-Neven se retrouvent sur scène, avec un répertoire nouveau. David Shea, figure emblématique de la scène new-yorkaise des années 90, maître du sampler qu’il utilise comme outil de composition,  a collaboré avec des artistes tels que Anthony Coleman, John Zorn, Zeena Parkins ou Shelly Hirsch. Pour Jean-Philippe Collard-Neven, il compose une série de pièces pour piano en rencontre avec l’échantillonneur, développant un dialogue subtil jouant sur le miroir, sur le reflet biaisé, sur l’anamorphose du jeu pianistique de l’instrumentiste qui devient par cette pirouette extatique un créateur à part entière.


<div class="galerie photos-spectacle" markdown=true>
!![pianisphere](/images/img-18-19/meyerpestova.jpg){: .image-process-large}
:   © Sanja Harris

![pianisphere](/images/img-18-19/CollardNevenJeanPhilippe©MarionBoisart.jpg){: .image-process-large}
:   © Marion Boisart

![pianisphere](/images/img-18-19/OdaYucClaudiaHansenwwwclaudiahansen.com_WEB-1-1200x800.jpg){: .image-process-large}
:   © Claudia Hansen

![pianisphere](/images/img-18-19/MedeksaiteEgidija©MartynasAleksa©MICL_WEB-1200x800.jpg){: .image-process-large}
:   ©   Martynas Aleksa

![pianisphere](/images/img-18-19/Pianisphère-1200x800.jpg){: .image-process-large}
:   -


