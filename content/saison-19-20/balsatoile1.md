Title: Iqbal, l'enfant qui n'avait pas peur 
Date: 2019/09/25
Time: 15h00
Slug: balsatoile1
Category: balsatoiles
Length: 80 minutes
Age: 6 ans et plus
Piece_author: Michel Fuzellier & Babak Payami
Key_image:
Key_image_detail_body: img-19-20/Iqbal__l_enfant_qui_n_avait_pas_peur.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=131
Event_type: Projection gratuite
Color: #6b89ed


Iqbal est un petit garçon espiègle et joyeux qui passe son temps entre les jeux avec ses copains, sa petite chèvre adorable et ses superbes dessins. Un jour, tout va changer… Son frère tombe gravement malade et il lui faut des médicaments coûteux, trop coûteux. Croyant bien faire, Iqbal attend la nuit pour s’éclipser vers la ville. Pour aider sa mère et soigner son frère, il n’a pas d’autres solutions que de vendre sa chèvre, le coeur serré... Mais, rien ne se passe comme prévu ! 

Entrée libre mais réservation souhaitée !

Chaque film sera précédé, à 14h, d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=nJP0zup6UuA) 


Avec le soutien du Fonds Baillet Latour, dans le cadre de l'initiative "L’Extrascolaire au cœur de l’intégration" gérée par la Fondation Roi Baudouin.
{: .production }

Prochaines séances : 
- le 13 novembre
- le 4 décembre 
- le 29 janvier 
- le 1er avril 
- le 6 mai
- le 3 juin
{: .pedagogique }