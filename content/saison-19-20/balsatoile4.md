Title: Dilili à Paris   
Subhead: Séance complète 
Date: 2020/01/29
Time: 15h00
Slug: balsatoile4
Category: balsatoiles
Length: 95 minutes
Age: 6 ans et plus
Piece_author: Michel Ocelot
Key_image:
Key_image_detail_body: img-19-20/dilili-dilili.jpg
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=134
Event_type: Projection gratuite
Color: #6b89ed


Dans le Paris de la Belle Époque, en compagnie d’un jeune livreur en triporteur, la petite kanake Dilili mène une enquête sur des enlèvements mystérieux de fillettes. Elle rencontre des hommes et des femmes extraordinaires, qui lui donnent des indices. Elle découvre sous terre des méchants très particuliers, les Mâles-Maîtres. Les deux amis lutteront avec entrain pour une vie active dans la lumière et le vivre-ensemble…

Entrée libre mais réservation souhaitée !

Chaque film sera précédé, à 14h, d’un atelier créatif en lien avec les thématiques abordées par l’œuvre.
Réservation indispensable.

[Voir la bande-annonce du film](https://www.youtube.com/watch?v=1NSMtkfw29k) 

Avec le soutien du Fonds Baillet Latour, dans le cadre de l'initiative "L’Extrascolaire au cœur de l’intégration" gérée par la Fondation Roi Baudouin.
{: .production }


Prochaines séances : 
- le 1er avril 
- le 6 mai
- le 3 juin
{: .pedagogique }