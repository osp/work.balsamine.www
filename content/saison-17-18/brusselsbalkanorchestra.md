Title: Brussels Balkan Orchestra
Date: 2018/06/16
Time: 22h00
Event_type: concert
Key_image_detail_body: musicos in the shadow.JPG
Status: draft
Subhead: Entrée libre
Collectif de musiciens professionnels et amateurs né des ateliers de musique des Balkans donnés à Bruxelles par Nicolas Hauzeur, le groupe reprend essentiellement des perles du répertoire populaire glanées lors de plusieurs voyages musicaux en Roumanie, Bulgarie et Grèce, pour en restituer une version orchestrale pleine de délicatesse et de sensualité ; dans un esprit d'ouverture vers les différentes cultures.                                                           