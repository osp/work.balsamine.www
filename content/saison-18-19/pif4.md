Title: Le PIF 4 - Le Pauvre et Insurgé Festival
Date: 2019/06/11
Slug: pif4
End_date: 2019/06/15
Time: 20h30
Event_type: Installations- Performances - Théâtre - Musique
Reservation_link: http://balsamine.billetterie.it 
Key_image: img-18-19/svg/pif-00.svg
Key_image_detail_body: img-18-19/svg/pif-01.svg
Subhead:
Color: #009933


Un festival ouvert à toutes les révoltes.

Un temps de décloisonnement artistique partagé en toute convivialité avec un public, à l’image du festival, diversifié et curieux.
Une quatrième édition insurgée. Si depuis plusieurs mois, les citoyens investissent massivement et régulièrement les rues et les espaces publics, qu’en est-il des artistes ? Comment se réapproprier ses droits élémentaires, réinventer, voire désobéir ? Le Pauvre et Insurgé Festival se propose d’être leur tribune, un espace d’insurrection. 
{: .pedagogique }

 

Au programme&thinsp;:
{: .programme }


**Le 11 juin** :

> 20h30 : [néKid#2](/saison-18-19/ne-kid.html) – 
de Tom Malmendier et Sab Nicole - Performance - Solo de batterie


> 21h : [L'art de la joie](/saison-18-19/art-joie.html) 
de JOY - Concert slam

**Le 12 juin** :

> 20h : [Aveugles, ou comment se donner du courage pour agir ensemble ?](/saison-18-19/aveugles.html) 

> 21h30 : [Antigone, une autre histoire de l’Europe](/saison-18-19/antigone.html)
de Vincent Collet - Théâtre de Poche / Le joli collectif – Diptyque théâtral

**Le 13 juin** :

> dès 19h: [Pourvu que quelque chose nous arrive](/saison-18-19/pourvu.html)
de Claire Farah & Eve Giordani - Installation immersive

> 20h30 : [Volcans, we don't know when it will happen](/saison-18-19/volcans.html)
de Thymios Fountas & Aurélien Leforestier - Performance / Installation

**Le 14 juin** :

>  20h30 : [Conférence](/saison-18-19/conférence_désorceler.html)

>  22h : [Open Outcry - rituel de désenvoûtement de la finance](/saison-18-19/open_outcry.html)
de la Compagnie Loops / Désorceler la finance

**Le 15 juin** :

> dès 16h : [Notre arme sera l'allégorie](/saison-18-19/notre_arme.html)
de t.r.a.n.s.i.t.s.c.a.p.e  Emmanuelle Vincent + Pierre Larauza - Performance – sculpture/maçonnerie – atelier – entraînement

> 22h : [DJ Captain Connasse](/saison-18-19/captain_connasse.html)

