Title: Mettre à jour <br> balsamine.be
Slug: howto
Date: 2017/04
status: hidden

## Comment éditer le contenu de balsamine.be:

Le site se met à jour via des fichiers sources écrits en Markdown qui sont partagés via git et gitlab OSP.

## Login à gitlab.constantvzw.org

https://gitlab.constantvzw.org/users/sign_in?redirect_to_referer=yes

## Aller dans le répertoire content

![content](/images/how_to/content.png)

![sections](/images/how_to/sections.png)

- le répertoire *images* sert à ajouter des images
- le répertoire *pages* contient les pages principales du site (billeterie, contact...)
- le répertoire *saison-XX-XX* contiens les pages pour la timeline

## Pour ajouter des images

Aller dans le répertoire image et cliquer sur *Upload File*

![new-img](/images/how_to/new-img.png)

## Pour ajouter des événements à la timeline

Aller dans le répertoire saison-XX-XX et cliquer sur *New File*

![new-txt](/images/how_to/new-txt.png)

## Conventions Markdown

Ces conventions servent à mettre en forme le texte

### Entête d'événement

```
Title: <titre de l'événement>
Slug: <permet de changer l'URL de la page>
Date: <date du début de l'événenement>
End_date: <date de fin de l'événement>
Time: <heure de l'événement>
Key_image: <nom-image.png>
Piece_author: <nom de l'auteur>
Event_type: <type d'événement>
Reservation_link: <lien vers la réservation>
Intro: <introduction>
Color: <valeur de la couleur: #9C5C62>
* Language: <en / fr>
* Translation: True
* slug: <le-titre-de-l-event>
```

* Clés marquées d'une * à utiliser dans le [cas d'une page traduite](#traduire-une-page).

Attention! l'utlisation des cles de dates peut devenir un peu complexe: quel que soit le type d'evenement ou sa frequence, chaque evenement doit imperativement avoir une  cle 'Date:'. Si l'evenement dure plusieurs jours d'afiles, nous signifions ceci en marquant une 'End_date:' resultant en une periode sur le site marquee '17.11 → 19.11' par exemple. Si un evenement couvre une periode irreguliere, une liste de dates peut etre notee a la suite de la cle 'Dates:' au pluriel comme l'exemple ci-dessous

```
Title: Bacteria mundi
Date: 2018/03/15
Dates: 2018/03/15 14h00
       2018/03/15 20h30
       2018/03/16 14h00
       2018/03/16 20h30
       2018/03/17 20h30
```




### Styles de texte


```
*italique*   **gras**
```
*italique*   **gras**


```
Titre
:   Texte tabulé

Direction générale et artistique
:   Monica Gomes
```
Titre
:   Texte tabulé

Direction générale et artistique
:   Monica Gomes


```
## Grand titre
### Petit titre
```
## Grand titre
### Petit titre


```
1. Item one
   1. Sub item one
   1. Sub item two
1. Item two
1. Item three
```
1. Item one
   1. Sub item one
   1. Sub item two
1. Item two
1. Item three


### Inserer des images

```
![texte alternatif](/images/nom-de-l'image.png)
```
![logo-balsa](/images/logo-balsa-16-17-large.svg)

### Inserer des liens

```
[texte à afficher](lien)
[Open Source Publishing](http://osp.kitchen)
[Fanny Arvieu](mailto:fanny.arvieu@balsamine.be)
```
[texte à afficher](lien)
[Open Source Publishing](http://osp.kitchen)
[Fanny Arvieu](mailto:fanny.arvieu@balsamine.be)

## Traduire une page:
Pour traduire une page en une autre langue, il faut signaler au système qu'il existe une traduction d'une page spécifique.

```
1. dupliquer le document à traduire
2. nommer le document de la page traduite avec un -en ou -nl: exemple `pratique-en.md`
3. dans l'entête du document original:
écrire les clés:
— slug: <le-même-slug-sur-toutes-les-pages-traduites>
— Translation: true
— Lang: <en / fr / nl / es>
dans l'entête du nouveau document, ecrire les clés:
— slug: <le-même-slug-sur-toutes-les-pages-traduites>
— Translation: true
— Lang: <en / fr / nl / es>
4. Traduire le contenu dans la langue du nouveau document.
```
Ensuite, un bouton pour changer de langue deviendra actif sur les pages en question.

## déployer le site

Il existe deux version publique du site, une qui s'appelle staging, qui sert de preview ou de test pour verifier que les mises a jours se soient bien passees.

Quand les modifications sont faites dans l'interface gitlab et qu'on cliques sur le bouton 'Save', la version staging du site se met a jour. ceci peut prendre quelques minutes. [https://balsamine.be/pelican-deploy/generate](https://balsamine.be/pelican-deploy/generate)

Si le staging ne refletes pas vos changements, il est possible de redemander manuellement la recreation de la version de staging en chargeant cette url: http://balsamine.be/update.php . Cette page prends du temps a charger car en chargeant cette url, on lance le procede d'update. Quand la page est chargee, la mise a jour est finie, et la machine nous retourne une serie d'informations sur cette mise a jour, semblable a ceci:

```
.  ____  .    ____________________________
|/      \|   |                            |
[| ♥    ♥ |]  | Git Deployment Script v0.1 |
|___==___|  /                             |
             | && sourced from            |
             | https://gist.github.com/   |
             | oodavid/1809044            |
             |                            |
             |____________________________|

pulling from remote
Already up-to-date.
Done: Processed 37 articles, 13 drafts, 4 pages and 3 hidden pages in 98.51 seconds.
pelican generation done
```  

Quand on est satisfait des changements visibles sur staging.balsamine.be il est temps de deployer ces changements au site publique. Pour publier ces changements, il faut charger une seconde url: [https://balsamine.be/pelican-deploy/publish](https://balsamine.be/pelican-deploy/publish). Cette page retourne une page semblable a update.php, mais donne trace de la copie de fichiers entre staging et le site live.

Voila le site est a jour avec vos derniers changements!
