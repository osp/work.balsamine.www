Title:Défense verbale
Date: 2020/03/30
Time: 19h00 à 22h00
Slug: xx-time-defenseverbale
Event_type: Atelier pratique avec Garance asbl <br/> gratuit mais réservation indispensable - nombre de participantes limité <br/> Atelier annulé
Status: draft
Color: #ffffff

Au travail, dans la rue, en famille ou ailleurs, il nous arrive parfois de sentir que quelqu'un.e dépasse les bornes, d'être surprise, indignée ou intimidée et de ne pas savoir comment répondre. Dans un atelier de défense verbale nous apprenons, de manière interactive et ludique, comment mieux connaître et poser nos limites, à choisir davantage nos réactions. Dans des discussions, des jeux de rôles et exercices pratiques, nous développerons ensemble des trucs et astuces pour la défense verbale au quotidien. 

Cet atelier est mené par Elise Dethier. 

Elise Dethier est formatrice en défense verbale et autodéfense féministes au sein de Garance ASBL depuis 2007.  

Garance est une organisation féministe d'Education Permanente reconnue par la Fédération Wallonie Bruxelles 


[voir le programme du festival xx time](https://www.balsamine.be/saison-19-20/xx-time.html)