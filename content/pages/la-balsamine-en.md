Title: Balsamine
Date: 2017/04
Slug: la-balsamine
Translation: true
Lang: en
Subhead: A theatre in its P.U.R.e state
Subsubhead: Pooled. Utopian. Responsible.


[TOC]

<!-- Photo balsa -->


## An ethical project for the 5 upcoming seasons.

A Balsa in a purified, purged, pureed state, on certain evenings and depending on the menus. Pure theatre. As it is.

Pooled, because open to the world, ready to receive the other in its diversity, in its complexity. In dialogue, because that is the only route towards appeasement.

The only path possible is accessibility to all, even to the bees which populate our roof. The Balsa is a place that changes and grows with the artists who inhabit it, with the audience that passes through it. The Balsa ripens and infects through its libertarian spirit. The choice is clear; we are a soil for foreign and reflective languages, a place where one can lose oneself to better discover oneself.

Utopian, what could be more logical? The risk of seeing, the risk of doing, the risk of saying and hearing, that is what utopia is: offering a development to singular works. A place which takes risks because this involves living more and there is no risk without the human human who takes that risk, who runs that risk. Creations which the stage the narratives which our era manufactures. Fragmentary, hybrid narratives. To give them time, to give them space.

Responsible, finally. It is a green house, which attempts through its choices to respect human beings and nature, to live within a sustainable development and to invest its means in creativity in its richest forms. Yesterday, today and tomorrow.

## Team
General and artistic management
:   Monica Gomes [monica.gomes@balsamine.be](mailto:monica.gomes@balsamine.be)

Financial and administrative management
:   Morgan Brunea [morgan.brunea@balsamine.be](mailto:morgan.brunea@balsamine.be)

General coordination – Communication – Welcoming companies
:   Fanny Arvieu [fanny.arvieu@balsamine.be](mailto:fanny.arvieu@balsamine.be)

Press – Promotion – Public relations
:   Irène Polimeridis, temporarily absent, replaced by Marion Birard [relations.publiques@balsamine.be](mailto:relations.publiques@balsamine.be)

Actress and Romanist – Mediation for schools and associations
:   Noemi Tiberghien [noemi.tiberghien@balsamine.be](mailto:noemi.tiberghien@balsamine.be)

Production- Diffusion
:   Clément Dallex Mabille [production@balsamine.be](mailto:production@balsamine.be)

Director and author – Associate artist
:   Martine Wijckaert [martine.wijckaert@balsamine.be](mailto:martine.wijckaert@balsamine.be)

Artistic and administrative residency
:   Théâtre du Tilleul [www.theatredutilleul.be](http://www.theatredutilleul.be)

Technical management
:   Jef Philips [jef.philips@balsamine.be](mailto:jef.philips@balsamine.be)

Stage manager
:   Rémy Urbain [remy.urbain@balsamine.be](mailto:remy.urbain@balsamine.be)

Trainee stage manager
:   Alice Spenlé

Bar manager
:   Inge Van audenaerde 

Associate stage photographer
:   Hichem Dahes

Associate graphic designers
:  Open Source Publishing [Open Source Publishing](http://osp.kitchen)

## Renting la Balsamine

Situated in a stone's throw from the European institutions, La Balsamine can offer you several of its spaces, comprising a technical and professional infrastructure as well as a convivial bar.

Whether you are organizing a seminar, a workshop, a training day, a birthday or a wedding, contact us with your query and we will advise you. We will put at your disposal the most suitable space, equipment of the highest quality and a professional technical team.

* * *

[More info](/rent.html)


## Partners

The Balsamine is subsidized by the Wallonia-Brussels Federation and is part of the Choreographic Stages (Scènes chorégraphiques) network of the French-Speaking Community Commission (CoCoF) of the Brussels-Capital Region.

The Balsamine is also supported by Wallonia-Brussels Theatre/Dance and Wallonia-Brussels International.

![](/images/img-19-20/logos/logos-all-large-19-20.svg)


The Balsamine is a signatory of the United Stages charter.

To proclaim loud and clear its support for civilian populations in danger pretty much everywhere in the world, the victims of violence and any other form of endangerment, many partners of the Belgian cultural sector have created an ethical charter and the UNITED STAGES label. The aim is to bring together and use the active forces of the performing arts and of community associations, as well as the closeness they maintain with their audiences, to initiate concrete actions bearing positive change for their beneficiaries. Various actions will be set up, at the Balsamine and in other venues.

[More information on Facebook United Stages Belgium](https://www.facebook.com/unitedstagesbelgium/).

An initiative of Action Sud CCR, Choux de Bruxelles, Globe Aroma, Kaaitheater, La Bellone, La Vénerie, La Tentation, Le boson, Les Midis de la poésie, MET-X, Passa Porta, Théâtre de l’Ancre, Théâtre La Balsamine, Théâtre Océan Nord, Théâtre Varia, Théâtre 140 …
{: footnotes }

## History

The theatre La Balsamine was founded by [Martine Wijckaert](http://martine-wijckaert.be) in 1974. At the beginning it was a simple legal entity, a non-profit organisation, making it possible to obtain subsidies for artistic creation.

The theatre thus created had neither a venue nor a form of residency within another existing structure. It nevertheless obtained subsidies on an ad hoc basis, enabling [Martine Wijckaert](http://martine-wijckaert.be)’s first shows to be produced. These productions were born from a mode of urban nomadism: abandoned sites became so many natural sets on which the performances carried out an intervention.

In 1981, La Balsamine theatre moved, in a temporarily definitive way, into the wasteland of the former Dailly barracks.

![historique 1](/images/shows/historique1.jpg)

These barracks had been abandoned for several years and they were discovered in their original state. An impressive perimeter of late-nineteenth-century buildings encircles an interior park which had become wild again. The site has all the virtues of a fictive city and this quality invited the theatre to establish itself there.

There then began a ‘wild’ period during which a very large number of premises were taken over for their raw spatial value and where made-to-measure shows came into being, as so many unique and totally ephemeral interventions on the architecture.

Theatre, visual art and music mixed and brought together on the site numerous artists discovering there a work and experimentation residency.

At the same time as managing La Balsamine, established in the barracks, [Martine Wijckaert](http://martine-wijckaert.be) pursued her own artistic work there.

Activity would also centre on one of the barracks’ particular sites, baptized the ‘amphitheatre’, then rapidly the ‘amphi’, because of its architectural characteristics: it is in effect a former military auditorium, structured in a semicircle.

<div class="jcarousel" markdown>
![historique 2](/images/shows/historique2.jpg)
:   -

![historique 3](/images/shows/historique3.jpg)
:   -
</div>

The first of the amphi’s developments were carried out according to the technique of absolute recycling, the necessary and still useable equipment being dismantled from other wings of the barracks and being reassembled in the amphi after being transformed.

In 1994, [Martine Wijckaert](http://martine-wijckaert.be) entrusted the theatre’s artistic management to Christian Machiels and remained there in her capacity as associate artist. The current theatre, of a contemporary style having nevertheless retained the singularity of the original theatre, dates from 2001 and is the work of architect Francis Metzger, for which he took 3rd prize in architecture at the Costa Rica Biennale. Christian Machiels’ programme was focused entirely on the performing arts and on fledgling theatre and contemporary dance companies.

## Website and season archives

* [Season 2011-2012](http://2011-17.balsamine.be/index.php/Archives/Saison2011-2012.html)
* [Season 2012-2013](http://2011-17.balsamine.be/index.php/Archives/Saison2012-2013.html)
* [Season 2013-2014](http://2011-17.balsamine.be/index.php/Archives/Saison2013-2014.html)
* [Season 2014-2015](http://2011-17.balsamine.be/index.php/Archives/Saison2014-2015.html)
* [Season 2015-2016](http://2011-17.balsamine.be/index.php/Archives/Saison2015-2016.html)
* [Season 2016-2017](http://2011-17.balsamine.be)
* [Saison 2017-2018](https://2017-18.balsamine.be)
* [Saison 2018-2019](https://2018-19.balsamine.be)
