Title: Masrah Festival
Date: 2018/11/28
Slug: masrah-festival
Time: 20h30
End_date: 2018/11/29
Key_image:
Key_image_detail_body: img-18-19/Masrah-RGB-horizontal.jpg
Piece_author:  Maman (je vois sans yeux et sans bouche je crie) / Solo
Event_type: <br/>En partenariat avec La charge du Rhinocéros et Le Moussem ( Centre nomade des arts) 
Subhead:
Partner_logo: /images/img-18-19/LOGOCHARGEnoir_txt.jpg  
              /images/img-18-19/Moussem-logo-CMYK-EN-black.jpg
Color: #ffffff


En 2018, Wallonie-Bruxelles international fête une année Maroc. Dans ce contexte, la Charge du Rhinocéros en collaboration avec le Moussem, propose un festival de deux jours consacré au répertoire théâtral marocain.
Un programme qui fait le lien entre auteurs, créateurs et public et qui lève le voile sur la pratique contemporaine du théâtre marocain. Le festival souhaite accroître les connaissances du terrain de chacun et favoriser un dialogue entre artistes belges et marocains. Un dialogue capable de contribuer à un répertoire futur partagé et commun.
{: .pedagogique }

## Au programme :

**[Le 28 novembre](/saison-18-19/maman.html)** :

> 15h : Ahmed Massaïa: introduction “théâtre au Maroc – des origines jusqu'au XXIe siècle" + conversation avec Ahmed Massaïa - en français 
entrée libre( réservation souhaitée) - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=101)

> 16h30 - Table ronde avec Ahmed Massaïa, le metteur en scène Mohamed Elhor et une comedienne du Akoun Theater. Moderation: Soraya Amrani - en français 
entrée libre ( réservation souhaitée) - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=102)

> 20h30 : [Maman (je vois sans yeux et sans bouche je crie)](/saison-18-19/maman.html) - Hamadi / La Charge du rhinocéros - en français
Théâtre - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=98)

**[Le 29 novembre](/saison-18-19/solo.html)** :

> 19h : Présentation du nouveau texte de théâtre 'Donia' de Taha Adnan: lecture et discussion - en français
entrée libre( réservation souhaitée) - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=103)


> 20h30 : [Solo](/saison-18-19/solo.html) - Akoun Theater
en arabe surtitré en français - [Réserver](http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=99)


Ce programme est présenté avec le soutien de Wallonie-Bruxelles International, dans le cadre de l'année du Maroc. 
{: .pedagogique }

Plus d'infos sur [La charge du Rhinocéros](http://www.chargedurhinoceros.be) et sur [Le Moussem](https://www.moussem.be/fr)
{: .pedagogique }
