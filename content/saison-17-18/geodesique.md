Title: Géodésique
Date: 2017/12/27
Times: 11h00
       14h00
Category: Noël au Théâtre
Color: #eae7af
Partner_logo: /images/logos/ctej.svg
              /images/logos/grenouille.jpg
Piece_author: Cie L’Inconnue
Age: à partir de 4 ans
Reservation_link: https://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=39

**Les représentations sont complètes.** 

Des pièces en bois à taille humaine, une feuille, une boule de laine, une girafe… deux danseuses. Elles sculptent, inventent et transforment l’espace dans leurs jeux de construction et reconstruction, de contrastes et d’échelles. Elles observent l’envol d’une montagne, elles traversent une muraille, elles s’embarquent sur une rivière. Elles s’opposent, elles se taquinent, elles se découvrent au soleil. Elles jouent et nous emportent dans le plaisir et le vertige du mouvement, la danse.

Concept, Chorégraphie
:    Javier Suárez

Création, danse
:    Erika Faccini, Nora Alberdi

Musique
:    Jesús Fictoria

Régie
:    Matthieu Kaempfer – Frédéric Vannes

Lumières, Scénographie
:    Raphaël Rubbens

Accompagnement Dramaturgique
:    Isabelle Dumont

Accompagnement du projet
:    Myriam Horman

Photo
:    Nicolas Bomal

<div class="galerie" markdown=true>

![credit: Nicolas Bomal](/images/geosite.jpg)

![credit: Nicolas Bomal](/images/geosite2.jpg)

</div>
