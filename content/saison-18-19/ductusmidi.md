Title: Ductus Midi
Date: 2019/05/16
Dates: 2019/05/16 20h30
       2019/05/17 20h30
       2019/05/18 18h00
       2019/05/19 20h30
Slug: Ductus Midi
Category: Kunstenfestivaldesarts
Key_image_detail_body: Anne_Lise_Le_Gac.jpg
Piece_author: Anne Lise Le Gac & Arthur Chambry
Event_type: Performance - Marseille - Création 
Partner_logo: /images/logos/logokunst.svg
Reservation_link: https://www.kfda.be/fr/tickets
Color: #009933

La jeune artiste française Anne Lise Le Gac fait preuve d’une grande liberté dans le développement de son langage formel, ce qui donne lieu à des mélanges de genres surprenants, comme un barbecue-conférence ou un banquet de recettes Saudade. Ductus Midi, réalisée avec l’artiste Arthur Chambry, est un mode d’écriture qui s’établit selon un trajet plutôt qu’un plan. Les premiers auteurs du Moyen-Âge percevaient ce flux en terme de ductus, un moyen de se frayer un chemin a l’intérieur d’une composition. Le trajet lui-même,
son élaboration et le vécu de son parcours produisent une liberté de penser, de lire et de se
mouvoir. Quatre voyageurs différents explorent ce principe nomade ; ils se rencontrent sur scène et entremêlent les genres et les pratiques, générant des croisements fascinants, comme des images acoustiques ou des instruments dansants. Un spectacle plus grand que la somme de ses composants. Une véritable découverte.


Concept & performance
: Anne Lise Le Gac & Arthur Chambry

Collaboration & performance
: Katerina Andreou

With participation of
: Christophe Manivet

Direction & light
: Nils Doucet

Presentation: Kunstenfestivaldesarts, La Balsamine
Production: Parallèle – Plateforme pour la jeune création internationale (Marseille)
Coproduction: Kunstenfestivaldesarts, Tanzquartier Wien, Be My Guest - Réseau international pour la jeune création, Buda Kunstencentrum (Kortrijk), Arsenic - Centre d’art
scénique contemporain (Lausanne), 3 bis f - lieu d'arts contemporains (Aix-en-Provence), CENTRALE FIES_Art Work Space (Italy)
Residencies: La Villette (Paris), Montévidéo centre d’art (Marseille), CENTRALE FIES_Art Work Space (Italy), Friche La Belle de Mai & Triangle France (Marseille), La Balsamine,
(Brussels), 3 bis f - lieu d'arts contemporains (Aix-en-Provence), Buda Kunstencentrum (Kortrijk)
With the support of: Triangle France – Astérides
{: .production }