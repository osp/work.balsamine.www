Title: Etna
Date: 2019/02/12
Time: 20h30
Slug: xx-time-etna
Piece_author: Thi-Mai Nguyen
Event_type: Reprise danse <br/> Dans le cadre de Brussels, dance ! Focus on contemporary dance.<br/> Représentation solidaire. L'intégralité de la billetterie est reversée à l'association DoucheFLUX.
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=104
Partner_logo: /images/logos/brussels-dance.svg
Status: draft
Color: #fe5000

Une femme errante et solitaire, tel un spectre hanté par ses débris de vie.

Etna est une femme sans âge, sans domicile, harcelée par les sonorités de sa vie passée et dont le corps incarne une lassitude extrême. Elle squatte la scène du théâtre et l’habite au travers de ses obsessions et de ses souvenirs.

Conception et interprétation
:    Thi-Mai Nguyen

Création sonore et régie son
:   Antoine Delagoutte

Création lumière et régie lumière 
:  Rémy Urbain

Diffusion
: Eléonore Valère Lachky

Production déléguée 
: Théâtre la Balsamine

Avec l’aide de la Fédération Wallonie-Bruxelles – Service de la Danse, Avec le soutien du Théâtre Marni, de la Maison de la Création et d’Ultima Vez.
{: .production }

Etna a reçu le Prix de la Critique 2018, dans la catégorie Meilleur spectacle de danse.
{: .production }

En savoir plus sur l'association [DouchefFLUX](http://www.doucheflux.be/)
{: .pedagogique }


<div class="galerie photos-spectacle" markdown=true>
![etna](/images/img-18-19/Etna1.jpg){: .image-process-large}
:   © Hugues Anhes

![etna](/images/img-18-19/Etna2.jpg){: .image-process-large}
:   © Hugues Anhes

![etna](/images/img-18-19/Etna3.jpg){: .image-process-large}
:   © Hugues Anhes

![etna](/images/img-18-19/Etna4.jpg){: .image-process-large}
:   © Hugues Anhes

![etna](/images/img-18-19/etna1.jpg){: .image-process-large}
:   © Hichem Dahes

![etna](/images/img-18-19/etna2.jpg){: .image-process-large}
:   © Hichem Dahes

![ou-suis-je](/images/img-18-19/etna3.jpg){: .image-process-large}
:   © Hichem Dahes

![etna](/images/img-18-19/etna4.jpg){: .image-process-large}
:   © Hichem Dahes

![etna](/images/img-18-19/etna5.jpg){: .image-process-large}
:   © Hichem Dahes

![etna](/images/img-18-19/etna6.jpg){: .image-process-large}
:   © Hichem Dahes
