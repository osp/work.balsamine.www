Title: 6. Backstage
Subhead: Adaptation libre et bilingue du texte éponyme d'Oscar van Woensel / Een vrije tweetalige bewerking van de gelijknamige tekst van Oscar van Woensel. <br/> Les représentations du jeudi 20 février à 14h et 20h30 sont complètes. 
Dates: 2020/02/12 → 2020/02/15 20h30
       2020/02/18 → 2020/02/21 20h30
       2020/02/17 14h00
       2020/02/20 14h00
Slug: backstage
Key_image: img-19-20/Backstage_web1.jpg
Key_image_detail_body: img-19-20/Backstage_web1.jpg 
Piece_author: Collectif BXL WILD
Event_type: Création théâtre - Theater creatie <br/>surtitré en NL+FR - met boventiteling in NL+FR 
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=124
Color: #7fa22a

Au Théâtre National d’une ville totalement fictive, une compagnie, réputée et grassement subventionnée, joue la centième représentation de *Hamlet* de William Shakespeare. Durant cette représentation, six figurants - francophones et néerlandophones – prennent en charge, tour à tour, tous les rôles muets de la pièce. Toute l’action se déroule dans la loge des figurants, un vendredi soir, par une froide nuit de décembre. Tandis que l’ennui et la contrariété vont grandissant, les voilà qui se mettent à boire et à saboter leurs entrées en scène. Des frustrations exacerbées qui provoqueront intrigues et broutilles en tous genres…
{: .push-down}

Backstage est le premier spectacle de BXL WILD, nouveau collectif dans le paysage théâtral bruxellois. Le collectif a une recette simple et efficace : un texte fort, des acteurs expérimentés et polyglottes, un lieu de représentation, les ingrédients essentiels pour produire un spectacle unique en son genre. Il est constitué de francophones et de néerlandophones : Andréa Bardos, Bernard Eylenbosch, Ina Geerts, Eno Krojanker, Circé Lethem et Tomas Pevenage. Pour sa première création, BXL WILD souhaite réaliser un spectacle bilingue mettant en valeur son identité bruxelloise et montrer combien le bilinguisme quotidien est dans leur ADN.

In een groot fictief theater, in een fictieve stad, speelt een groot tweetalig gezelschap de honderdste voorstelling van HamletHamlet. In deze voorstelling, nemen zes figuranten, Nederlandstaligen en Franstaligen, alle tekstloze rollen voor hun rekening. De actie speelt zich af in de loge van de figuranten, op een koude winteravond in december. Terwijl verveling en frustratie de kop opsteken, zetten ze zich aan het drinken en mislopen op die manier hun entrée op de scène, met intriges en onenigheid als gevolg. 
{: .nl}

BXL WILD is een nieuw acteurscollectief in het Brusselse theaterlandschap. Het concept is eenvoudig maar vanzelfsprekend: een sterke tekst, een groep tweetalig doorgewinterde acteurs en een speelplek, de bouwstenen om in vertrouwen een authentieke voorstelling te maken. BXL WILD bestaat uit Nederlandstaligen en Franstaligen: Andrea Bardos, Bernard Eylenbosch, Ina Geerts, Eno Krojanker, Circé Lethem en Tomas Pevenage. In haar eerste creatie wil BXL WILD een tweetalige voorstelling maken waarin onze Brusselse identiteit in de verf wordt gezet. Is het uniek om in een theaterstuk naargelang de taal van de gesprekspartner te switchen van het Nederlands naar het Frans. 
{: .nl}


Avec / met  
:   Andréa Bardos, Bernard Eylenbosch, Ina Geerts, Eno Krojanker, Circé Lethem, Tomas Pevenage

Création sonore / geluidsontwerp
:   Guillaume Istace

Création costumes / kostuumontwerp
:   Lieve Meeussen

Création lumière / lichtontwerp
:   Rémy Urbain

Regard extérieur / coach
:   Vital Schraenen

Surtitrages / boventitel
:   Inge Floré et Tineke De Meyer 

Production déléguée / Afgevaardigd producent
:   Théâtre la Balsamine

Coproductions / Coproducties
:   Théâtre la Balsamine, La Coop asbl et Shelter Prod

Soutiens / Met de steun van
:   Fédération Wallonie-Bruxelles –Service du théâtre, Théâtre Jardin Passion, taxshelter.be, ING, Tax-shelter du gouvernement fédéral belge, GC De Kriekelaar, Brasserie de la Senne

Backstage bénéficie du programme initiation théâtrale, un soutien de la Cocof, qui nous permet de proposer ce spectacle aux élèves des écoles secondaires francophones de la Région Bruxelles-Capitale au tarif de 2€ la place.<br/>
Backstage profiteert van het theatrale initiatieprogramma, een ondersteuning van Cocof, waardoor we deze show kunnen aanbieden aan studenten van Franstalige middelbare scholen in het Brussels Hoofdstedelijk Gewest voor slechts € 2 per stoel.
{: .pedagogique }

<div class="galerie photos-spectacle" markdown=true>
![backstage](/images/img-19-20/Backstage_web1.jpg){: .image-process-large}
:   © Hichem Dahes

![backstage](/images/img-19-20/Backstageweb2.jpg){: .image-process-large}
:   © Hichem Dahes

![backstage](/images/img-19-20/Backstageweb3.jpg){: .image-process-large}
:   © Hichem Dahes

![backstage](/images/img-19-20/Backstageweb4.jpg){: .image-process-large}
:   © Hichem Dahes 

![backstage](/images/img-19-20/Backstageweb6.jpg){: .image-process-large}
:   © Hichem Dahes 

![backstage](/images/img-19-20/Backstageweb7.jpg){: .image-process-large}
:   © Hichem Dahes

![backstage](/images/img-19-20/Bacsktageweb5.jpg){: .image-process-large}
:   © Hichem Dahes

<iframe src="https://player.vimeo.com/video/389698071" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
</div>
