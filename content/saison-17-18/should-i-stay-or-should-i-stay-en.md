Title: Should i stay or should i stay
Date: 2017/11/16
Dates: 2017/11/16 20h30
       2017/11/20 20h30
       2017/11/22 20h30
       2017/11/23 20h30
       2017/11/24 20h30
Piece_author: Simon Thomas
Event_type: Theater Cover
Key_image: shows/7-should-i-stay-or-should-i-stay/should-i-stay-composition1.png
Key_image_detail_body: shows/7-should-i-stay-or-should-i-stay/anim-chat.gif
Reservation_link: http://balsamine.billetterie.it/index.php?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=22&cntnt01returnid=44
Color: #3b4086
Translation: true
Lang: en
Slug: should-i-stay-or-should-i-stay


Sold out 

An empty space. A door. And four crazy-looking weirdos in gaudy costumes. Location unknown. Unknown period. Names multiplied to infinity. All that we know is that they are stuck in a room when it would just be enough to open the door ...

They talk about the immortality of lobster, three-tail cats, exile on Mars. It is understandable that they are rather existential questions which haunt them and which echo our anxieties of living in the face of passing time, death and the feeling of helplessness.

Director and writer
:    Simon Thomas

Cast
:    Jules Churin
:    Héloïse Jadoul
:    Manon Joannotéguy
:    Lucas Meister

Costume design
:    Camille Flahaux

First elaboration within the Institut National Supérieur des Arts du Spectacle et des Techniques de Diffusion of the Fédération Wallonie-Bruxelles. Produced with support from the Balsamine Theater, helped by the Fédération Wallonie-Bruxelles — Service du Théâtre and the support of the Fonds Marie-Paule Delvaux Godenne, managed by la Fondation Roi Baudouin.
{: .production }

<div class="galerie" markdown=true>
![chat](/images/shows/7-should-i-stay-or-should-i-stay/cat-bake-tex-split-3.png){: .image-process-detail}

![chat](/images/shows/7-should-i-stay-or-should-i-stay/chat-3-queues.png){: .image-process-large}

![chat](/images/shows/7-should-i-stay-or-should-i-stay/cat-bake-tex-split-1.png){: .image-process-large}

![chat](/images/shows/7-should-i-stay-or-should-i-stay/chat-3-queues-transparent.png){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis1.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis2.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis3.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis4.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis5.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis6.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis7.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis8.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis9.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis10.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis11.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis12.jpg){: .image-process-large}

![plateau](/images/shows/7-should-i-stay-or-should-i-stay/sis13.jpg){: .image-process-large}

<iframe class="video" src="https://player.vimeo.com/video/242549969"></iframe>

</div>
